FROM node:12-slim

ENV NODE_ENV development

WORKDIR /app

COPY package*.json ./

RUN set -ex \
  && apt-get update \
  && apt-get install -y make g++ python --no-install-recommends git ca-certificates \
  && npm ci

COPY . .

CMD ["npm", "run", "dev"]