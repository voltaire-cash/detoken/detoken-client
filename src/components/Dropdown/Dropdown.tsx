import React, { FC, useCallback, useEffect, useRef, useState } from 'react';
import ReactDOM from 'react-dom';
import { useTranslation } from 'react-i18next';
import { CSSTransition } from 'react-transition-group';
import styled, { Theme } from '~config/theme';
import ArrowIcon from '~icons/dropdownArrow.svg';

const DURATION = 200;

export interface ITranslations {
  [key: string]: string;
}

interface IProps {
  iconClassName?: string;
  maxHeight?: number;
  minWidth?: number;
  items: Array<string | number>;
  onChange: (value: string) => void;
  value: string | number;
  compact?: boolean;
  hideBorder?: boolean;
  translations?: ITranslations;
}

export const Dropdown: FC<IProps> = ({
  iconClassName,
  items,
  onChange,
  value,
  minWidth,
  compact,
  hideBorder = false,
  translations,
  ...other
}: IProps) => {
  const [isOpen, setIsOpen] = useState(false);
  const dropdownRef = useRef<HTMLDivElement>(null);
  const { t } = useTranslation();
  const handleOutsideClick = useCallback(
    (e: MouseEvent) => {
      // @ts-expect-error
      if (isOpen && !dropdownRef?.current?.contains(e.target)) {
        setIsOpen(false);
      }
    },
    [isOpen]
  );

  // is using to make dropdown element smaller. Digits are pixels
  const padding = compact ? '4' : '7';

  useEffect(() => {
    document.addEventListener('click', handleOutsideClick);
    return (): void => {
      document.removeEventListener('click', handleOutsideClick);
    };
  }, [handleOutsideClick]);

  const handleHeaderClick = useCallback((): void => {
    setIsOpen(!isOpen);
  }, [isOpen]);

  const handleOptionClick = useCallback(
    (e: React.MouseEvent<HTMLDivElement>): void => {
      const { option } = e.currentTarget.dataset as { option: string };
      onChange(option);
    },
    [onChange]
  );

  const newItems = items
    .filter((item) => item !== value)
    .map((item) => (
      <Option
        key={item}
        onClick={handleOptionClick}
        data-option={item}
        padding={padding}
      >
        {translations ? t(translations[item]) : item}
      </Option>
    ));

  const Portal = (): React.ReactElement => {
    return ReactDOM.createPortal(
      <Body padding={padding} compact={compact}>
        {newItems}
      </Body>,
      dropdownRef.current!
    );
  };

  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <Wrapper ref={dropdownRef} isOpen={isOpen} {...other}>
      <Header onClick={handleHeaderClick} minWidth={minWidth}>
        <DropdownHeader
          padding={padding}
          isOpen={isOpen}
          compact={compact}
          hideBorder={hideBorder}
        >
          {translations ? t(translations[value]) : value}
        </DropdownHeader>
        <ArrowIconStyled
          className={iconClassName || ''}
          rotation={isOpen ? 180 : 0}
        />
      </Header>
      <CSSTransition
        in={isOpen}
        classNames={{
          enter: `animation--enter`,
          enterActive: `animation--enterActive`,
          exit: `animation--leave`,
          exitActive: `animation--leaveActive`,
        }}
        timeout={DURATION}
        unmountOnExit
      >
        <Portal />
      </CSSTransition>
    </Wrapper>
  );
};
Dropdown.defaultProps = {
  iconClassName: '',
  maxHeight: 300,
};
const Wrapper = styled.div<{ isOpen: boolean }>`
  position: relative;
  margin: 0 10px;
`;

interface IHeader {
  minWidth?: number;
}

const Header = styled.div`
  position: relative;
  width: calc(100% + 10px);
  background: ${({ theme }): string => theme.background.primary};
  ${({ minWidth }: IHeader): string =>
    minWidth
      ? `
    min-width: ${minWidth}px;
  `
      : ''};
  cursor: pointer;
  right: 12px;
`;

interface IBody {
  theme: Theme;
  padding: string;
  compact?: boolean;
}

const Body = styled.div`
  position: absolute !important;
  max-height: 300px;
  box-shadow: 0 2px 3px 0 rgba(34, 36, 38, 0.15);
  z-index: 800;
  cursor: default;
  transform-style: flat;
  transform-origin: top center;
  top: calc(100% - ${({ compact }: IBody): string => (compact ? '4' : '10')}px);
  border: 1px solid ${({ theme }): string => theme.dropdown.border};
  border-radius: 0 0 3px 3px;
  border-top: none;
  width: calc(100% + 8px);
  background-color: ${({ theme }): string => theme.background.primary};
  padding-top: ${({ padding }: IBody): string => padding}px;
  right: 2px;
  color: ${({ theme }): string => theme.dropdown.text};
  user-select: none;
  overflow: auto;

  ${({ compact }: IBody): string =>
    compact
      ? `
    & > div {
      padding-top: 5px;
    }
    & > div:not(:last-of-type) {
      padding-bottom: 5px;
    }
  `
      : ''}

  &.animation--enter {
    opacity: 0.01;
    transform: scaleY(0);
  }
  &.animation--enterActive {
    opacity: 1;
    transform: scaleY(1);
    transition: all 200ms ease-in-out;
  }
  &.animation--leave {
    transform: scaleY(1);
    opacity: 1;
  }
  &.animation--leaveActive {
    opacity: 0.01;
    transform: scaleY(0);
    transition: all 200ms ease-in-out;
  }
`;

interface IArrowIconStyled {
  rotation: number;
}
const ArrowIconStyled = styled(ArrowIcon as 'img')`
  position: absolute;
  right: 5px;
  top: calc(50% - 2px);
  pointer-events: none;
  width: 6px;
  fill: ${({ theme }): string => theme.primaryAppColor};
  transform: rotate(${({ rotation }: IArrowIconStyled): number => rotation}deg);
  transition: transform 0.2s ease;
`;

interface IDropdownHeader {
  theme: Theme;
  padding: string;
  isOpen: boolean;
  compact?: boolean;
  hideBorder: boolean;
}
const DropdownHeader = styled.div`
  border: 1px solid
    ${({ theme, isOpen, compact }: IDropdownHeader): string =>
      !compact || isOpen ? theme.dropdown.border : theme.background.primary};
  ${({ hideBorder, isOpen }: IDropdownHeader): string =>
    hideBorder && !isOpen ? 'border: 1px solid transparent' : ''};
  border-radius: 4px;
  padding: ${({ padding }: IDropdownHeader): string => padding}px;
  color: ${({ theme }): string => theme.dropdown.text};
  user-select: none;
  &:hover {
    color: ${({ theme }): string => theme.menu.hover};
  }
`;
interface IOption {
  theme: Theme;
  padding: string;
}

const Option = styled.div(
  ({ theme, padding }: IOption) => `
  white-space: nowrap;
  padding: ${padding}px;
  &:hover {
    color: ${theme.dropdown.textHover};
    cursor: pointer;
  }
`
);
