import React, { FC, ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import { useLocation } from 'react-router-dom';
import styled, { Theme } from '~config/theme';
import LogoSvg from '~icons/shortLogo.svg';
import { DETOKEN_LINK } from '~config/constants';

const SHOW_FOOTER_LIST = ['/account'];

interface IFooter {
  version: string;
}

export const Footer: FC<IFooter> = ({ version }: IFooter): ReactElement => {
  const { t } = useTranslation();
  const location = useLocation();
  return (
    <Container show={SHOW_FOOTER_LIST.includes(location.pathname)}>
      <LogoContainer>
        <LogoSvg />
      </LogoContainer>
      <VersionContainer>{version}</VersionContainer>
      <LinksContainer>
        <a
          href={`${DETOKEN_LINK}/blog`}
          target="_blank"
          rel="noopener noreferrer"
        >
          {t('blog')}
        </a>
        <a
          href={`${DETOKEN_LINK}/terms`}
          target="_blank"
          rel="noopener noreferrer"
        >
          {t('terms')}
        </a>
        <a
          href={`${DETOKEN_LINK}/privacy`}
          target="_blank"
          rel="noopener noreferrer"
        >
          {t('privacy')}
        </a>
        <a
          href={`${DETOKEN_LINK}/about`}
          target="_blank"
          rel="noopener noreferrer"
        >
          {t('about')}
        </a>
        <a
          href="https://twitter.com/DetokenDotNet"
          target="_blank"
          rel="noopener noreferrer"
        >
          Twitter
        </a>
      </LinksContainer>
    </Container>
  );
};

interface IContainer {
  show: boolean;
  theme: Theme;
}
const Container = styled.div`
  display: ${({ show }: IContainer): string => (show ? 'flex' : 'none')};
  align-items: center;
  justify-content: center;
  height: 120px;
  flex: 0 0 auto;
  flex-direction: column;
  background-color: ${({ theme }): string => theme.background.primary};
  border-top: 1px solid ${({ theme }): string => theme.border.default};

  ${({ theme }): string => `
    ${theme.mediaTablet} {
      height: 180px;
    };
    `};
`;

const LogoContainer = styled.div`
  height: 30px;
  & svg {
    width: 24px;
  }
  & .shortLogo_svg__primary {
    fill: ${({ theme }): string => theme.logo.primaryColor};
  }
  & .shortLogo_svg__underline {
    fill: ${({ theme }): string => theme.logo.secondaryColor};
  }
  & .shortLogo_svg__dot {
    fill: ${({ theme }): string => theme.logo.dotColor};
  }
`;

const LinksContainer = styled.div`
  display: flex;
  margin-top: 2rem;
  & a {
    margin-left: 0.6rem;
    margin-right: 0.6rem;
    text-decoration: none;
    color: ${({ theme }): string => theme.footer.text};
    &:visited {
      color: ${({ theme }): string => theme.footer.text};
    }
    &:hover {
      color: ${({ theme }): string => theme.footer.hover};
    }
  }
  ${({ theme }): string => `
    ${theme.mediaTablet}{
      & a {
        margin-left: 1.1rem;
        margin-right: 1.1rem;
      }
      margin-top: 30px;
  `}
`;

const VersionContainer = styled.div`
  margin-top: 15px;
  font-size: 12px;
  color: ${({ theme }): string => theme.footer.text};
`;
