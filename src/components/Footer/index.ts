import { connect } from 'react-redux';
import { Footer } from './Footer';
import { IState } from '~dux/rootDux';

interface IFooter {
  version: IState['app']['version'];
}

const mapStateToProps = (state: IState): IFooter => ({
  version: state.app.version,
});

const FooterConnect = connect(mapStateToProps)(Footer);

export { FooterConnect as Footer };
