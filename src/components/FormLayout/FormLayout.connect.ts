import { connect } from 'react-redux';
import { IState } from '~dux/rootDux';
import { FormLayout } from './FormLayout';
import { IMapState, IMapDispatch } from './types';
import { switchTheme } from '~dux/app/appDux';

const mapDispatchToProps: IMapDispatch = {
  switchTheme,
};

const mapStateToProps = (state: IState): IMapState => ({
  userLang: state.app.userLang,
  isAuthorized: state.user.isAuthorized,
  theme: state.app.theme,
});

export default connect(mapStateToProps, mapDispatchToProps)(FormLayout);
