import React, { FC, ReactElement, ReactNode, useCallback } from 'react';
import { Link } from 'react-router-dom';
import FocusLock from 'react-focus-lock';
import i18next from 'i18next';
import LogoSvg from '~icons/mainLogo.svg';
import styled, { Theme } from '~config/theme';
import ChevronIcon from '~icons/chevron.svg';
import { IFormLayoutProps } from './types';
import { LANGUAGES } from '~config/constants';
import { Dropdown } from '~components/Dropdown';
import SunSvg from '~icons/sun.svg';
import MoonSvg from '~icons/moon.svg';

interface IProps extends IFormLayoutProps {
  logo?: boolean;
  title?: string;
  subtitle?: string | ReactElement;
  children: ReactNode;
  onBackClick?: () => void;
  icon?: ReactNode;
}

export const FormLayout: FC<IProps> = ({
  logo,
  title,
  subtitle,
  children,
  onBackClick,
  icon,
  userLang,
  isAuthorized,
  switchTheme,
  theme,
}: IProps): ReactElement => {
  const handleLangDropdownChange = useCallback((value: string): void => {
    i18next.changeLanguage(value === '中文' ? 'ZH' : value);
  }, []);
  const handleBackClick = useCallback(() => {
    onBackClick?.();
  }, [onBackClick]);
  return (
    <FocusLock autoFocus={false}>
      {!isAuthorized && (
        <Switchers>
          <ThemeIcon onClick={switchTheme}>
            {theme ? <MoonSvg /> : <SunSvg />}
          </ThemeIcon>
          <StyledDropdown
            items={LANGUAGES}
            onChange={handleLangDropdownChange}
            value={userLang === 'ZH' ? '中文' : userLang}
            hideBorder
          />
        </Switchers>
      )}
      <Container authorized={isAuthorized}>
        {logo && (
          <LogoContainer>
            <BackIcon onClick={handleBackClick}>
              {onBackClick && <ChevronIcon />}
            </BackIcon>
            <Link to="/dashboard">
              <Logo backBtn={!!onBackClick}>
                <LogoSvg />
              </Logo>
            </Link>
            <div />
          </LogoContainer>
        )}
        {icon && <IconContainer>{icon}</IconContainer>}
        {title && (
          <TitleContainer
            subtitle={!!subtitle}
            backBtn={!!onBackClick && !logo}
          >
            <BackIcon>
              {/* @ts-expect-error */}
              {!logo && onBackClick && <ChevronIcon onClick={onBackClick} />}
            </BackIcon>
            <Title>{title}</Title>
            <div />
          </TitleContainer>
        )}
        {subtitle && <Subtitle isChildren={!!children}>{subtitle}</Subtitle>}
        {children}
      </Container>
    </FocusLock>
  );
};

interface IContainer {
  theme: Theme;
  authorized: boolean;
}

const Container = styled.div`
  color: ${({ theme }): string => `${theme.text.primary}`};
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: ${({ authorized }: IContainer): string =>
    authorized ? '0' : '65px 44px 44px'};
  max-width: 400px;
  box-sizing: border-box;
  width: ${({ authorized }: IContainer): string =>
    authorized ? '100%' : '100vw'};
  text-align: center;
  background: ${({ theme, authorized }: IContainer): string =>
    authorized ? theme.background.modals : theme.background.primary};
  ${({ theme, authorized }: IContainer): string =>
    authorized
      ? ''
      : `${theme.mediaTablet} {
    padding: 44px;
    box-shadow: 0 0 30px 0 ${theme.shadowColor};
    border-radius: 4px;
  }`};
`;

const Title = styled.div`
  font-size: calc(4rem / 3);
  color: ${({ theme }): string => theme.text.header};
`;

const Subtitle = styled.div<{ isChildren: boolean }>`
  margin-bottom: ${({ isChildren }): string => (isChildren ? '30px' : '0px')};
  text-align: center;
`;

interface ILogo {
  theme: Theme;
  backBtn: boolean;
}
const Logo = styled.div`
  height: 27px;
  & svg {
    width: 135px;
  }
  // Keep logo in the center in case back button showed
  ${({ backBtn }: ILogo): string => `
    margin-left: ${backBtn ? '-16px;' : ''}
  `};
  & .mainLogo_svg__primary {
    fill: ${({ theme }): string => theme.logo.primaryColor};
  }
  & .mainLogo_svg__underline {
    fill: ${({ theme }): string => theme.logo.secondaryColor};
  }
  & .mainLogo_svg__dot {
    fill: ${({ theme }): string => theme.logo.dotColor};
  }
`;

const LogoContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  margin-bottom: 50px;
`;

const BackIcon = styled.div`
  cursor: pointer;
  & svg {
    width: 1rem;
    padding: 15px;
    margin: -15px;
    fill: ${({ theme }): string => theme.actionButton.color};
    opacity: 0.5;
    &:hover {
      opacity: 1;
    }
  }
`;

interface ITitleContainer {
  theme: Theme;
  subtitle: boolean;
  backBtn: boolean;
}

const TitleContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  text-align: center;
  font-weight: bold;
  ${({ subtitle, backBtn }: ITitleContainer): string => `
    margin-bottom: ${subtitle ? '10px' : '30px'};
    margin-left: ${backBtn ? '-16px;' : ''};
  `};
  ${({ theme, subtitle }): string => `${theme.mediaDesktop} {
    margin-bottom: ${subtitle ? '10px' : '30px'};
    
  }`};
`;

export const IconContainer = styled.div`
  margin-bottom: 26px;
  width: 60px;
  height: 60px;
  & > svg {
    width: 60px;
    height: 60px;
  }

  ${({ theme }): string => `
    ${theme.mediaDesktop}{
      width: 80px;
      height: 80px;
      & > svg {
        width: 80px;
        height: 80px;
      };
    };
  `};
`;

const StyledDropdown = styled(Dropdown)`
  font-size: 14px;
`;

const ThemeIcon = styled.div`
  cursor: pointer;
  width: 16px;
  display: flex;
  margin-right: 5px;

  & svg {
    fill: ${({ theme }): string => theme.menu.icon};
    width: 16px;
    &:hover {
      fill: ${({ theme }): string => theme.menu.hover};
    }
  }
`;
const Switchers = styled.div`
  display: flex;
  align-items: center;
  position: fixed;
  width: fit-content;
  top: 28px;
  right: 3px;
  height: 0;
  ${({ theme }): string => `
    ${theme.mediaTablet}{
      position: relative;
      left: calc(100% - 79px);
      top: 28px;
    }
  `}
`;
