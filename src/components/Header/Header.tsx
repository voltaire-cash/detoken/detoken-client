import React, { FC, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import styled from '~config/theme';

import { HeaderNavigation } from '~components/HeaderNavigation';
import { TopMenu } from '~components/TopMenu';
import { OpenModalAction } from '~dux/modals/modalsDux';

import MainLogoSvg from '~icons/mainLogo.svg';
import ShortLogoSvg from '~icons/shortLogo.svg';
import { Button } from '~components/Button';

interface IProps {
  openModal: (props: OpenModalAction) => void;
}

export const Header: FC<IProps> = ({ openModal }: IProps) => {
  const { t } = useTranslation();

  const handleSendClick = useCallback(
    () => openModal({ type: 'withdraw', data: {} }),
    [openModal]
  );
  const handleReceiveClick = useCallback(
    () => openModal({ type: 'deposit', data: {} }),
    [openModal]
  );
  return (
    <Container>
      <TopMenu />
      <MainMenuContainer>
        <Link to="/dashboard">
          <Logo>
            <MainLogoSvg />
          </Logo>
          <SmallLogo>
            <ShortLogoSvg />
          </SmallLogo>
        </Link>
        <HeaderNavigation />
        <Buttons>
          <Button onClick={handleReceiveClick}>{t('receive')}</Button>
          <Button variant="secondary" onClick={handleSendClick}>
            {t('send')}
          </Button>
        </Buttons>
      </MainMenuContainer>
    </Container>
  );
};

const Container = styled.div`
  background-color: ${({ theme }): string => theme.background.topHeader};
  z-index: 999;
  position: relative;

  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  border-bottom: 1px solid ${({ theme }): string => theme.border.default};

  box-sizing: border-box;
`;

const MainMenuContainer = styled.div`
  box-sizing: border-box;
  display: flex;
  justify-content: space-between;
  width: 100%;
  padding: 7px 10px;
  height: 44px;
  position: relative;

  & > a {
    align-self: center;
    display: flex;
  }

  ${({ theme }): string => `
    ${theme.mediaTablet}{
      height: 70px;
      padding: 13px 15px;  
    }
    ${theme.mediaDesktop}{
      padding: 13px 60px;  
    }
  `}
`;

const Logo = styled.svg`
  height: 24px;
  width: 114px;
  display: none;
  ${({ theme }): string => `
      ${theme.mediaTablet}{
        display: block;
      };
    `};

  & .mainLogo_svg__primary {
    fill: ${({ theme }): string => theme.logo.primaryColor};
  }
  & .mainLogo_svg__underline {
    fill: ${({ theme }): string => theme.logo.secondaryColor};
  }
  & .mainLogo_svg__dot {
    fill: ${({ theme }): string => theme.logo.dotColor};
  }
`;

const SmallLogo = styled.svg`
  width: 20px;
  height: 30px;
  ${({ theme }): string => `
      ${theme.mediaTablet}{
        display: none;
      };
    `};
  & .shortLogo_svg__primary {
    fill: ${({ theme }): string => theme.logo.primaryColor};
  }
  & .shortLogo_svg__underline {
    fill: ${({ theme }): string => theme.logo.secondaryColor};
  }
  & .shortLogo_svg__dot {
    fill: ${({ theme }): string => theme.logo.dotColor};
  }
`;

const Buttons = styled.div`
  align-self: center;
  display: none;
  & > button:first-of-type {
    margin-right: 15px;
  }

  ${({ theme }): string => `
    ${theme.mediaTablet}{
      display: block;
    }
  `}
`;
