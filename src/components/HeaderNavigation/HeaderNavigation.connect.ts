import { connect } from 'react-redux';
import { HeaderNavigation } from './HeaderNavigation';
import { IState } from '~dux/rootDux';
import { toggleMobileMenu } from '~dux/app/appDux';
import { IMapDispatch } from './types';

const mapDispatchToProps: IMapDispatch = {
  mobileMenuClick: toggleMobileMenu,
};

const mapStateToProps = (state: IState) => ({
  isMobileMenuOpen: state.app.isMobileMenuOpen,
});

export default connect(mapStateToProps, mapDispatchToProps)(HeaderNavigation);
