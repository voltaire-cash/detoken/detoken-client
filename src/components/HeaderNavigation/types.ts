export interface IMapDispatch {
  mobileMenuClick: () => void;
}

export interface IMapState {
  isMobileMenuOpen: boolean;
}

export interface IHeaderNavigationProps extends IMapDispatch, IMapState {}
