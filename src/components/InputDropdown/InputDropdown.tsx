import React, { FC, ReactElement } from 'react';
import styled, { Theme } from '~config/theme';
import { Input } from '~components';
import { IInputProps, InputButton } from '~components/Input/Input';

interface IProps extends IInputProps {
  dropdown: React.ReactNode;
  btnHighlighted: boolean;
}

export const InputDropdown: FC<IProps> = (props: IProps): ReactElement => {
  const { dropdown, btnText, btnClick, btnHighlighted, ...rest } = props;
  return (
    <InputWrapper>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <Input rightComponent={<DropWrapper>{dropdown}</DropWrapper>} {...rest} />
      {btnText && (
        <Button onClick={btnClick} type="button" highlighted={btnHighlighted}>
          {btnText}
        </Button>
      )}
    </InputWrapper>
  );
};

const InputWrapper = styled.div`
  display: flex;
  & > div {
    height: 48px;
    & > input {
      padding-right: 75px;
      font-size: 24px;
      border-radius: 3px 0 0 3px;
    }
  }
  & > button {
    height: 48px;
  }
`;

const DropWrapper = styled.div`
  position: absolute;
  right: 0;
  top: 50%;
  transform: translateY(-50%);
  & > div {
    margin: 0 5px;
  }
`;

interface IButton {
  theme: Theme;
  highlighted: boolean;
}
const Button = styled(InputButton)`
  ${({ theme, highlighted }: IButton): string => `
    color: ${highlighted ? theme.text.error : theme.secondaryButton.text};
    text-shadow: ${highlighted ? '0 0 1rem white' : 'none'}
    `}
`;
