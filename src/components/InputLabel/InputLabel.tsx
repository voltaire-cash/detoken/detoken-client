import styled, { Theme } from '~config/theme';

interface IInputLabel {
  theme: Theme;
  visible?: boolean;
}
export const InputLabel = styled.label`
  color: ${({ theme }): string => theme.text.primary};
  margin-bottom: ${({ visible }: IInputLabel): string =>
    visible ? '3px' : '0'};
`;
