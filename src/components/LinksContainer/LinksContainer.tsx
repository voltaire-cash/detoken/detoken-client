import styled, { Theme } from '~config/theme';

interface ILinksContainer {
  spaceBetween?: number;
  theme: Theme;
}

export const LinksContainer = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  font-size: 14px;
  margin-top: 30px;
  & > div {
    display: none;
  }
  & > a:first-of-type {
    margin-bottom: ${({ spaceBetween }: ILinksContainer): string =>
      spaceBetween ? `${spaceBetween}px` : '20px'};
  }
  flex-direction: column;
  align-items: center;
  ${({ theme }): string => `
    ${theme.mediaTablet}{
      & > div {
        display: block;
      };
      & > a:first-of-type {
        margin-bottom: 0;
      }
      flex-direction: row;
    };
  `}
`;
