import React, { FC, ReactElement, ReactNode, useEffect } from 'react';
import { ThemeProvider } from 'emotion-theming';
import { connect } from 'react-redux';
import { Global } from '@emotion/core';
import storage from 'local-storage-fallback';
import { IState } from '~dux/rootDux';
import styled, { dark, light, Theme } from '~config/theme';
import { makeGlobalStyles } from '~config/globals';
import { Header, MobileMenu, Footer } from '~components';
import { useQuery } from '~hooks/useQuery';
import { Notifier } from './Notifier';
import { appInit, isLoadingSelector } from '~dux/app/appDux';
import { FullScreenLoader } from './FullScreenLoader';
import { getCookie, setCookie } from '~utils/cookie';
import { Modals } from '~components/Modals';

interface IMapDispatch {
  initApplication: () => void;
}

interface IMapState {
  theme: boolean;
  isMobileMenuOpen: boolean;
  isAuthorized: boolean;
  isLoading: boolean;
}

interface IProps extends IMapState, IMapDispatch {
  children?: ReactNode;
}

export const MainLayoutFC: FC<IProps> = ({
  isLoading,
  initApplication,
  theme,
  isMobileMenuOpen,
  children,
  isAuthorized,
}: IProps): ReactElement => {
  const refId = useQuery().get('r') || null;
  const cookieRefId = getCookie('refId');
  if (refId) {
    storage.setItem('refId', refId);
    setCookie('refId', refId);
  } else if (cookieRefId) {
    storage.setItem('refId', cookieRefId);
  }
  const currentTheme = theme ? light : dark;

  useEffect(() => {
    if (isAuthorized) {
      initApplication();
    }
  }, [isAuthorized, initApplication]);

  return (
    <ThemeProvider theme={currentTheme}>
      <Notifier />
      {isLoading ? (
        <FullScreenLoader />
      ) : (
        <GlobalContainer
          disableScroll={isMobileMenuOpen}
          isAuthorized={isAuthorized}
        >
          <Global styles={makeGlobalStyles(currentTheme)} />
          {isAuthorized ? (
            <Content>
              <Header key="header" />
              <MobileMenu key="menu" />
              {children}
            </Content>
          ) : (
            <Center>{children}</Center>
          )}

          {isAuthorized && <Footer />}
        </GlobalContainer>
      )}
      <Modals />
    </ThemeProvider>
  );
};

const mapDispatchToProps: IMapDispatch = {
  initApplication: appInit,
};

const mapStateToProps = (state: IState): IMapState => ({
  theme: state.app.theme,
  isLoading: isLoadingSelector(state),
  isMobileMenuOpen: state.app.isMobileMenuOpen,
  isAuthorized: state.user.isAuthorized,
});

export default connect(mapStateToProps, mapDispatchToProps)(MainLayoutFC);

interface IContainerProps {
  theme: Theme;
  disableScroll: boolean;
  isAuthorized: boolean;
}
const GlobalContainer = styled.div`
  height: 100%;
  overscroll-behavior: none;
  display: flex;
  flex-direction: column;
  background-color: ${({ theme }: IContainerProps): string =>
    theme.background.primary};
  width: 100%;
  box-sizing: border-box;
  padding: 0;

  ${({ disableScroll }: IContainerProps): string =>
    disableScroll
      ? `
    position: fixed;
    overflow: hidden;
    `
      : ''}

  ${({ isAuthorized, theme }: IContainerProps): string =>
    ` 
   background: ${theme.background.primary};
    ${theme.mediaTablet} and (orientation: landscape){
      padding: ${isAuthorized ? '0' : '60px 0 0'};
    }
    ${theme.mediaDesktop}{
      padding: ${isAuthorized ? '0' : '60px 0 0'} !important;  
    }
    ${theme.mediaTablet}{
      padding: ${isAuthorized ? '0' : '200px 0 0'};  
      background: ${
        isAuthorized ? theme.background.primary : theme.background.unauthorized
      };
    }
    `}
`;

const Center = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  height: 100%;
`;

const Content = styled.div`
  flex: 1 0 auto;
  background: ${({ theme }): string => theme.background.secondary};
`;
