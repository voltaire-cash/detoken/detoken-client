import { OpenModalAction } from '~dux/modals/modalsDux';

export interface IMapDispatch {
  switchMenuState: () => void;
  switchTheme: () => void;
  openModal: (props: OpenModalAction) => void;
}

export interface IMapState {
  theme: boolean;
  userLang: string;
  isMenuOpen: boolean;
}

export interface IMobileMenuProps extends IMapState, IMapDispatch {}
