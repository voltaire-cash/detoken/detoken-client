import { connect } from 'react-redux';
import { IState } from '~dux/rootDux';
import { closeModal, IDepositDetailsData } from '~dux/modals/modalsDux';
import { DepositDetailsModal } from './DepositDetailsModal';
import { IMapState, IMapDispatch } from './types';
import { exchangeRatesSelector, oraclePriceSelector } from '~dux/app/appDux';

const mapStateToProps = (state: IState): IMapState => ({
  data: state.modals.depositDetails.data as IDepositDetailsData,
  exchangeRates: exchangeRatesSelector(state),
  currency: state.app.userCurrency,
  oraclePrice: oraclePriceSelector(state),
});

const mapDispatchToProps: IMapDispatch = {
  closeModal,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DepositDetailsModal);
