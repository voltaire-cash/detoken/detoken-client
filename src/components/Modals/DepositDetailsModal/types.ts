import { CloseModalAction, IDepositDetailsData } from '~dux/modals/modalsDux';
import { IDashboardPageProps } from '~pages/DashboardPage/types';
import { IAppState, IExchangeRates } from '~dux/app/appDux';

export interface IMapState {
  exchangeRates: IExchangeRates;
  data: IDepositDetailsData;
  currency: IAppState['userCurrency'];
  oraclePrice: IDashboardPageProps['oraclePrice'];
}

export type IMapDispatch = {
  closeModal: (props: CloseModalAction) => void;
};

export interface IDepositDetailsModalProps extends IMapState, IMapDispatch {}
