import { IModalsState, CloseModalAction } from '~dux/modals/modalsDux';
import { IAppState } from '~dux/app/appDux';

export interface IMapState {
  data: IModalsState['deposit']['data'];
  theme: IAppState['theme'];
  addresses: { activeAddress: string; changeAddress: string };
}

export type IMapDispatch = {
  closeModal: (props: CloseModalAction) => void;
};

export interface IDepositModalProps extends IMapState, IMapDispatch {}
