import React, { FC, ReactElement, useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  FormLayout,
  Form,
  Modal,
  TextField,
  PinCode2FA,
  InputWrapper,
} from '~components';
import { IDisable2FAModalProps } from './types';

export const Disable2FAModal: FC<IDisable2FAModalProps> = ({
  closeModal,
  data,
  disable2FA,
  setError,
}: IDisable2FAModalProps): ReactElement | null => {
  const [password, setPassword] = useState('');
  const [code, setCode] = useState('');

  const { t } = useTranslation();

  const { field, key, params } = data.error || {};

  const handleCloseModal = useCallback(() => {
    closeModal({ type: 'disable2FA' });
  }, [closeModal]);

  const handleFormSubmit = useCallback(
    (e: React.FormEvent) => {
      e.preventDefault();
      disable2FA({ password, code });
    },
    [disable2FA, password, code]
  );
  const handlePasswordChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setError(null);
      const { value } = e.currentTarget;
      setPassword(value);
    },
    [setError]
  );
  const handleChangeCode = useCallback(
    (value) => {
      setError(null);
      setCode(value);
    },
    [setError]
  );

  const handleEnterKeyDown = useCallback(() => {
    disable2FA({ password, code });
  }, [code, password, disable2FA]);

  return (
    <Modal onClose={handleCloseModal}>
      <FormLayout
        title={t('disable_2fa')}
        subtitle={t('enter_password_to_disable_2FA')}
      >
        <Form
          onSubmit={handleFormSubmit}
          submitText={t('disable_button')}
          submitDisabled={data.loading}
        >
          <TextField
            type="password"
            placeholder={t('current_password')}
            error={field === 'passwordHash'}
            helperText={field === 'passwordHash' && key ? t(key, params) : ''}
            value={password}
            onChange={handlePasswordChange}
          />
          <InputWrapper
            label={t('two_fa_code')}
            error={field === 'code'}
            helperText={field === 'code' && key ? t(key, params) : ''}
          >
            <PinCode2FA
              value={code}
              onChange={handleChangeCode}
              onEnterKeyDown={handleEnterKeyDown}
              autoFocus={false}
            />
          </InputWrapper>
        </Form>
      </FormLayout>
    </Modal>
  );
};
