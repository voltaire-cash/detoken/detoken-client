import { Dispatch } from 'redux';
import { batch } from 'react-redux';
import i18next from 'i18next';
import { IState } from '~dux/rootDux';
import { setUser2FAModeAction } from '~dux/user/userDux';
import { disable2FA as disable2FARequest } from './Disable2FAModalApi';
import { getHash } from '~utils/password';
import { IError } from '~types/Error';
import { updateModalData, closeModal } from '~dux/modals/modalsDux';
import { addSnackbarAction } from '~dux/notifications/notificationsDux';
import { validatePassword, validate2FACode } from '~utils/validation';

export interface IDisable2FA {
  password: string;
  code: string;
}

type Action = ReturnType<
  | typeof updateModalData
  | typeof setUser2FAModeAction
  | typeof addSnackbarAction
  | typeof closeModal
>;

export const setError = (err: IError | null) => (
  dispatch: Dispatch<Action>
): void => {
  dispatch(updateModalData({ type: 'disable2FA', newData: { error: err } }));
};

export const disable2FA = ({ password, code }: IDisable2FA) => async (
  dispatch: Dispatch<Action>,
  getState: () => IState
): Promise<void> => {
  dispatch(updateModalData({ type: 'disable2FA', newData: { loading: true } }));
  const passwordErr = validatePassword(password);
  const codeErr = validate2FACode(code);
  const err = passwordErr || codeErr;
  if (err) {
    dispatch(
      updateModalData({
        type: 'disable2FA',
        newData: {
          error: {
            field: passwordErr ? 'passwordHash' : 'code',
            key: err.key,
            params: err.params,
          },
          loading: false,
        },
      })
    );
    return;
  }
  const { email } = getState().user;
  try {
    await disable2FARequest({
      email,
      passwordHash: getHash(password),
      code,
    });
    batch(() => {
      dispatch(closeModal({ type: 'disable2FA' }));
      dispatch(setUser2FAModeAction(false));
      dispatch(
        addSnackbarAction({
          message: i18next.t('two_fa_disabled'),
          variant: 'success',
        })
      );
    });
  } catch (error) {
    dispatch(
      updateModalData({
        type: 'disable2FA',
        newData: { error, loading: false },
      })
    );
  }
};
