import { connect } from 'react-redux';
import { EmailInUseModal } from './EmailInUseModal';
import { closeModal } from '~dux/modals/modalsDux';

const mapDispatchToProps = {
  closeModal,
};

export default connect(null, mapDispatchToProps)(EmailInUseModal);
