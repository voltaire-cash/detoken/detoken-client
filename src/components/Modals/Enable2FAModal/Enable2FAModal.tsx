import React, {
  FC,
  ReactElement,
  useCallback,
  useEffect,
  useState,
} from 'react';
import { IEnable2FAModalProps, PREV_STEPS, STEPS } from './types';
import { Enable2FAModalCurrentPassword } from './components/Enable2FAModalCurrentPassword';
import { Enable2FAModalInfo } from './components/Enable2FAModalInfo';
import { Enable2FAModalQRCode } from './components/Enable2FAModalQRCode';
import { Enable2FAModal2FACode } from './components/Enable2FAModal2FACode';
import { Enable2FAModalSuccess } from './components/Enable2FAModalSuccess';
import { Modal } from '~components';

export const Enable2FAModal: FC<IEnable2FAModalProps> = ({
  init2FA,
  email,
  enable2FA,
  data,
  setError,
  closeModal,
}: IEnable2FAModalProps): ReactElement | null => {
  const [step, setStep] = useState<STEPS>(STEPS.CURRENT_PASSWORD);
  const [password, setPassword] = useState('');

  useEffect(() => {
    if (data.secretCode) {
      setStep(STEPS.INFO);
    }
    if (data.success) {
      setStep(STEPS.SUCCESS);
    }
  }, [data.secretCode, data.success]);

  const handlePasswordSubmit = useCallback(
    (pass: string) => {
      setPassword(pass);
      init2FA(pass);
    },
    [init2FA]
  );

  const handleInfoSubmit = useCallback((e: React.FormEvent) => {
    e.preventDefault();
    setStep(STEPS.QR_CODE);
  }, []);

  const handleQRCodeSubmit = useCallback(() => {
    setStep(STEPS.TWO_FA_CODE);
  }, []);

  const handle2FACodeSubmit = useCallback(
    (code: string) => {
      enable2FA({ password, code });
    },
    [enable2FA, password]
  );
  const handleCloseModal = useCallback(() => {
    closeModal({ type: 'enable2FA' });
    setStep(STEPS.CURRENT_PASSWORD);
    setPassword('');
  }, [closeModal]);

  const handlePrevStep = useCallback(() => {
    setStep(PREV_STEPS[step]);
  }, [step]);

  return (
    <Modal
      onClose={handleCloseModal}
      onBack={
        step !== STEPS.CURRENT_PASSWORD && step !== STEPS.SUCCESS
          ? handlePrevStep
          : undefined
      }
    >
      {step === STEPS.CURRENT_PASSWORD && (
        <Enable2FAModalCurrentPassword
          userPassword={password}
          onSubmit={handlePasswordSubmit}
          setError={setError}
          error={data.error}
          loading={data.loading}
        />
      )}
      {step === STEPS.INFO && (
        <Enable2FAModalInfo onSubmit={handleInfoSubmit} />
      )}
      {step === STEPS.QR_CODE && (
        <Enable2FAModalQRCode
          onSubmit={handleQRCodeSubmit}
          secret={data.secretCode}
          email={email}
        />
      )}
      {step === STEPS.TWO_FA_CODE && (
        <Enable2FAModal2FACode
          error={data.error}
          onSubmit={handle2FACodeSubmit}
          loading={data.loading}
          setError={setError}
        />
      )}
      {step === STEPS.SUCCESS && (
        <Enable2FAModalSuccess onSubmit={handleCloseModal} />
      )}
    </Modal>
  );
};
