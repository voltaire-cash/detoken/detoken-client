import { Dispatch } from 'redux';
import { batch } from 'react-redux';
import { IState } from '~dux/rootDux';
import { setUser2FAModeAction } from '~dux/user/userDux';
import { updateModalData } from '~dux/modals/modalsDux';
import {
  init2FA as init2FARequest,
  enable2FA as enable2FARequest,
} from './Enable2FAModalApi';
import { getHash } from '~utils/password';
import { IError } from '~types/Error';
import { validatePassword, validate2FACode } from '~utils/validation';

type Action = ReturnType<typeof updateModalData | typeof setUser2FAModeAction>;

export const setError = (err: IError | null) => (
  dispatch: Dispatch<Action>
): void => {
  dispatch(updateModalData({ type: 'enable2FA', newData: { error: err } }));
};

export const init2FA = (password: string) => async (
  dispatch: Dispatch<Action>,
  getState: () => IState
): Promise<void> => {
  dispatch(updateModalData({ type: 'enable2FA', newData: { loading: true } }));
  const err = validatePassword(password);
  if (err) {
    dispatch(
      updateModalData({
        type: 'enable2FA',
        newData: {
          error: { field: 'password', key: err.key, params: err.params },
          loading: false,
        },
      })
    );
    return;
  }

  const { email } = getState().user;
  try {
    const { secretCode } = await init2FARequest({
      email,
      passwordHash: getHash(password),
    });

    dispatch(
      updateModalData({
        type: 'enable2FA',
        newData: { secretCode, loading: false },
      })
    );
  } catch (error) {
    dispatch(
      updateModalData({
        type: 'enable2FA',
        newData: {
          error,
          loading: false,
        },
      })
    );
  }
};

export interface IEnable2FA {
  password: string;
  code: string;
}

export const enable2FA = ({ password, code }: IEnable2FA) => async (
  dispatch: Dispatch<Action>,
  getState: () => IState
): Promise<void> => {
  dispatch(updateModalData({ type: 'enable2FA', newData: { loading: true } }));
  const err = validate2FACode(code);
  if (err) {
    dispatch(
      updateModalData({
        type: 'enable2FA',
        newData: {
          error: { field: 'code', key: err.key, params: err.params },
          loading: false,
        },
      })
    );
    return;
  }
  const { email } = getState().user;
  try {
    await enable2FARequest({ email, passwordHash: getHash(password), code });
    batch(() => {
      dispatch(setUser2FAModeAction(true));
      dispatch(
        updateModalData({
          type: 'enable2FA',
          newData: { secretCode: '', success: true, loading: false },
        })
      );
    });
  } catch (error) {
    dispatch(
      updateModalData({
        type: 'enable2FA',
        newData: { error, loading: false },
      })
    );
  }
};
