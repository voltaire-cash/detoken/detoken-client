import React, { FC, ReactElement, useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { FormLayout, Form, PinCode2FA, InputWrapper } from '~components';
import { IEnable2FAModalProps } from '../types';

interface IProps {
  onSubmit: (code: string) => void;
  error: IEnable2FAModalProps['data']['error'];
  loading: boolean;
  setError: IEnable2FAModalProps['setError'];
}

export const Enable2FAModal2FACode: FC<IProps> = ({
  onSubmit,
  error,
  loading,
  setError,
}: IProps): ReactElement => {
  const [code, setCode] = useState('');
  const { t } = useTranslation();

  const { key, field, params } = error || {};

  const handleChangeCode = useCallback(
    (value) => {
      setError(null);
      setCode(value);
    },
    [setError]
  );

  const handleEnterKeyDown = useCallback(() => onSubmit(code), [
    onSubmit,
    code,
  ]);

  const handleFormSubmit = useCallback(
    (e: React.FormEvent) => {
      e.preventDefault();
      onSubmit(code);
    },
    [code, onSubmit]
  );

  return (
    <FormLayout title={t('enable_2fa')} subtitle={t('enter_your_2fa_code')}>
      <Form
        onSubmit={handleFormSubmit}
        submitText={t('next_button')}
        steps={4}
        currentStep={4}
        submitDisabled={loading}
      >
        <InputWrapper
          label={t('two_fa_code')}
          error={field === 'code'}
          helperText={field === 'code' && key ? t(key, params) : ''}
        >
          <PinCode2FA
            value={code}
            onChange={handleChangeCode}
            onEnterKeyDown={handleEnterKeyDown}
            error={field === 'code'}
          />
        </InputWrapper>
      </Form>
    </FormLayout>
  );
};
