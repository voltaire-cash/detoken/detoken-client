import React, { FC, ReactElement, useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { IEnable2FAModalProps } from '../types';

import { FormLayout, Form, TextField } from '~components';

interface IProps {
  userPassword: string;
  onSubmit: (password: string) => void;
  setError: IEnable2FAModalProps['setError'];
  error: IEnable2FAModalProps['data']['error'];
  loading: IEnable2FAModalProps['data']['loading'];
}

export const Enable2FAModalCurrentPassword: FC<IProps> = ({
  userPassword,
  onSubmit,
  setError,
  error,
  loading,
}: IProps): ReactElement => {
  const [password, setPassword] = useState(userPassword);

  const { t } = useTranslation();

  const { field, key, params } = error || {};

  const handleInputChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setError(null);
      const { value } = e.currentTarget;
      setPassword(value);
    },
    [setError]
  );

  const handleFormSubmit = useCallback(
    (e: React.FormEvent) => {
      e.preventDefault();
      onSubmit(password);
    },
    [password, onSubmit]
  );
  return (
    <FormLayout
      title={t('enable_2fa')}
      subtitle={t('enter_your_password_to_enable_2fa')}
    >
      <Form
        onSubmit={handleFormSubmit}
        submitText={t('next_button')}
        steps={4}
        currentStep={1}
        submitDisabled={loading}
      >
        <TextField
          value={password}
          type="password"
          placeholder={t('current_password')}
          onChange={handleInputChange}
          error={field === 'password'}
          helperText={field === 'password' && key ? t(key, params) : ''}
        />
      </Form>
    </FormLayout>
  );
};
