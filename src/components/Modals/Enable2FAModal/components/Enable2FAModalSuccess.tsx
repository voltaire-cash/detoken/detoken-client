import React, { FC, ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import { FormLayout, Button } from '~components';
import styled from '~config/theme';
import SuccessIcon from '~icons/success.svg';

interface IProps {
  onSubmit: () => void;
}

export const Enable2FAModalSuccess: FC<IProps> = ({
  onSubmit,
}: IProps): ReactElement => {
  const { t } = useTranslation();

  return (
    <FormLayout icon={<StyledSuccess />}>
      <Text>{t('two_fa_successfully_enabled')}</Text>
      <Button onClick={onSubmit} fullWidth variant="secondary">
        {t('close_button')}
      </Button>
    </FormLayout>
  );
};

const Text = styled.div`
  text-align: center;
  font-size: calc(4rem / 3);
  font-weight: bold;
  margin-bottom: 30px;
`;

const StyledSuccess = styled(SuccessIcon as 'img')`
  fill: ${({ theme }): string => theme.icons.success};
`;
