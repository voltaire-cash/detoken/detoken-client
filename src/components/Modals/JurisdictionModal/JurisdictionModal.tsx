import React, { FC, ReactElement } from 'react';
import { Trans, useTranslation } from 'react-i18next';
import { Modal } from '~components';
import { IJurisdictionModal } from './types';
import styled from '~config/theme';
import { DETOKEN_LINK } from '~config/constants';
import FailSvg from '~icons/fail.svg';

export const JurisdictionModal: FC<IJurisdictionModal> = (): ReactElement => {
  const { t } = useTranslation();
  return (
    <Modal noClose>
      <Container>
        <IconContainer>
          <StyledFail />
        </IconContainer>
        <h3>{t('oops_something_went_wrong')}</h3>
        <p>
          <Trans i18nKey="prohibited_jurisdiction_text">
            You appear to be accessing Detoken from a
            <a
              href={`${DETOKEN_LINK}/prohibited-jurisdictions`}
              target="_blank"
              rel="noreferrer"
            >
              prohibited jurisdiction
            </a>
            .
          </Trans>
        </p>
      </Container>
    </Modal>
  );
};

const Container = styled.div`
  color: ${({ theme }): string => theme.text.primary};
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  height: 100%;
  text-align: center;
  & a {
    text-decoration: underline;
  }
`;

const StyledFail = styled(FailSvg as 'img')`
  fill: ${({ theme }): string => theme.icons.fail};
`;

const IconContainer = styled.div`
  margin-bottom: 25px;
  width: 60px;
  height: 60px;
  & > svg {
    width: 60px;
    height: 60px;
  }

  ${({ theme }): string => `
    ${theme.mediaDesktop}{
      width: 80px;
      height: 80px;
      & > svg {
        width: 80px;
        height: 80px;
      };
    };
  `};
`;
