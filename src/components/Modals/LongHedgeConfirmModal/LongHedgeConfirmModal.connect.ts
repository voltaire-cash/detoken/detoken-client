import { connect } from 'react-redux';
import { LongHedgeConfirmModal } from './LongHedgeConfirmModal';
import { IState } from '~dux/rootDux';
import { closeModal, IConfirmModalData } from '~dux/modals/modalsDux';
import { IMapState, IMapDispatch } from './types';
import { sendContract } from '~pages/DashboardPage/dashboardPageDux';
import { recalculateContract, closeSnackbar } from './LongHedgeConfirmModalDux';
import { oraclePriceSelector } from '~dux/app/appDux';

const mapDispatchToProps: IMapDispatch = {
  closeModal,
  sendContract,
  recalculateContract,
  closeSnackbar,
};

const mapStateToProps = (state: IState): IMapState => ({
  oraclePrice: oraclePriceSelector(state).toString(),
  data: state.modals.longHedgeConfirm.data as IConfirmModalData,
  loading: state.dashboardPage.isLoading,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LongHedgeConfirmModal);
