import React, {
  FC,
  ReactElement,
  useCallback,
  useState,
  useEffect,
  useRef,
} from 'react';
import { useTranslation } from 'react-i18next';
import Big from 'big.js';
import {
  FormLayout,
  Modal,
  Button,
  LinksContainer,
  LinksDot,
  Premium,
} from '~components';
import styled from '~config/theme';
import { ILongHedgeConfirmModal } from './types';
import { ContractDetails } from './components/ContractDetails';
import {
  SATOSHIS_IN_BCH,
  POSITION,
  CURRENCY_SYMBOL,
  BCH_DP,
  BLOCKS_IN_DAY,
  DETOKEN_LINK,
} from '~config/constants';
import { formatFiat } from '~utils/formatters';
import { getTranslationKey } from '~utils/translations';
import { getMargin } from '~utils/contract';
import { IConfirmModalData } from '~dux/modals/modalsDux';

export const LongHedgeConfirmModal: FC<ILongHedgeConfirmModal> = ({
  data,
  closeModal,
  sendContract,
  recalculateContract,
  oraclePrice,
  closeSnackbar,
  loading,
}: ILongHedgeConfirmModal): ReactElement | null => {
  const [showDetails, setShowDetails] = useState(false);
  const { t } = useTranslation();
  const isFirstRender = useRef(true);
  const handleShowDetails = useCallback(() => setShowDetails(true), []);
  const handleHideDetails = useCallback(() => setShowDetails(false), []);

  const handleCloseClick = useCallback(() => {
    handleHideDetails();
    closeModal({ type: 'longHedgeConfirm' });
    closeSnackbar();
    isFirstRender.current = false;
  }, [closeModal, handleHideDetails, closeSnackbar, isFirstRender]);

  const handleButtonClick = useCallback(() => {
    sendContract();
  }, [sendContract]);

  useEffect(() => {
    if (isFirstRender.current) {
      isFirstRender.current = false;
      return;
    }
    recalculateContract();
  }, [oraclePrice, recalculateContract]);

  const {
    startPrice,
    lowLiquidationMulti,
    maturityModifier,
    value,
    premium,
    fee,
    position,
    hedgeInputSats,
    longInputSats,
    dustCost,
    minerCost,
    premiumSatoshis,
    lowLiquidationPrice,
  } = data as IConfirmModalData;

  if (showDetails) {
    return (
      <ContractDetails
        data={data}
        onBackBtn={handleHideDetails}
        onClose={handleCloseClick}
      />
    );
  }

  const startPriceUSD = Big(startPrice).div(100).toString();
  const { HEDGE } = POSITION;

  const takerInputSatoshis =
    position === HEDGE ? hedgeInputSats : longInputSats;

  const feeBCH = Big(fee)
    .div(100)
    .times(takerInputSatoshis)
    .round(0, 3)
    .div(SATOSHIS_IN_BCH);

  const valueUSD = formatFiat(Big(value).times(startPriceUSD).toFixed(2), {
    zeroFraction: true,
    currency: CURRENCY_SYMBOL.USD,
  });

  const premiumBCH = Big(premiumSatoshis).abs().div(SATOSHIS_IN_BCH);
  const premiumUSD = premiumBCH.mul(startPriceUSD).toFixed(2);

  const extraCostBCH = Big(dustCost)
    .plus(minerCost)
    .plus(premiumSatoshis > 0 ? premiumSatoshis : 0)
    .div(SATOSHIS_IN_BCH);

  const totalCostBCH = Big(value).plus(feeBCH).plus(extraCostBCH);
  const totalCostUSD = formatFiat(
    totalCostBCH.times(startPriceUSD).round(2, 0).toString(),
    {
      zeroFraction: true,
      currency: CURRENCY_SYMBOL.USD,
    }
  );

  const lowLiquidationPriceUSD = formatFiat(
    Big(lowLiquidationPrice).div(100).toFixed(2),
    { zeroFraction: true }
  );
  const margin = `${getMargin(lowLiquidationMulti)}:1`;

  const duration = maturityModifier / BLOCKS_IN_DAY;

  // premium < 0 means maker pays to taker
  const premiumSign = {
    positive: premiumSatoshis < 0,
    negative: premiumSatoshis > 0,
    none: premiumSatoshis === 0,
  };

  return (
    <Modal onClose={handleCloseClick} wide>
      <FormLayout
        title={t('long_hedge_confirm_title', { position })}
        subtitle={
          position === HEDGE
            ? t('hedge_confirm_subtitle')
            : t('long_confirm_subtitle')
        }
      >
        <Data>
          <div>
            <ItemHeader>{t('contract_start_price')}</ItemHeader>
            <ItemData>
              {formatFiat(startPriceUSD, { zeroFraction: true })}
            </ItemData>
          </div>
          {position !== HEDGE && (
            <div>
              <ItemHeader>{t('liquidation_price')}</ItemHeader>
              <ItemData>{lowLiquidationPriceUSD}</ItemData>
            </div>
          )}
          {position !== HEDGE && (
            <div>
              <ItemHeader>{t('margin')}</ItemHeader>
              <ItemData>{margin}</ItemData>
            </div>
          )}
          <div>
            <ItemHeader>{t('duration')}</ItemHeader>
            <ItemData>
              {duration} {t(getTranslationKey(duration, 'day'))}
            </ItemData>
          </div>
          <div>
            <ItemHeader>{t('value')}</ItemHeader>
            <ItemData>
              {valueUSD} = {Big(value).toFixed(BCH_DP)} BCH
            </ItemData>
          </div>
          {premiumSign.positive && (
            <div>
              <ItemHeader>
                {t('premium')} {t('premium_earn_pay', { type: t('earn') })}
              </ItemHeader>
              <ItemData>
                {Big(premium).abs().times(100).toFixed(2)}% ={' '}
                <Premium positive={!Big(premiumUSD).eq(0)}>
                  {formatFiat(premiumUSD, {
                    zeroFraction: true,
                    currency: CURRENCY_SYMBOL.USD,
                  })}
                </Premium>
                {' = '}
                {premiumBCH.toFixed(BCH_DP)} BCH
              </ItemData>
            </div>
          )}
          <div>
            <ItemHeader>{t('total_contract_cost')}</ItemHeader>
            <ItemData>
              {totalCostUSD} = {totalCostBCH.toFixed(BCH_DP)} BCH
            </ItemData>
          </div>
        </Data>
        <StyledLinksContainer spaceBetween={10}>
          <Button variant="link" onClick={handleShowDetails}>
            {t('contract_details')}
          </Button>
          <LinksDot />
          <a
            href={`${DETOKEN_LINK}/about`}
            target="_blank"
            rel="noopener noreferrer"
          >
            {t('learn_more')}
          </a>
        </StyledLinksContainer>
        <Button onClick={handleButtonClick} fullWidth disabled={loading}>
          {t('confirm_button')}
        </Button>
      </FormLayout>
    </Modal>
  );
};

const Data = styled.div`
  text-align: left;
  width: 100%;
  box-sizing: border-box;
  border-radius: 4px;
  line-height: 21px;
  font-size: 1rem;
  margin-bottom: 15px;
  border: 1px solid ${({ theme }): string => theme.border.default};
  background: ${({ theme }): string => theme.background.tertiary};
  padding: 20px;
  & > div:not(:last-of-type) {
    margin-bottom: 15px;
  }
`;

const ItemHeader = styled.div`
  font-weight: bold;
  color: ${({ theme }): string => theme.text.header};
`;

const ItemData = styled.span`
  color: ${({ theme }): string => theme.text.primary};
  overflow-wrap: break-word;
`;

const StyledLinksContainer = styled(LinksContainer)`
  margin-top: 0;
  margin-bottom: 30px;
  & > button {
    font-size: 14px;
    height: auto;
    &:first-of-type {
      margin-bottom: 7px;
    }
  }
  ${({ theme }): string => `
    ${theme.mediaTablet}{
      margin-bottom: 30px;
      & > button:first-of-type {
        margin-bottom: 0;
      }
    }
  `}
`;
