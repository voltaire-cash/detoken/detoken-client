import { Dispatch } from 'react';
import i18next from 'i18next';
import { IState } from '~dux/rootDux';
import {
  addSnackbarAction,
  removeSnackbarAction,
} from '~dux/notifications/notificationsDux';
import { IContractData } from '~pages/DashboardPage/dashboardPageApi';
import {
  setMaxAmount,
  showContractInfo,
} from '~pages/DashboardPage/dashboardPageDux';

type Action = ReturnType<
  typeof addSnackbarAction | typeof removeSnackbarAction
>;

export const recalculateContract = () => async (
  dispatch: Dispatch<Action>,
  getState: () => IState
): Promise<void> => {
  const state = getState();
  const { amount } = state.dashboardPage;
  const { id, takerSide } = state.dashboardPage.contractData as IContractData;
  const { isMaxAmount } = state.dashboardPage;
  let maxAmount;
  if (isMaxAmount) {
    maxAmount = await setMaxAmount({ position: takerSide, productId: id })(
      // @ts-expect-error
      dispatch,
      getState
    );
  }
  const success = await showContractInfo({
    amount: isMaxAmount && !!maxAmount ? maxAmount : amount,
    position: takerSide,
    productId: id,
    // @ts-expect-error
  })(dispatch, getState);
  if (success) {
    dispatch(
      addSnackbarAction({
        message: i18next.t('contract_was_recalculated'),
        variant: 'info',
      })
    );
  }
};

export const closeSnackbar = () => (dispatch: Dispatch<Action>): void => {
  dispatch(removeSnackbarAction(''));
};
