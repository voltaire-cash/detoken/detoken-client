import { CloseModalAction, IModalsState } from '~dux/modals/modalsDux';
import { IDashboardPageState } from '~pages/DashboardPage/dashboardPageDux';

export interface IMapState {
  data: IModalsState['longHedgeConfirm']['data'];
  oraclePrice: string;
  loading: IDashboardPageState['isLoading'];
}

export interface IMapDispatch {
  closeModal: (props: CloseModalAction) => void;
  sendContract: () => void;
  recalculateContract: () => void;
  closeSnackbar: () => void;
}

export interface ILongHedgeConfirmModal extends IMapState, IMapDispatch {}
