import { connect } from 'react-redux';
import { IState } from '~dux/rootDux';
import { Modals } from './Modals';

interface IMapState {
  modals: IState['modals'];
}

const mapStateToProps = (state: IState): IMapState => ({
  modals: state.modals,
});

export default connect(mapStateToProps)(Modals);
