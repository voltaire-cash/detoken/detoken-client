import React, { FC, ReactElement } from 'react';
import { IState } from '~dux/rootDux';
import { WithdrawalDetailsModal } from './WithdrawalDetailsModal';
import { DepositDetailsModal } from './DepositDetailsModal';
import { DepositModal } from './DepositModal';
import { WithdrawModal } from './WithdrawModal';
import { Enable2FAModal } from './Enable2FAModal';
import { Disable2FAModal } from './Disable2FAModal';
import { SignoutModal } from './SignoutModal';
import { LongHedgeConfirmModal } from './LongHedgeConfirmModal';
import { PositionModal } from './PositionModal';
import { JurisdictionModal } from './JurisdictionModal';
import { SeedWordsModal } from './SeedWordsModal';
import { EmailInUseModal } from './EmailInUseModal';
import { SinInUseModal } from './SinInUseModal';
import { NewVersionModal } from './NewVersionModal';

interface IProps {
  modals: IState['modals'];
}

export const Modals: FC<IProps> = ({ modals }: IProps): ReactElement => (
  <>
    {modals.position.isOpen ? <PositionModal /> : null}
    {modals.deposit.isOpen ? <DepositModal /> : null}
    {modals.withdraw.isOpen ? <WithdrawModal /> : null}
    {modals.withdrawalDetails.isOpen ? <WithdrawalDetailsModal /> : null}
    {modals.depositDetails.isOpen ? <DepositDetailsModal /> : null}
    {modals.enable2FA.isOpen ? <Enable2FAModal /> : null}
    {modals.disable2FA.isOpen ? <Disable2FAModal /> : null}
    {modals.signout.isOpen ? <SignoutModal /> : null}
    {modals.longHedgeConfirm.isOpen ? <LongHedgeConfirmModal /> : null}
    {modals.jurisdiction.isOpen ? <JurisdictionModal /> : null}
    {modals.seedWords.isOpen ? <SeedWordsModal /> : null}
    {modals.emailInUse.isOpen ? <EmailInUseModal /> : null}
    {modals.sinInUse.isOpen ? <SinInUseModal /> : null}
    {modals.newVersion.isOpen ? <NewVersionModal /> : null}
  </>
);
