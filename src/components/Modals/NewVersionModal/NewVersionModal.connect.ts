import { connect } from 'react-redux';
import { NewVersionModal } from './NewVersionModal';
import { closeModal } from '~dux/modals/modalsDux';

const mapDispatchToProps = {
  closeModal,
};

export default connect(null, mapDispatchToProps)(NewVersionModal);
