import React, { FC, ReactElement, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { FormLayout, Button, Modal } from '~components';
import { INewVersionModal } from './types';
import styled from '~config/theme';

export const NewVersionModal: FC<INewVersionModal> = ({
  closeModal,
}: INewVersionModal): ReactElement | null => {
  const { t } = useTranslation();

  const handleCancelClick = useCallback(() => {
    closeModal({ type: 'newVersion' });
  }, [closeModal]);
  const handleReloadClick = useCallback(() => {
    document.location.reload();
  }, []);
  return (
    <Modal onClose={handleCancelClick}>
      <FormLayout
        title={t('new_version_header')}
        subtitle={t('new_version_text')}
      >
        <Buttons>
          <Button onClick={handleCancelClick} variant="secondary">
            {t('cancel')}
          </Button>
          <Button onClick={handleReloadClick}>{t('reload_button')}</Button>
        </Buttons>
      </FormLayout>
    </Modal>
  );
};

const Buttons = styled.div`
  & > button:first-of-type {
    margin-right: 10px;
  }
  & > button:nth-of-type(2) {
    margin-left: 10px;
  }
`;
