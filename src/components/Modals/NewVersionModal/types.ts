import { CloseModalAction } from '~dux/modals/modalsDux';

export interface IMapDispatch {
  closeModal: (props: CloseModalAction) => void;
}

export type INewVersionModal = IMapDispatch;
