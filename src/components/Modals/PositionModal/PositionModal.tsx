import React, { FC, ReactElement, useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';
import Big from 'big.js';
import {
  FormLayout,
  Modal,
  LinksContainer,
  LinksDot,
  Button,
  Premium,
} from '~components';
import { IPositionModal } from './types';
import styled from '~config/theme';
import { PositionDetails } from './components/PositionDetails';
import { calculateContractValues } from '~utils/contract';
import { POSITION, DETOKEN_LINK } from '~config/constants';
import { getTranslationKey, getDurationTranslation } from '~utils/translations';
import { IPositionModalData } from '~dux/modals/modalsDux';
import { formatFiat } from '~utils/formatters';

export const PositionModal: FC<IPositionModal> = ({
  closeModal,
  data,
}: IPositionModal): ReactElement | null => {
  const [showDetails, setShowDetails] = useState(false);

  const handleShowDetails = useCallback(() => setShowDetails(true), []);
  const handleHideDetails = useCallback(() => setShowDetails(false), []);

  const handleCloseClick = useCallback(() => {
    handleHideDetails();
    closeModal({ type: 'position' });
  }, [closeModal, handleHideDetails]);

  const { t } = useTranslation();

  const {
    metadata,
    parameters,
    simulation,
    contract,
    oraclePrice,
    blockHeight,
    settlement,
  } = data as IPositionModalData;

  const contractData = calculateContractValues({
    metadata,
    parameters,
    simulation,
    contract,
    oraclePrice,
    blockHeight,
    settlement,
  });

  const {
    position,
    startValueUSD,
    startValueBCH,
    isSettled,
    currentValueBCH,
    currentValueUSD,
    startPrice,
    lowLiquidationPrice,
    margin,
    dayDuration,
    expiration,
    profitBCH,
    settlePriceUSD,
    settleReason,
    settleValueBCH,
    settleValueUSD,
    premium,
    premiumUSD,
    leftBlocks,
  } = contractData;

  const [days, hours] = expiration;
  const expirationStr =
    days || hours
      ? `~${getDurationTranslation(expiration)}`
      : t('less_then_one_hour');

  const expiresIn = `${leftBlocks} ${t(
    getTranslationKey(leftBlocks, 'block')
  )} (${expirationStr})`;

  if (showDetails) {
    return (
      <PositionDetails
        data={data}
        onClose={handleCloseClick}
        onBackBtn={handleHideDetails}
        contract={contractData}
      />
    );
  }

  const profitSign = {
    positive: Big(profitBCH).gt(0),
    negative: Big(profitBCH).lt(0),
  };

  return (
    <Modal onClose={handleCloseClick} wide>
      <FormLayout title={t(`position_title`, { side: t(position) })}>
        <Data>
          <div>
            <ItemHeader>{t('start_value')}</ItemHeader>
            <ItemData>
              {startValueUSD} = {startValueBCH} BCH
            </ItemData>
          </div>
          {!isSettled ? (
            <div>
              <ItemHeader>{t('current_value')}</ItemHeader>
              <ItemData>
                {currentValueUSD} = {currentValueBCH} BCH
              </ItemData>
            </div>
          ) : null}
          {isSettled ? (
            <div>
              <ItemHeader>{t('settle_value')}</ItemHeader>
              <ItemData>
                {Big(settleValueBCH).eq(0) ? (
                  t('not_available')
                ) : (
                  <>
                    {formatFiat(settleValueUSD, { zeroFraction: true })} ={' '}
                    {settleValueBCH} BCH
                  </>
                )}
              </ItemData>
            </div>
          ) : null}
          <div>
            <ItemHeader>{t('profit_loss')}</ItemHeader>
            <ItemData>
              {isSettled && Big(settleValueBCH).eq(0) ? (
                t('not_available')
              ) : (
                <Premium
                  positive={profitSign.positive}
                  negative={profitSign.negative}
                >
                  {profitBCH} BCH
                </Premium>
              )}
            </ItemData>
          </div>
          <div>
            <ItemHeader>{t('start_price')}</ItemHeader>
            <ItemData>${startPrice}</ItemData>
          </div>
          {isSettled ? (
            <div>
              <ItemHeader>{t('settle_price')}</ItemHeader>
              <ItemData>
                {Big(settleValueBCH).eq(0) ? (
                  t('not_available')
                ) : (
                  <>{formatFiat(settlePriceUSD, { zeroFraction: true })}</>
                )}
              </ItemData>
            </div>
          ) : null}
          {isSettled && (
            <div>
              <ItemHeader>{t('settle_reason')}</ItemHeader>
              <ItemData>{t(settleReason)}</ItemData>
            </div>
          )}
          {position === POSITION.LONG && !isSettled && (
            <div>
              <ItemHeader>{t('liquidation_price')}</ItemHeader>
              <ItemData>{lowLiquidationPrice}</ItemData>
            </div>
          )}
          {Big(premium).lt(0) && !Big(premiumUSD).eq(0) && (
            <div>
              <ItemHeader>{t('premium')}</ItemHeader>
              <ItemData>
                <Premium positive>
                  {formatFiat(premiumUSD, {
                    zeroFraction: true,
                  })}
                </Premium>
              </ItemData>
            </div>
          )}
          {position === POSITION.LONG && (
            <div>
              <ItemHeader>{t('margin')}</ItemHeader>
              <ItemData>{margin}</ItemData>
            </div>
          )}
          {!isSettled && (
            <div>
              <ItemHeader>{t('duration')}</ItemHeader>
              <ItemData>
                {dayDuration} {t(getTranslationKey(dayDuration, 'day'))}
              </ItemData>
            </div>
          )}
          {!isSettled ? (
            <div>
              <ItemHeader>{t('expires_in')}</ItemHeader>
              <ItemData>{expiresIn}</ItemData>
            </div>
          ) : null}
        </Data>
        <StyledLinksContainer spaceBetween={10}>
          <Button variant="link" onClick={handleShowDetails}>
            {t('contract_details')}
          </Button>
          <LinksDot />
          <a
            href={`${DETOKEN_LINK}/about`}
            target="_blank"
            rel="noopener noreferrer"
          >
            {t('learn_more')}
          </a>
        </StyledLinksContainer>
        <Button variant="secondary" fullWidth onClick={handleCloseClick}>
          {t('close')}
        </Button>
      </FormLayout>
    </Modal>
  );
};

const Data = styled.div`
  text-align: left;
  width: 100%;
  box-sizing: border-box;
  border-radius: 4px;
  line-height: 21px;
  font-size: 1rem;
  margin-bottom: 15px;
  border: 1px solid ${({ theme }): string => theme.border.default};
  background: ${({ theme }): string => theme.background.tertiary};
  padding: 20px;
  & > div:not(:last-of-type) {
    margin-bottom: 15px;
  }
`;

const ItemHeader = styled.div`
  font-weight: bold;
  color: ${({ theme }): string => theme.text.header};
`;

const ItemData = styled.span`
  color: ${({ theme }): string => theme.text.primary};
  overflow-wrap: break-word;
`;

const StyledLinksContainer = styled(LinksContainer)`
  margin-top: 0;
  margin-bottom: 30px;
  & > button {
    font-size: 14px;
    height: auto;
    &:first-of-type {
      margin-bottom: 7px;
    }
  }
  ${({ theme }): string => `
    ${theme.mediaTablet}{
      margin-bottom: 30px;
      & > button:first-of-type {
        margin-bottom: 0;
      }
    }
  `}
`;
