import { CloseModalAction, IPositionModalData } from '~dux/modals/modalsDux';
import { IState } from '~dux/rootDux';

export interface IMapState {
  data: IPositionModalData;
  oraclePrice: IState['app']['oraclePrice']['price'];
  blockHeight: IState['app']['oraclePrice']['blockHeight'];
}

export interface IMapDispatch {
  closeModal: (props: CloseModalAction) => void;
}

export interface IPositionModal extends IMapState, IMapDispatch {}
