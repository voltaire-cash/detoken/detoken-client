import { connect } from 'react-redux';
import { closeModal } from '~dux/modals/modalsDux';
import { IMapState, IMapDispatch } from './types';
import { IState } from '~dux/rootDux';
import { SeedWordsModal } from './SeedWordsModal';

const mapDispatchToProps: IMapDispatch = {
  closeModal,
};

const mapStateToProps = (state: IState): IMapState => ({
  encryptedMnemonic: state.user.encryptedMnemonic,
});

export default connect(mapStateToProps, mapDispatchToProps)(SeedWordsModal);
