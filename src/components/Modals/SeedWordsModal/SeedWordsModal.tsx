import React, { FC, ReactElement, useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { ISeedWordsModal } from './types';
import { Modal, FormLayout, TextField, Button } from '~components';
import styled from '~config/theme';
import { decrypt } from '~utils/wallet';

export const SeedWordsModal: FC<ISeedWordsModal> = ({
  closeModal,
  encryptedMnemonic,
}: ISeedWordsModal): ReactElement => {
  const { t } = useTranslation();
  const [mnemonic, setMnemonic] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');

  const handleCloseClick = useCallback(() => {
    closeModal({ type: 'seedWords' });
  }, [closeModal]);

  const handlePasswordChange = useCallback(
    (e) => {
      setPassword(e.currentTarget.value);
      setError('');
    },
    [setError]
  );

  const handleShowButtonClick = useCallback(() => {
    if (!password) {
      setError('value_is_empty');
      return;
    }
    let decrypted;
    try {
      decrypted = decrypt(encryptedMnemonic, password);
      // eslint-disable-next-line no-empty
    } catch (e) {}
    if (!decrypted) {
      setError('wrong_password');
      return;
    }
    setMnemonic(decrypted);
  }, [setError, password, setMnemonic, encryptedMnemonic]);

  const handleKeyPress = useCallback(
    (e: KeyboardEvent) => {
      if (['Enter', 13].includes(e.key)) {
        handleShowButtonClick();
      }
    },
    [handleShowButtonClick]
  );

  return (
    <Modal onClose={handleCloseClick}>
      <FormLayout title={t('seed_words_title')}>
        {!mnemonic ? (
          <>
            <Subtitle>{t('enter_wallet_password_to_view_seed_words')}</Subtitle>
            <TextField
              id="password"
              name="password"
              type="password"
              placeholder={t('password')}
              value={password}
              onChange={handlePasswordChange}
              error={!!error}
              helperText={t(error)}
              inputProps={{ onKeyPress: handleKeyPress }}
            />
            <StyledButton fullWidth onClick={handleShowButtonClick}>
              {t('seed_words_button')}
            </StyledButton>
          </>
        ) : (
          <>
            <SeedWords>{mnemonic}</SeedWords>
            <Button variant="secondary" onClick={handleCloseClick} fullWidth>
              {t('close_button')}
            </Button>
          </>
        )}
      </FormLayout>
    </Modal>
  );
};

const SeedWords = styled.div`
  margin-bottom: 30px;
  width: 100%;
  max-width: 100%;
  border: 3px solid ${({ theme }): string => theme.border.copySeedWords};
  border-radius: 3px;
  background-color: transparent;
  font-size: 1rem;
  text-align: center;
  padding: 10px;
  resize: none;
  box-sizing: border-box;
  color: ${({ theme }): string => theme.text.primary};
`;

const Subtitle = styled.div`
  margin-bottom: 30px;
`;

const StyledButton = styled(Button)`
  margin-top: 14px;
`;
