import { CloseModalAction } from '~dux/modals/modalsDux';
import { IState } from '~dux/rootDux';

export interface IMapDispatch {
  closeModal: (props: CloseModalAction) => void;
}

export interface IMapState {
  encryptedMnemonic: IState['user']['encryptedMnemonic'];
}

export interface ISeedWordsModal extends IMapDispatch, IMapState {}
