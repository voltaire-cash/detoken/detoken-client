import { connect } from 'react-redux';
import { SinInUseModal } from './SinInUseModal';
import { closeModal } from '~dux/modals/modalsDux';

const mapDispatchToProps = {
  closeModal,
};

export default connect(null, mapDispatchToProps)(SinInUseModal);
