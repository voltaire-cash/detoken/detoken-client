import React, { FC, ReactElement, useCallback } from 'react';
import { Trans, useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { Button, Modal } from '~components';
import { ISinInUseModal } from './types';
import styled from '~config/theme';
import { IconContainer } from '~components/FormLayout/FormLayout';
import FailSvg from '~icons/fail.svg';

export const SinInUseModal: FC<ISinInUseModal> = ({
  closeModal,
}: ISinInUseModal): ReactElement | null => {
  const { t } = useTranslation();

  const handleCancelClick = useCallback(() => {
    closeModal({ type: 'sinInUse' });
  }, [closeModal]);

  return (
    <Modal onClose={handleCancelClick}>
      <Container>
        <IconContainer>
          <StyledFail />
        </IconContainer>
        <Title>{t('mnemonic_already_registered')}</Title>
        <Subtitle>
          <Trans i18nKey="wallet_with_sin_already_exists">
            A wallet with these seed words already exists, please try to{' '}
            <Link to="/signin">sign in</Link> or{' '}
            <Link to="/forgotpass">reset your password</Link>.
          </Trans>
        </Subtitle>
        <Button variant="secondary" onClick={handleCancelClick}>
          {t('close_button')}
        </Button>
      </Container>
    </Modal>
  );
};

const Container = styled.div`
  color: ${({ theme }): string => `${theme.text.primary}`};
  display: flex;
  flex-direction: column;
  align-items: center;
  max-width: 400px;
  box-sizing: border-box;
  text-align: center;
`;

const Title = styled.div`
  font-size: calc(4rem / 3);
  color: ${({ theme }): string => theme.text.header};
  margin-bottom: 10px;
  font-weight: bold;
`;

const Subtitle = styled.div`
  margin-bottom: 30px;
  text-align: center;

  & > a {
    text-decoration: underline;
  }
`;

const StyledFail = styled(FailSvg as 'img')`
  fill: ${({ theme }): string => theme.icons.fail};
`;
