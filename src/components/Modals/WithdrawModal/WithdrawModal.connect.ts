import { connect } from 'react-redux';
import { IState } from '~dux/rootDux';
import { WithdrawModal } from './WithdrawModal';
import { IMapState } from './types';
import { closeModal, IWithdrawData } from '~dux/modals/modalsDux';
import { setError, prepareTransaction, withdraw } from './WithdrawModalDux';

// TODO: remove walletBalanceSelector when fiat balance will be in redux
import { oraclePriceSelector, exchangeRatesSelector } from '~dux/app/appDux';
import { walletBalanceSelector } from '~dux/wallet/walletDux';

const mapStateToProps = (state: IState): IMapState => ({
  data: state.modals.withdraw.data as IWithdrawData,
  exchangeRates: exchangeRatesSelector(state),
  oraclePrice: oraclePriceSelector(state),
  balance: walletBalanceSelector(state),
  currency: state.app.userCurrency,
});

const mapDispatchToProps = {
  closeModal,
  setError,
  prepareTransaction,
  withdraw,
};

export default connect(mapStateToProps, mapDispatchToProps)(WithdrawModal);
