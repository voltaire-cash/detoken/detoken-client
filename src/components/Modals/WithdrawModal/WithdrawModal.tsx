import React, {
  FC,
  ReactElement,
  useCallback,
  useEffect,
  useState,
} from 'react';
import Big from 'big.js';
import { IWithdrawModalProps, PREV_STEPS, STEPS } from './types';
import { WithdrawAmount } from './components/WithdrawAmount';
import { WithdrawAddress } from './components/WithdrawAddress';
import { WithdrawConfirm } from './components/WithdrawConfirm';
import { WithdrawSuccess } from './components/WithdrawSuccess';

import { Modal } from '~components';
import {
  CURRENCY_SYMBOL,
  MIN_WITHDRAWAL,
  SATOSHIS_IN_BCH,
} from '~config/constants';
import { ERROR_KEYS } from '~types/Error';
import { validateBchAddress } from '~utils/validation';
import { ISignedTransaction } from '~components/Modals/WithdrawModal/withdrawModalApi';
import { formatFiat } from '~utils/formatters';

export const WithdrawModal: FC<IWithdrawModalProps> = ({
  closeModal,
  exchangeRates,
  oraclePrice,
  balance,
  setError,
  data,
  prepareTransaction,
  withdraw,
  currency,
}: IWithdrawModalProps): ReactElement | null => {
  const [step, setStep] = useState<STEPS>(STEPS.AMOUNT);
  const [amount, setAmount] = useState('');
  const [address, setAddress] = useState('');
  const [transaction, setTransaction] = useState<ISignedTransaction | null>(
    null
  );

  const getFiat = (bch: string | number | Big): string =>
    formatFiat(
      Big(bch).mul(oraclePrice).mul(exchangeRates[currency]).toString(),
      {
        currency: CURRENCY_SYMBOL[currency],
        zeroFraction: true,
      }
    );

  useEffect(() => {
    if (data.success) {
      setStep(STEPS.SUCCESS);
    }
  }, [data.success]);

  const handleCloseModal = useCallback(() => {
    closeModal({ type: 'withdraw' });
    setStep(STEPS.AMOUNT);
    setAmount('');
    setAddress('');
    setTransaction(null);
  }, [closeModal]);

  const resetError = useCallback(() => setError(null), [setError]);

  const handleAmountSubmit = useCallback(
    (value: string): void => {
      if ((value.match(/\./g) || []).length > 1) {
        setError({
          field: 'amount',
          key: ERROR_KEYS.INVALID_VALUE,
        });
        return;
      }
      if (!value) {
        setError({
          field: 'amount',
          key: ERROR_KEYS.EMPTY_VALUE,
        });
        return;
      }
      if (Big(value).gt(balance)) {
        setError({
          field: 'amount',
          key: ERROR_KEYS.INSUFFICIENT_FUNDS,
        });
        return;
      }

      if (Big(value).times(SATOSHIS_IN_BCH).lt(MIN_WITHDRAWAL)) {
        setError({
          field: 'amount',
          key: ERROR_KEYS.AMOUNT_BELOW_MINIMUM,
        });
        return;
      }
      setAmount(value);
      setStep(STEPS.ADDRESS);
    },
    [balance, setError]
  );

  const handleAddressSubmit = useCallback(
    (value: string) => {
      if (!value) {
        setError({
          field: 'address',
          key: ERROR_KEYS.EMPTY_VALUE,
        });
        return;
      }
      const err = validateBchAddress(value);
      if (err) {
        setError({
          key: err.key,
          field: 'address',
        });
        return;
      }
      setAddress(value);
      const signedTransaction = prepareTransaction({
        amount,
        sendAddress: value,
      });
      if (signedTransaction) {
        setTransaction(signedTransaction);
        setStep(STEPS.CONFIRM);
      }
    },
    [setError, prepareTransaction, amount]
  );

  const handleWithdrawSubmit = useCallback(() => {
    withdraw(transaction as ISignedTransaction);
  }, [transaction, withdraw]);

  const handleModalBack = useCallback(() => {
    setStep(PREV_STEPS[step]);
  }, [step]);

  return (
    <Modal
      onClose={handleCloseModal}
      onBack={
        step === STEPS.ADDRESS || step === STEPS.CONFIRM
          ? handleModalBack
          : undefined
      }
      wide={step === STEPS.CONFIRM}
    >
      {step === STEPS.AMOUNT && (
        <WithdrawAmount
          balance={balance}
          onSubmit={handleAmountSubmit}
          error={data.error}
          resetError={resetError}
          userAmount={amount}
        />
      )}
      {step === STEPS.ADDRESS && (
        <WithdrawAddress
          error={data.error}
          resetError={resetError}
          onSubmit={handleAddressSubmit}
          userAddress={address}
        />
      )}
      {step === STEPS.CONFIRM && (
        <WithdrawConfirm
          transaction={transaction as ISignedTransaction}
          address={address}
          getFiat={getFiat}
          onSubmit={handleWithdrawSubmit}
          loading={data.loading}
        />
      )}
      {step === STEPS.SUCCESS && (
        <WithdrawSuccess
          hash={transaction?.hash}
          closeModal={handleCloseModal}
          transaction={transaction}
        />
      )}
    </Modal>
  );
};
