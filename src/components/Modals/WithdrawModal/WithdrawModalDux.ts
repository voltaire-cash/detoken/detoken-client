import Big from 'big.js';
import { Dispatch } from 'react';
import { batch } from 'react-redux';
import { createSendTransaction, signSendTransaction } from '~utils/wallet';

import { sendWSMessageAction } from '~dux/app/appDux';
import {
  getAddressesSelector,
  getPrivateKeysSelector,
} from '~dux/user/userDux';
import {
  removeUTXO,
  addSpentUTXOs,
  removeSpentUTXOs,
  UTXOsSelector,
} from '~dux/wallet/walletDux';
import { ERROR_KEYS, IError } from '~types/Error';
import { IState } from '~dux/rootDux';
import { updateModalData } from '~dux/modals/modalsDux';
import { addSnackbarAction } from '~dux/notifications/notificationsDux';
import { broadcastTransaction, ISignedTransaction } from './withdrawModalApi';

export interface IPrepareTransaction {
  sendAddress: string;
  amount: string;
}

type Actions = ReturnType<
  | typeof sendWSMessageAction
  | typeof removeUTXO
  | typeof updateModalData
  | typeof addSnackbarAction
  | typeof addSpentUTXOs
  | typeof removeSpentUTXOs
>;

export const setError = (err: IError | null) => (
  dispatch: Dispatch<Actions>
): void => {
  dispatch(
    updateModalData({ type: 'withdraw', newData: { error: err || {} } })
  );
};

export const prepareTransaction = ({
  amount,
  sendAddress,
}: IPrepareTransaction) => (
  dispatch: Dispatch<Actions>,
  getState: () => IState
): ISignedTransaction | null => {
  dispatch(updateModalData({ type: 'withdraw', newData: { loading: true } }));
  const state = getState();

  const { changeAddress } = getAddressesSelector(state);
  const UTXOs = UTXOsSelector(state);

  let signedTransaction = null;

  try {
    const { activePrivateKey, changePrivateKey } = getPrivateKeysSelector(
      state
    );
    const transaction = createSendTransaction({
      amount: Big(amount),
      sendAddress,
      changeAddress,
      UTXOs,
    });

    signedTransaction = signSendTransaction(transaction, [
      changePrivateKey,
      activePrivateKey,
    ]);
  } catch (e) {
    dispatch(
      addSnackbarAction({
        variant: 'error',
        message: ERROR_KEYS.COMMON,
      })
    );
  }
  dispatch(updateModalData({ type: 'withdraw', newData: { loading: false } }));
  return signedTransaction;
};

export const withdraw = (transaction: ISignedTransaction) => async (
  dispatch: Dispatch<Actions>
): Promise<void> => {
  dispatch(updateModalData({ type: 'withdraw', newData: { loading: true } }));
  const spentUTXOs = transaction.inputs.map(
    (input) => `${input.prevTxId}:${input.outputIndex}`
  );
  dispatch(addSpentUTXOs(spentUTXOs));
  try {
    await broadcastTransaction({ transaction });
    batch(() => {
      transaction.inputs.forEach((input) => {
        dispatch(removeUTXO(`${input.prevTxId}:${input.outputIndex}`));
      });
      dispatch(
        updateModalData({ type: 'withdraw', newData: { success: true } })
      );
    });
  } catch (e) {
    dispatch(removeSpentUTXOs(spentUTXOs));
    dispatch(
      addSnackbarAction({
        variant: 'error',
        message: ERROR_KEYS.COMMON,
      })
    );
  }
  dispatch(updateModalData({ type: 'withdraw', newData: { loading: false } }));
};
