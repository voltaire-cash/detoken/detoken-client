import React, { FC, ReactElement, useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { FormLayout, Form, TextField } from '~components';
import styled from '~config/theme';
import { IError } from '~types/Error';
import { IWithdrawModalProps } from '../types';
import { UserBalance } from '~components/UserBalance';

interface IProps {
  balance: IWithdrawModalProps['balance'];
  onSubmit: (amount: string) => void;
  error: IError | null;
  resetError: () => void;
  userAmount: string;
}

export const WithdrawAmount: FC<IProps> = ({
  balance,
  onSubmit,
  error,
  resetError,
  userAmount,
}: IProps): ReactElement => {
  const { t } = useTranslation();
  const [amount, setAmount] = useState(userAmount || '');

  const handleMaxClick = useCallback(() => setAmount(balance.toString()), [
    setAmount,
    balance,
  ]);

  const handleAmountChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>): void => {
      const { value } = e.currentTarget;
      resetError();
      setAmount(
        value
          .replace(/[^0-9.]/, '')
          .replace(/^0+([^.])/, '$1')
          .replace(/^0+/, '0')
          .replace(/^\./, '0.')
      );
    },
    [setAmount, resetError]
  );

  const handleFormSubmit = useCallback(
    (e: React.FormEvent): void => {
      e.preventDefault();
      onSubmit(amount.replace(/\.$/, ''));
    },
    [onSubmit, amount]
  );

  const { key, field } = error || {};

  return (
    <FormLayout
      title={t('withdraw_header')}
      subtitle={t('enter_amount_you_wish_to_transfer', { currency: 'BCH' })}
    >
      <MainText>
        <UserBalance />
      </MainText>
      <Form
        submitText={t('continue_button')}
        onSubmit={handleFormSubmit}
        steps={3}
        currentStep={1}
      >
        <TextField
          value={amount}
          onChange={handleAmountChange}
          inputProps={{
            btnText: t('max_button'),
            btnClick: handleMaxClick,
            key: 'amount',
          }}
          placeholder={t('amount')}
          error={field === 'amount'}
          helperText={field === 'amount' && key ? t(key) : ''}
        />
      </Form>
    </FormLayout>
  );
};

const MainText = styled.div`
  width: 100%;
  text-align: left;
  margin-bottom: 8px;
`;
