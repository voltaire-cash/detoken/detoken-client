import React, { FC, ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import Big from 'big.js';
import styled from '~config/theme';
import { FormLayout, Steps, Button } from '~components';
import { SATOSHIS_IN_BCH } from '~config/constants';
import { ISignedTransaction } from '~components/Modals/WithdrawModal/withdrawModalApi';

interface IProps {
  transaction: ISignedTransaction;
  address: string;
  getFiat: (bch: string | number | Big) => string;
  onSubmit: () => void;
  loading: boolean;
}

export const WithdrawConfirm: FC<IProps> = ({
  transaction,
  address,
  getFiat,
  onSubmit,
  loading,
}: IProps): ReactElement => {
  const { t } = useTranslation();
  const inputSatoshis = transaction.inputs.reduce(
    (acc, input) => acc.plus(input.output.satoshis),
    Big(0)
  );
  const outputSatoshis = transaction.outputs.reduce(
    (acc, output) => acc.plus(output.satoshis),
    Big(0)
  );

  const fee = inputSatoshis.minus(outputSatoshis).div(SATOSHIS_IN_BCH);
  // outputs[0] is output related to external address
  const total = Big(transaction.outputs[0].satoshis)
    .div(SATOSHIS_IN_BCH)
    .plus(fee);

  return (
    <FormLayout
      title={t('withdraw_header')}
      subtitle={t('confirm_your_transfer')}
    >
      <Wrapper>
        <Item>
          <Header>{t('address')}</Header>
          <Data>{address}</Data>
        </Item>
        <Item>
          <Header>{t('amount')}</Header>
          <Data>
            {total.minus(fee).toString()} BCH{' '}
            <span>~ {getFiat(total.minus(fee))}</span>
          </Data>
        </Item>
        <Item>
          <Header>{t('fee')}</Header>
          <Data>
            {fee.toString()} BCH <span>~ {getFiat(fee)}</span>
          </Data>
        </Item>
        <Item>
          <Header>{t('total')}</Header>
          <Data>
            {total.toString()} BCH <span>~ {getFiat(total)}</span>
          </Data>
        </Item>
      </Wrapper>
      <StyledButton fullWidth onClick={onSubmit} disabled={loading}>
        {t('confirm_button')}
      </StyledButton>
      <Steps steps={3} current={3} />
    </FormLayout>
  );
};

const Wrapper = styled.div`
  border-radius: 4px;
  width: 100%;
  overflow: hidden;
  margin-bottom: 30px;
  box-sizing: border-box;
  text-align: left;
  background: ${({ theme }): string => theme.background.tertiary};
  border: 1px solid ${({ theme }): string => theme.border.default};
  padding: 20px;
`;

const StyledButton = styled(Button)`
  margin-bottom: 40px;
`;

const Item = styled.div`
  margin-bottom: 15px;
  & div:first-of-type {
    margin-top: 0;
  }
  & > div:last-of-type {
    margin-bottom: 0;
  }
`;

const Header = styled.div`
  font-weight: bold;
  color: ${({ theme }): string => theme.text.header};
`;

const Data = styled.div`
  overflow-wrap: break-word;
  & > span {
    color: ${({ theme }): string => theme.text.fiat};
  }
`;
