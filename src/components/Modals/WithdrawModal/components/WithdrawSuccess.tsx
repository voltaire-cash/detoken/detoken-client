import React, { FC, ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import Big from 'big.js';
import { FormLayout, Button } from '~components';
import SuccessSvg from '~icons/success.svg';
import { EXPLORER_URL, SATOSHIS_IN_BCH } from '~config/constants';
import styled from '~config/theme';
import { ISignedTransaction } from '~components/Modals/WithdrawModal/withdrawModalApi';

interface IProps {
  hash?: string;
  closeModal: () => void;
  transaction: ISignedTransaction | null;
}

export const WithdrawSuccess: FC<IProps> = ({
  hash,
  closeModal,
  transaction,
}: IProps): ReactElement => {
  const { t } = useTranslation();
  // outputs[0] is output related to external address
  const amountBCH = Big(transaction?.outputs[0].satoshis || 0)
    .div(SATOSHIS_IN_BCH)
    .toString();
  const amount = `${amountBCH} BCH`;

  return (
    <FormLayout icon={<StyledSuccess />}>
      <Title>{t('successfully_sent', { amount })}</Title>
      <a
        href={`${EXPLORER_URL}/bch/tx/${hash}`}
        target="_blank"
        rel="noreferrer"
      >
        {t('block_explorer_link')}
      </a>
      <StyledButton onClick={closeModal} fullWidth>
        {t('close')}
      </StyledButton>
    </FormLayout>
  );
};

const StyledButton = styled(Button)`
  margin-top: 30px;
`;

const StyledSuccess = styled(SuccessSvg as 'img')`
  fill: ${({ theme }): string => theme.icons.success};
`;

const Title = styled.div`
  font-weight: bold;
  color: ${({ theme }): string => theme.text.primary};
  font-size: calc(4rem / 3);
  margin-top: 4px;
  margin-bottom: 30px;
`;
