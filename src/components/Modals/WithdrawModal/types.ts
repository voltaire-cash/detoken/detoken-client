import Big from 'big.js';
import { CloseModalAction, IWithdrawData } from '~dux/modals/modalsDux';
import { IError } from '~types/Error';
import { IPrepareTransaction } from './WithdrawModalDux';
import { ISignedTransaction } from '~components/Modals/WithdrawModal/withdrawModalApi';
import { CURRENCY_SYMBOL } from '~config/constants';
import { IExchangeRates } from '~dux/app/appDux';

export interface IMapState {
  data: IWithdrawData;
  exchangeRates: IExchangeRates;
  oraclePrice: Big;
  balance: Big;
  currency: keyof typeof CURRENCY_SYMBOL;
}

export type IMapDispatch = {
  closeModal: (props: CloseModalAction) => void;
  setError: (err: IError | null) => void;
  prepareTransaction: ({
    amount,
    sendAddress,
  }: IPrepareTransaction) => ISignedTransaction | null;
  withdraw: (transaction: ISignedTransaction) => void;
};

export enum STEPS {
  AMOUNT = 'amount',
  ADDRESS = 'address',
  CONFIRM = 'confirm',
  SUCCESS = 'success',
}

export const PREV_STEPS: { [key: string]: STEPS } = {
  [STEPS.ADDRESS]: STEPS.AMOUNT,
  [STEPS.CONFIRM]: STEPS.ADDRESS,
};

export interface IWithdrawModalProps extends IMapState, IMapDispatch {}
