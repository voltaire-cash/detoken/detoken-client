import request from '~providers/helpers';

interface IBroadcastOutput {
  satoshis: number;
  script: string;
}

interface IBroadcastInput {
  output: IBroadcastOutput;
  outputIndex: number;
  prevTxId: string;
  script: string;
  scriptString: string;
  sequenceNumber: number;
}

export interface ISignedTransaction {
  changeIndex: number;
  changeScript: string;
  fee: number;
  hash: string;
  inputs: Array<IBroadcastInput>;
  nLockTime: number;
  outputs: Array<IBroadcastOutput>;
  version: 1;
}

interface IBroadcastProps {
  transaction: ISignedTransaction;
}

export const broadcastTransaction = async (
  params: IBroadcastProps
): Promise<void> => request.post('/wallet/broadcast', params);
