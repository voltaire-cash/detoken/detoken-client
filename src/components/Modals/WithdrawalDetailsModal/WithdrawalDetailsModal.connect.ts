import { connect } from 'react-redux';
import { IState } from '~dux/rootDux';
import { closeModal, IWithdrawalDetailsData } from '~dux/modals/modalsDux';
import { WithdrawalDetailsModal } from './WithdrawalDetailsModal';
import { IMapState, IMapDispatch } from './types';
import { exchangeRatesSelector, oraclePriceSelector } from '~dux/app/appDux';

const mapStateToProps = (state: IState): IMapState => ({
  data: state.modals.withdrawalDetails.data as IWithdrawalDetailsData,
  exchangeRates: exchangeRatesSelector(state),
  oraclePrice: oraclePriceSelector(state),
  currency: state.app.userCurrency,
});

const mapDispatchToProps: IMapDispatch = {
  closeModal,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WithdrawalDetailsModal);
