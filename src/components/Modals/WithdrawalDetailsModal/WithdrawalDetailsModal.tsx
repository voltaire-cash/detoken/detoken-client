import React, { FC, useCallback, ReactElement } from 'react';
import Big from 'big.js';
import { fromUnixTime } from 'date-fns';
import { useTranslation } from 'react-i18next';
import { Modal, Button, CopyButton } from '~components';
import styled from '~config/theme';
import { IWithdrawalDetailsModalProps } from './types';
import { CURRENCY_SYMBOL, EXPLORER_URL } from '~config/constants';
import { formatFiat } from '~utils/formatters';
import { formatDateWithTranslation } from '~utils/translations';

export const WithdrawalDetailsModal: FC<IWithdrawalDetailsModalProps> = ({
  oraclePrice,
  currency,
  exchangeRates,
  data,
  closeModal,
}: IWithdrawalDetailsModalProps): ReactElement | null => {
  const { t } = useTranslation();
  const { txid, address, amount, time } = data;

  const fiatAmount = Big(amount || '0')
    .mul(oraclePrice)
    .mul(exchangeRates[currency])
    .toString();

  const handleCloseModal = useCallback(() => {
    closeModal({ type: 'withdrawalDetails' });
  }, [closeModal]);

  return (
    <Modal onClose={handleCloseModal} wide>
      <Center>
        <Title>{t('withdrawal_details_title')}</Title>
        <Amount>
          {amount} BCH{' '}
          <AmountFiat>
            ~{' '}
            {formatFiat(fiatAmount, {
              currency: CURRENCY_SYMBOL[currency],
              zeroFraction: true,
            })}
          </AmountFiat>
        </Amount>

        <Info>
          <Row>
            <Subtitle>{t('transaction_id')}</Subtitle>
            <Transaction>
              {txid}
              <CopyButton value={txid} />
            </Transaction>
          </Row>
          <Row>
            <Subtitle>{t('date')}</Subtitle>
            <span>
              {formatDateWithTranslation(fromUnixTime(Number(time || 0)))}
            </span>
          </Row>
          <Row>
            <Subtitle>{t('recipient_address')}</Subtitle>
            <Address>{address}</Address>
          </Row>
        </Info>
        <TransactionLink>
          <a
            target="_blank"
            rel="noreferrer"
            href={`${EXPLORER_URL}/bch/tx/${txid}`}
          >
            {t('block_explorer_link')}
          </a>
        </TransactionLink>

        <Button fullWidth onClick={handleCloseModal} variant="secondary">
          {t('close_button')}
        </Button>
      </Center>
    </Modal>
  );
};

const Center = styled.div`
  text-align: center;
`;

const Title = styled.div`
  font-weight: bold;
  font-size: calc(4rem / 3);
  color: ${({ theme }): string => theme.text.header};
`;

const Amount = styled.div`
  color: ${({ theme }): string => theme.text.primary};
  margin-top: 17px;
  font-size: calc(4rem / 3);
  line-height: 1.5625rem;
  ${({ theme }): string => `
    ${theme.mediaTablet} {
        margin-top: 30px;      
  `}
`;

const AmountFiat = styled.span`
  color: ${({ theme }): string => theme.text.fiat};
`;

const Info = styled.div`
  color: ${({ theme }): string => theme.text.primary};
  text-align: left;
  border-radius: 4px;
  background-color: ${({ theme }): string => theme.background.tertiary};
  border: 1px solid ${({ theme }): string => theme.border.default};
  margin-top: 30px;
  padding: 20px;
`;

const Subtitle = styled.h5`
  color: ${({ theme }): string => theme.text.header};
  font-weight: bold;
  font-size: 1rem;
  margin: 0;
`;

const Row = styled.div`
  margin-bottom: 15px;
  &:last-child {
    margin-bottom: 0;
  }
`;

const Transaction = styled.span`
  overflow-wrap: break-word;
  & > span {
    vertical-align: middle;
  }
`;

const Address = styled.span`
  overflow-wrap: break-word;
`;

const TransactionLink = styled.div`
  text-align: center;
  margin-top: 15px;
  margin-bottom: 30px;
  font-size: 14px;
`;
