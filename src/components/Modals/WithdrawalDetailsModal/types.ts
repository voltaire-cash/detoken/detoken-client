import {
  CloseModalAction,
  IWithdrawalDetailsData,
} from '~dux/modals/modalsDux';
import { IDashboardPageProps } from '~pages/DashboardPage/types';
import { IAppState, IExchangeRates } from '~dux/app/appDux';

export interface IMapState {
  exchangeRates: IExchangeRates;
  data: IWithdrawalDetailsData;
  currency: IAppState['userCurrency'];
  oraclePrice: IDashboardPageProps['oraclePrice'];
}

export type IMapDispatch = {
  closeModal: (props: CloseModalAction) => void;
};

export interface IWithdrawalDetailsModalProps extends IMapState, IMapDispatch {}
