import { INotificationsState } from '~dux/notifications/notificationsDux';

export interface IMapState {
  notifications: INotificationsState;
}

export interface INotifierProps extends IMapState {}
