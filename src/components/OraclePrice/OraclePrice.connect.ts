import { connect } from 'react-redux';
import { IState } from '~dux/rootDux';
import { OraclePrice } from './OraclePrice';

export interface IOraclePrice {
  price: string | undefined;
}

const mapStateToProps = (state: IState): IOraclePrice => ({
  price: state.app.oraclePrice?.price,
});

export default connect(mapStateToProps)(OraclePrice);
