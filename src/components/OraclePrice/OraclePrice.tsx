import React, { FC, ReactElement } from 'react';
import styled from '~config/theme';
import { formatFiat } from '~utils/formatters';

interface IProps {
  price: string | undefined;
}

export const OraclePrice: FC<IProps> = ({
  price = '',
  ...other
}: IProps): ReactElement => {
  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <Container {...other}>
      <Header>BCH-USD: </Header>
      <Price>{formatFiat(price, { zeroFraction: true })}</Price>
    </Container>
  );
};

const Header = styled.span`
  color: ${({ theme }): string => theme.text.secondary};
`;

const Price = styled.span`
  color: ${({ theme }): string => theme.text.primary};
`;

const Container = styled.div`
  margin-right: 25px;
  font-size: 14px;
`;
