import React, { FC, ReactElement } from 'react';
import Helmet from 'react-helmet';

interface IProps {
  title: string;
}
export const PageTitle: FC<IProps> = ({ title }: IProps): ReactElement => (
  <Helmet>
    <title>{title}</title>
  </Helmet>
);
