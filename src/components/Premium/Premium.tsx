import styled, { Theme } from '~config/theme';

interface IPremium {
  positive?: boolean;
  negative?: boolean;
  theme: Theme;
}
export const Premium = styled.span`
  color: ${({ positive, theme, negative }: IPremium): string => {
    if (positive) {
      return theme.snackbar.success.text;
    }
    if (negative) {
      return theme.text.error;
    }
    return theme.text.primary;
  }};
`;
