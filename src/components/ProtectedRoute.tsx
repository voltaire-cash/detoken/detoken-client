/* eslint-disable react/jsx-props-no-spreading */
import React, { FC } from 'react';
import {
  Route,
  Redirect,
  RouteProps,
  RouteComponentProps,
} from 'react-router-dom';
import { connect } from 'react-redux';
import { IState } from '~dux/rootDux';

interface IRouteProps extends RouteProps {
  userIsAuthorised: boolean;
}

const ProtectedRouteFunc: FC<IRouteProps> = (routeProps) => {
  const { component: Component, userIsAuthorised, ...rest } = routeProps;
  if (!Component) return null;
  return (
    <Route
      {...rest}
      render={(props: RouteComponentProps): React.ReactElement =>
        userIsAuthorised ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/signin',
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};

const mapStateToProps = (state: IState) => ({
  userIsAuthorised: state.user.isAuthorized,
});

export const ProtectedRoute = connect(mapStateToProps)(ProtectedRouteFunc);
