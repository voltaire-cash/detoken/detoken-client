import React, { FunctionComponent } from 'react';
import { Route, Switch, Redirect, Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { DashboardPage } from '~pages/DashboardPage';
import { NotFoundPage } from '~pages/NotFoundPage';
import { SigninPage } from '~pages/SigninPage';
import { ProtectedRoute } from './ProtectedRoute';
import { UnprotectedRoute } from './UnprotectedRoute';
import { ActivationPage } from '~pages/ActivationPage';
import { SignupPage } from '~pages/SignupPage';
import { ResetPasswordPage } from '~pages/ResetPasswordPage';
import MainLayout from './MainLayout';
import { AccountPage } from '~pages/AccountPage';
import { TransfersPage } from '~pages/TransfersPage';
import { PositionsPage } from '~pages/PositionsPage';
import { ForgotPasswordPage } from '~pages/ForgotPasswordPage';

export const history = createBrowserHistory();

export const RootRouter: FunctionComponent = () => {
  return (
    <Router history={history}>
      <MainLayout>
        <Switch>
          <Redirect from="/" exact to="/dashboard" />
          <UnprotectedRoute path="/signup" component={SignupPage} />
          <UnprotectedRoute path="/activation" component={ActivationPage} />
          <UnprotectedRoute path="/signin" component={SigninPage} />
          <UnprotectedRoute path="/resetpass" component={ResetPasswordPage} />
          <UnprotectedRoute path="/forgotpass" component={ForgotPasswordPage} />
          <ProtectedRoute path="/dashboard" component={DashboardPage} />
          <ProtectedRoute path="/account" component={AccountPage} />
          <ProtectedRoute path="/transfers" component={TransfersPage} />
          <ProtectedRoute path="/positions" component={PositionsPage} />
          <Route component={NotFoundPage} />
        </Switch>
      </MainLayout>
    </Router>
  );
};
