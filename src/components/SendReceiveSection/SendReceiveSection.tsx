import React, { FC, ReactElement, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import styled from '~config/theme';
import { Button } from '~components';
import { OpenModalAction } from '~dux/modals/modalsDux';

interface IProps {
  openModal: (props: OpenModalAction) => void;
}

export const SendReceiveSection: FC<IProps> = ({
  openModal,
}: IProps): ReactElement => {
  const { t } = useTranslation();

  const handleSendClick = useCallback(
    () => openModal({ type: 'withdraw', data: {} }),
    [openModal]
  );
  const handleReceiveClick = useCallback(
    () => openModal({ type: 'deposit', data: {} }),
    [openModal]
  );
  return (
    <Buttons>
      <div>
        <Button onClick={handleReceiveClick} variant="link">
          {t('receive')}
        </Button>
      </div>
      <div>
        <Button onClick={handleSendClick} variant="link">
          {t('send')}
        </Button>
      </div>
    </Buttons>
  );
};

const Buttons = styled.section`
  display: flex;
  border-bottom: 1px solid ${({ theme }): string => theme.border.section};
  justify-content: center;
  & > div {
    &:first-of-type {
      margin-right: 15px;
    }
    &:last-of-type {
      margin-left: 15px;
    }
  }
  ${({ theme }): string => `
    ${theme.mediaTablet}{
      display: none;
  `};
`;
