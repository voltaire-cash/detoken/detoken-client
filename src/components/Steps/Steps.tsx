import React, { FC, ReactElement } from 'react';
import styled, { Theme } from '~config/theme';

interface IProps {
  steps: number;
  current: number;
}

export const Steps: FC<IProps> = ({ steps, current }: IProps): ReactElement => {
  const items = [];

  for (let i = 1; i < steps; i++) {
    if (i < current) {
      items.push(<Line key={`${i}-line`} done />);
      items.push(<Dot key={`${i}-dot`} done />);
    } else {
      items.push(<Line key={`${i}line`} done={false} />);
      items.push(<Dot key={`${i}dot`} done={false} />);
    }
  }

  return (
    <Container>
      <Dot key="0-dot" done />
      {items}
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

interface IDot {
  theme: Theme;
  done: boolean;
}

const Dot = styled.div`
  height: 10px;
  width: 10px;
  border-radius: 50%;
  background: ${({ theme, done }: IDot): string =>
    done ? theme.background.stepDone : theme.background.stepGrayed};
`;
interface ILine {
  theme: Theme;
  done: boolean;
}
const Line = styled.div`
  width: 50px;
  height: 1px;
  background: ${({ theme, done }: ILine): string =>
    done ? theme.background.stepDone : theme.background.stepGrayed};
`;
