/* eslint-disable react/jsx-props-no-spreading */
import React, { FC, ReactElement } from 'react';
import { Input, InputWrapper, TextArea } from '~components';

interface ITextFiledProps extends React.InputHTMLAttributes<any> {
  id?: string;
  label?: string;
  error?: boolean;
  multiline?: boolean;
  helperText?: string;
  disabled?: boolean;
  name?: string;
  type?: string;
  value?: string;
  placeholder?: string;
  inputProps?: object;
  onChange?: (e: React.ChangeEvent<any>) => void;
  disableIcon?: boolean;
  withCopy?: boolean;
  disableBtn?: boolean;
  tooltip?: string;
}

export const TextField: FC<ITextFiledProps> = (
  props: ITextFiledProps
): ReactElement => {
  const {
    id,
    label,
    error,
    multiline,
    helperText,
    disabled = false,
    name,
    type,
    value,
    placeholder,
    inputProps,
    onChange,
    disableIcon,
    withCopy,
    disableBtn,
    tooltip,
    ...rest
  } = props;
  const componentProps = {
    id,
    error,
    disabled,
    name,
    type,
    value,
    placeholder,
    onChange,
    disableIcon,
  };
  return (
    <InputWrapper
      id={id}
      label={label}
      error={error}
      helperText={helperText}
      disableIcon={disableIcon}
      {...rest}
    >
      {multiline ? (
        <TextArea {...componentProps} {...inputProps} />
      ) : (
        <Input
          {...componentProps}
          {...inputProps}
          withCopy={withCopy}
          disableBtn={disableBtn}
          tooltip={tooltip}
        />
      )}
    </InputWrapper>
  );
};
