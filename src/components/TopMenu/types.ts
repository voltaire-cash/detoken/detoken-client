import { OpenModalAction } from '~dux/modals/modalsDux';

export interface IMapState {
  theme: boolean;
  userLang: string;
  isActivePosition: boolean;
}

export interface IMapDispatch {
  switchTheme: () => void;
  openModal: (props: OpenModalAction) => void;
}

export interface ITopMenuProps extends IMapState, IMapDispatch {}
