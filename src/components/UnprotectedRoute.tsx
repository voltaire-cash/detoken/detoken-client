/* eslint-disable react/jsx-props-no-spreading */
import React, { FC } from 'react';
import {
  Route,
  Redirect,
  RouteProps,
  RouteComponentProps,
} from 'react-router-dom';
import { connect } from 'react-redux';
import { IState } from '~dux/rootDux';

interface IRouteProps extends RouteProps {
  userIsAuthorised: boolean;
}

const UnprotectedRouteFunc: FC<IRouteProps> = (routeProps) => {
  const { component: Component, userIsAuthorised, ...rest } = routeProps;
  return (
    <Route
      {...rest}
      render={(props: RouteComponentProps): React.ReactElement =>
        userIsAuthorised || !Component ? (
          <Redirect
            to={{
              // @ts-expect-error
              pathname: props?.location?.state?.from?.pathname
                ? // @ts-expect-error
                  props.location.state.from.pathname
                : '/dashboard',
              state: { from: props.location },
            }}
          />
        ) : (
          <Component {...props} />
        )
      }
    />
  );
};

const mapStateToProps = (state: IState) => ({
  userIsAuthorised: state.user.isAuthorized,
});

export const UnprotectedRoute = connect(mapStateToProps)(UnprotectedRouteFunc);
