import { connect } from 'react-redux';
import { IState } from '~dux/rootDux';
import { exchangeRatesSelector, oraclePriceSelector } from '~dux/app/appDux';
import { walletBalanceSelector } from '~dux/wallet/walletDux';
import { UserBalance } from './UserBalance';
import { IMapState } from './types';

const mapStateToProps = (state: IState): IMapState => ({
  exchangeRates: exchangeRatesSelector(state),
  oraclePrice: oraclePriceSelector(state),
  balance: walletBalanceSelector(state),
  currency: state.app.userCurrency,
});

export default connect(mapStateToProps)(UserBalance);
