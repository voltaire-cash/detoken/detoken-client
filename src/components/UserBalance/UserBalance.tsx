import React, { FC, ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import styled from '~config/theme';
import { formatFiat } from '~utils/formatters';
import { CURRENCY_SYMBOL } from '~config/constants';
import { IUserBalance } from './types';

export const UserBalance: FC<IUserBalance> = ({
  exchangeRates,
  balance,
  oraclePrice,
  currency,
}: IUserBalance): ReactElement => {
  const { t } = useTranslation();

  const multiplication = {
    ...exchangeRates,
    USD: '1',
  };

  const fiatBalance = balance
    .mul(oraclePrice)
    .mul(multiplication[currency])
    .toString();
  return (
    <Container>
      <Title>{t('balance')}: </Title>
      {balance.toString()} BCH ~{' '}
      {formatFiat(fiatBalance, {
        currency: CURRENCY_SYMBOL[currency],
        zeroFraction: true,
      })}
    </Container>
  );
};

const Container = styled.div`
  font-size: 14px;
  color: ${({ theme }): string => theme.text.primary};
`;

const Title = styled.span`
  color: ${({ theme }): string => theme.text.secondary};
`;
