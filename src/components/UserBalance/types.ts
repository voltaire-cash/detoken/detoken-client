import Big from 'big.js';
import { IAppState, IExchangeRates } from '~dux/app/appDux';

export interface IMapState {
  exchangeRates: IExchangeRates;
  oraclePrice: Big;
  balance: Big;
  currency: IAppState['userCurrency'];
}

export type IUserBalance = IMapState;
