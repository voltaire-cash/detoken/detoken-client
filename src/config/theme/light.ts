import { commonParams } from './common';

export const light = {
  ...commonParams,
  primaryAppColor: '#2A2E5B',
  scrollBarColor: '#d9d9d9',
  shadowColor: 'rgba(0, 0, 0, 0.1)',
  loader: 'loaderLight.svg',
  dot: '#2A2E5B',
  text: {
    primary: '#2A2E5B',
    secondary: '#AAB0C7',
    error: '#F08391',
    header: '#000000',
    fiat: '#AAB0C7',
  },
  background: {
    unauthorized:
      'linear-gradient(to bottom right, rgba(0,255,255,0.1), rgba(0,255,0,0.1));',
    primary: '#ffffff',
    secondary: '#fbfbfb',
    tertiary: '#ffffff',
    error: 'rgba(240, 131, 145, 0.2)',
    success: '#52c41a',
    topHeader: '#FFFFFF',
    mobileMenu: '#E5E5E5',
    modals: '#FBFBFB',
    stepDone: '#2A2B5B',
    stepGrayed: '#E2E1E3',
  },
  link: {
    hover: '#383e7a',
  },
  border: {
    default: '#E1E1E1',
    error: '#F08391',
    copySeedWords: '#b7a5ff',
    accountSection: '#DFDFDF',
    section: '#E1E1E1',
    mobileMenu: '#E1E1E1',
  },
  button: {
    color: '#2A2E5B',
    text: '#FFFFFF',
    hoverColor: '#525683', //  background color on hover
    disabledColor: '#9496ad',
    textHover: 'red',
    activeColor: 'green',
  },
  secondaryButton: {
    color: '#E2E1E3',
    text: '#000000',
    hoverColor: '#F1F0F2', // background color on hover
    disabledColor: '#f0f0f1',
  },
  linkButton: {
    color: 'transparent',
    text: '#2A2E5B',
    hoverColor: '#525683', // background color on hover
    disabledColor: '#9496ad',
  },
  actionButton: {
    // back/close buttons on modal
    color: '#2A2E5B',
  },
  input: {
    background: '#FFFFFF',
    borderColor: '#E2E1E3',
    errorBorder: '#F08391',
    focusBorder: '#2A2E5B',
    focusErrorBorder: '#F08391',
    disabledBorder: '#f0f0f1',
    disabledText: '#d4d7e3',
    placeholder: '#AAB0C7',
  },
  snackbar: {
    success: {
      border: '#00DD64',
      background: '#DDFFEF',
      text: '#00DD64',
    },
    info: {
      border: '#00B7F1',
      background: '#CCF1FC',
      text: '#00B7F1',
    },
    error: {
      border: '#F08391',
      background: '#FCE6E9',
      text: '#F08391',
    },
  },
  dropdown: {
    border: '#DFDFDF',
    text: '#2A2E5B',
    textHover: '#6C6F96 ',
  },
  menu: {
    text: '#2A2E5B',
    activeColor: '#b7a5ff',
    hover: '#6C6F96',
    icon: '#2A2E5B',
  },
  footer: {
    text: '#2A2E5B',
    hover: '#6C6F96',
  },
  icons: {
    error: '#F08391',
    success: '#00FF73',
    fail: '#F08391',
  },
  logo: {
    primaryColor: '#2A2E5B',
    secondaryColor: '#B7A5FF',
    dotColor: '#2A2E5B',
  },
};
