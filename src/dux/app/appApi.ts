import request from '~providers/helpers';
import { COINBASE_URL, CONTRACTS } from '~config/constants';

interface IRatesRes {
  data: {
    currency: string;
    rates: { [key: string]: string };
  };
}

export type IFuture = {
  id: typeof CONTRACTS[number];
  oraclePubKey: string;
  maturityModifier: number;
  lowLiquidationMulti: string;
  highLiquidationMulti: string;
  fee: string;
  minHedgeSatoshis: number;
  minLongSatoshis: number;
};

export const getExchangeRates = (currency = 'USD'): Promise<IRatesRes> => {
  return request.get(`${COINBASE_URL}?currency=${currency}`, {
    withCredentials: false,
  });
};

interface IGetVersion {
  version: string;
  buildDate: string;
}

export const getVersion = (): Promise<IGetVersion> => {
  return request.get('/version.json', { baseURL: '', withCredentials: false });
};

export const getFuturesList = (): Promise<IFuture[]> => request.get('/futures');
