import {
  sign,
  // @ts-expect-error
} from 'bitauth/lib/bitauth-browserify';
import Big from 'big.js';
import { batch } from 'react-redux';
import { createSelector } from 'reselect';
import { createAction, createReducer } from 'redux-act';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { Subject, of, timer, race } from 'rxjs';
import i18next from 'i18next';
import {
  switchMap,
  map,
  filter,
  retryWhen,
  delay,
  mergeMap,
  startWith,
  takeUntil,
  catchError,
} from 'rxjs/operators';
import { Epic, ofType, StateObservable } from 'redux-observable';
import { Dispatch } from 'redux';
import storage from 'local-storage-fallback';
import {
  getWalletUTXOs,
  receiveHistoryMessage,
  eraseWalletDataAction,
  resetFirstListUnspentLoading,
} from '~dux/wallet/walletDux';
import { IState } from '~dux/rootDux';
import { IWSError, ERROR_KEYS } from '~types/Error';
import {
  addSnackbarAction,
  closeSnackbarAction,
} from '~dux/notifications/notificationsDux';
import {
  getExchangeRates,
  getFuturesList,
  IFuture,
  getVersion,
} from './appApi';
import {
  getPrivateKeysSelector,
  getPublicKeysSelector,
  getAddressesSelector,
  eraseUserDataAction,
} from '~dux/user/userDux';
import {
  WS_URL,
  STORAGE_KEYS,
  CURRENCIES,
  CURRENCY_SYMBOL,
} from '~config/constants';
import {
  savePositions,
  getPositions,
  updatePositions,
  updatePositionsFromTransactions,
} from '~pages/PositionsPage/positionsPageDux';
import { getTransactionsHistory } from '~dux/wallet/walletApi';
import { IListUnspentMessage } from '~dux/wallet/types';
import { updateEntryData } from '~pages/DashboardPage/dashboardPageDux';
import { openModal } from '~dux/modals/modalsDux';
// @ts-expect-error
import { version } from '../../../package.json';

export type userCurrencyType = keyof typeof CURRENCY_SYMBOL;

export interface IWSOraclePrice {
  price: string;
  time: string;
  block_height: number;
}

export interface IAppState {
  theme: boolean;
  isDataServerConnecting: boolean;
  isDataServerConnected: boolean;
  isMobileMenuOpen: boolean;
  websocketUrl: string;
  error: IWSError | {};
  isHttpLoading: boolean;
  isWsLoading: boolean;
  oraclePrice: {
    price: string;
    time: string;
    blockHeight: number;
  };
  exchangeRates: IRates | null;
  userLang: string;
  userCurrency: userCurrencyType;
  futuresList: IFuture[];
  version: string;
}

interface IRates {
  [x: string]: { [key: string]: string };
}

interface IWSMessage {
  type: 'list_unspent' | 'heartbeat' | 'oracle_price' | 'error';
  [key: string]: any;
}

interface IWSAction {
  websocketUrl: string;
}

export interface IExchangeRates {
  EUR: string;
  GBP: string;
  USD: string;
}

let webSocketSubject: WebSocketSubject<IWSMessage>;
let onOpenSubject = new Subject();
let onCloseSubject = new Subject();
const RETRY_TIME = 5000;
const START_PING_FROM = 3000;
const PING_INTERVAL = 30000;
const PING_MESSAGE: IWSMessage = { type: 'heartbeat' };
const PING_RESPONSE_DELAY = 1500;
const RECONNECT_MESSAGE_KEY = 'trying_to_reconnect';
// Create the websocket subject
const connectSocket = (websocketUrl: string): WebSocketSubject<IWSMessage> => {
  onOpenSubject = new Subject();
  onCloseSubject = new Subject();
  webSocketSubject = webSocket({
    url: websocketUrl,
    openObserver: onOpenSubject,
    closeObserver: onCloseSubject,
  });
  return webSocketSubject;
};

const DEFAULT_STATE = {
  theme: storage.getItem(STORAGE_KEYS.USER_THEME) === 'true',
  isDataServerConnecting: false,
  isDataServerConnected: false,
  isMobileMenuOpen: false,
  websocketUrl: '',
  oraclePrice: {
    price: '0',
    time: '',
    blockHeight: 0,
  },
  exchangeRates: null,
  error: {},
  isHttpLoading: false,
  isWsLoading: false,
  userLang: '',
  userCurrency: CURRENCIES[0] as userCurrencyType,
  futuresList: [],
  version,
};

// Selectors
const appSelector = (state: IState): IAppState => state.app;

export const isLoadingSelector = createSelector(
  appSelector,
  ({ isWsLoading, isHttpLoading }): boolean => isWsLoading || isHttpLoading
);

export const oraclePriceSelector = createSelector(
  appSelector,
  ({ oraclePrice }): Big => Big(oraclePrice.price)
);

export const exchangeRatesSelector = createSelector(
  appSelector,
  ({ exchangeRates }) =>
    exchangeRates?.USD
      ? { GBP: exchangeRates.USD.GBP, EUR: exchangeRates.USD.EUR, USD: '1' }
      : { GBP: '0', EUR: '0', USD: '1' }
);

// Sync actions
export const toggleMobileMenu = createAction(' toggleMobileMenu');
export const startAppAction = createAction('startApp');
export const connectWSAction = createAction<IWSAction>('connectWSAction');
export const sendWSMessageAction = createAction<IWSMessage>(
  'sendWSMessageAction'
);
export const disconnectWSAction = createAction<{ retry: boolean }>(
  'disconnectWSAction'
);
const sentWSMessageAction = createAction('sentWSMessageAction');
const switchThemeAction = createAction('switchThemeAction');
const isDataServerConnectedAction = createAction<boolean>(
  'isDataServerConnectedAction'
);
const httpLoading = createAction<boolean>('httpLoading');
const wsLoading = createAction<boolean>('wsLoading');
const successGetRates = createAction<IRates>('successGetRates');
const successGetFutures = createAction<IFuture[]>('successGetFutures');
const getOraclePrice = createAction<IWSOraclePrice>('getOraclePrice');
export const setUserLangAction = createAction<string>('setUserLangAction');
const setUserCurrencyAction = createAction<userCurrencyType>(
  'setUserCurrencyAction'
);

type Action = ReturnType<
  | typeof addSnackbarAction
  | typeof eraseUserDataAction
  | typeof disconnectWSAction
  | typeof eraseWalletDataAction
  | typeof httpLoading
  | typeof addSnackbarAction
  | typeof connectWSAction
  | typeof setUserLangAction
  | typeof setUserCurrencyAction
  | typeof savePositions
  | typeof getOraclePrice
  | typeof successGetFutures
  | typeof openModal
>;

// Async actions
export const errorMessage = ({ key }: { key: ERROR_KEYS }) => (
  dispatch: Dispatch<Action>
): void => {
  dispatch(
    addSnackbarAction({
      message: key,
      variant: 'error',
    })
  );
};

export const switchTheme = () => (
  dispatch: Dispatch,
  getState: () => IState
): void => {
  const { theme } = getState().app;
  storage.setItem(STORAGE_KEYS.USER_THEME, String(!theme));
  dispatch(switchThemeAction());
};

export const setUserCurrency = (currency: userCurrencyType) => (
  dispatch: Dispatch
): void => {
  dispatch(setUserCurrencyAction(currency));
};

export const appInit = () => async (
  dispatch: Dispatch<Action>,
  getState: () => IState
): Promise<void> => {
  dispatch(httpLoading(true));
  try {
    const state = getState();
    const { activePrivateKey } = getPrivateKeysSelector(state);
    const { activePublicKey } = getPublicKeysSelector(state);
    const { activeAddress, changeAddress } = getAddressesSelector(state);
    const signature = sign(WS_URL, activePrivateKey).toString('hex');
    const websocketUrl = `${WS_URL}?x-identity=${activePublicKey}&x-signature=${signature}`;
    const { version: localVersion } = state.app;
    const { version: remoteVersion } = await getVersion();

    if (localVersion !== remoteVersion) {
      // dispatch(openModal({ type: 'newVersion', data: {} }));
      console.log(
        `New version ${remoteVersion} is available. Installed ${localVersion}.`
      );
    }

    // As long a wallet page will be a start page for app,
    // as long getTransactionsHistory will be inside appInit function
    batch(() => {
      dispatch(connectWSAction({ websocketUrl }));
      dispatch(wsLoading(true));
    });
    const [{ data }, history, futuresList] = await Promise.all([
      getExchangeRates(),
      getTransactionsHistory({ addresses: [activeAddress, changeAddress] }),
      getFuturesList(),
    ]);
    batch(() => {
      dispatch(receiveHistoryMessage(history));
      dispatch(successGetRates({ [data.currency]: data.rates }));
      dispatch(successGetFutures(futuresList));
    });
    // @ts-expect-error
    updatePositionsFromTransactions(history)(dispatch, getState);
  } catch ({ fields }) {
    let message: string = ERROR_KEYS.COMMON;
    if (fields) {
      message = `Addresses ${fields.addresses.key}`;
    }
    dispatch(
      addSnackbarAction({
        message,
        variant: 'error',
      })
    );
  }
  const userCurrency = CURRENCIES[0] as keyof typeof CURRENCY_SYMBOL;

  batch(() => {
    dispatch(setUserCurrencyAction(userCurrency as userCurrencyType));
    dispatch(httpLoading(false));
  });
};

const getListUnspentMessage = (data: IListUnspentMessage) => (
  dispatch: Dispatch<Action>,
  getState: () => IState
): void => {
  const state = getState();
  const { activeAddress, changeAddress } = getAddressesSelector(state);
  if (data[activeAddress] || data[changeAddress]) {
    dispatch<any>(getWalletUTXOs(data));
  } else {
    dispatch<any>(updatePositions(data));
  }
};

const handleOraclePriceMessage = (data: IWSOraclePrice) => (
  dispatch: Dispatch<Action>,
  getState: () => IState
): void => {
  const state = getState();
  const { blockHeight } = state.app.oraclePrice;
  // indicates last get positions request was failed. Can happen on network problems
  const { requestFailed } = state.positionsPage;
  batch(() => {
    dispatch(getOraclePrice(data));
  });
  // @ts-expect-error
  updateEntryData(dispatch, getState);
  if (blockHeight === 0 || requestFailed) {
    // @ts-expect-error
    getPositions(data.block_height)(dispatch, getState);
  }
};

const subscriptionHandler: Partial<IWSMessage> = {
  list_unspent: getListUnspentMessage, // eslint-disable-line @typescript-eslint/camelcase
  oracle_price: handleOraclePriceMessage, // eslint-disable-line @typescript-eslint/camelcase
  error: errorMessage,
};

// Connect to web socket
export const connectEpic: Epic = (action$, state$: StateObservable<IState>) =>
  action$.pipe(
    ofType(connectWSAction.toString()),
    switchMap(({ payload }) =>
      connectSocket(payload.websocketUrl).pipe(
        filter(({ type }) => !!subscriptionHandler[type]),
        mergeMap(({ type, data, ...other }) => {
          const actions = [subscriptionHandler[type](data || other)];
          if (type === 'oracle_price' && state$.value.app.isWsLoading) {
            actions.push(wsLoading(false));
          }
          return actions;
        }),
        retryWhen((errors) =>
          errors.pipe(
            delay(RETRY_TIME),
            takeUntil(action$.pipe(ofType(disconnectWSAction.toString())))
          )
        )
      )
    )
  );

export const connectedEpic: Epic = (action$, state$: StateObservable<IState>) =>
  action$.pipe(
    ofType(connectWSAction.toString()),
    switchMap(() =>
      onOpenSubject.pipe(
        mergeMap(() => {
          onCloseSubject.pipe(
            map(() => of(disconnectWSAction({ retry: true })))
          );
          const { activeAddress, changeAddress } = getAddressesSelector(
            state$.value
          );
          webSocketSubject.next({
            type: 'list_unspent',
            addresses: [activeAddress, changeAddress],
          });
          webSocketSubject.next({ type: 'oracle_price' });
          return [
            closeSnackbarAction(RECONNECT_MESSAGE_KEY),
            isDataServerConnectedAction(true),
            getPositions(),
          ];
        })
      )
    )
  );

export const sendMessageEpic: Epic = (action$) =>
  action$.pipe(
    ofType(sendWSMessageAction.toString()),
    map((action) => {
      webSocketSubject.next(action.payload);
      return sentWSMessageAction();
    })
  );

export const heartbeatEpic: Epic = (action$) =>
  action$.pipe(
    ofType(isDataServerConnectedAction.toString()),
    filter(({ payload }) => payload),
    switchMap(() =>
      timer(START_PING_FROM, PING_INTERVAL).pipe(
        takeUntil(action$.pipe(ofType(disconnectWSAction.toString()))),
        switchMap(() => {
          webSocketSubject.next(PING_MESSAGE);
          return race(
            of('error').pipe(delay(PING_RESPONSE_DELAY)),
            webSocketSubject.pipe(catchError(() => of('error')))
          );
        }),
        filter((msg) => msg === 'error'),
        mergeMap(() => [
          addSnackbarAction({
            key: RECONNECT_MESSAGE_KEY,
            keepVisible: true,
            variant: 'error',
            message: i18next.t('trying_to_reconnect'),
          }),
          disconnectWSAction({ retry: true }),
        ])
      )
    )
  );

export const disconnectEpic: Epic = (
  action$,
  state$: StateObservable<IState>
) =>
  action$.pipe(
    ofType(disconnectWSAction.toString()),
    mergeMap(({ payload }) => {
      if (payload.retry) {
        return of(
          connectWSAction({ websocketUrl: state$.value.app.websocketUrl })
        ).pipe(startWith(isDataServerConnectedAction(false)));
      }
      onCloseSubject.complete();
      webSocketSubject.complete();
      return [isDataServerConnectedAction(false)];
    })
  );

export const signout = () => (dispatch: Dispatch<Action>): void => {
  storage.setItem(STORAGE_KEYS.USER_EMAIL, '');
  resetFirstListUnspentLoading();
  batch(() => {
    dispatch(eraseUserDataAction());
    dispatch(eraseWalletDataAction());
    dispatch(disconnectWSAction({ retry: false }));
    dispatch(
      addSnackbarAction({
        variant: 'success',
        message: 'you_have_successfully_signed_out',
      })
    );
  });
};

export default createReducer<IAppState>(
  {
    [switchThemeAction.toString()]: (state: IAppState) => ({
      ...state,
      theme: !state.theme,
    }),

    [connectWSAction.toString()]: (
      state: IAppState,
      { websocketUrl }: IWSAction
    ) => ({
      ...state,
      websocketUrl,
      isDataServerConnecting: true,
    }),

    [isDataServerConnectedAction.toString()]: (
      state: IAppState,
      value: boolean
    ) => ({
      ...state,
      isDataServerConnected: value,
      isDataServerConnecting: false,
    }),

    [toggleMobileMenu.toString()]: (state: IAppState) => ({
      ...state,
      isMobileMenuOpen: !state.isMobileMenuOpen,
    }),

    [getOraclePrice.toString()]: (state: IAppState, data) => ({
      ...state,
      oraclePrice: {
        price: Big(data.price).div(100).toString(),
        time: data.time,
        blockHeight: data.block_height,
      },
    }),

    [successGetRates.toString()]: (state: IAppState, data) => ({
      ...state,
      exchangeRates: {
        ...state.exchangeRates,
        ...data,
      },
    }),

    [successGetFutures.toString()]: (state: IAppState, futuresList) => ({
      ...state,
      futuresList,
    }),

    [httpLoading.toString()]: (state: IAppState, isHttpLoading: boolean) => ({
      ...state,
      isHttpLoading,
    }),

    [wsLoading.toString()]: (state: IAppState, isWsLoading: boolean) => ({
      ...state,
      isWsLoading,
    }),

    [setUserCurrencyAction.toString()]: (
      state: IAppState,
      userCurrency: userCurrencyType
    ) => ({
      ...state,
      userCurrency,
    }),

    [setUserLangAction.toString()]: (state: IAppState, userLang: string) => ({
      ...state,
      userLang,
    }),
  },
  DEFAULT_STATE
);
