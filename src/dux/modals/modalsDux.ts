import { createAction, createReducer } from 'redux-act';
import { IError } from '~types/Error';
import { CONTRACTS, POSITION } from '~config/constants';
import { IFuturesPosition } from '~pages/PositionsPage/types';
import {
  IContractMetadata,
  IContractParameters,
  IParsedSettlementData,
  ISimulationOutput,
} from '~types/anyHedge';

export interface IPositionModalData {
  metadata: IContractMetadata;
  parameters: IContractParameters;
  simulation: ISimulationOutput;
  contract: IFuturesPosition;
  oraclePrice: string;
  blockHeight: number;
  settlement: IParsedSettlementData | {};
}

export interface IConfirmModalData {
  startPrice: string;
  lowLiquidationMulti: string;
  maturityModifier: number;
  value: number;
  fee: string;
  position: POSITION;
  premium: number;
  hedgeInputSats: number;
  longInputSats: number;
  dustCost: number;
  minerCost: number;
  premiumSatoshis: number;
  startBlockHeight: number;
  contractAddress: string;
  highLiquidationPrice: number;
  lowLiquidationPrice: number;
  productId: typeof CONTRACTS[number];
}

export interface IWithdrawData {
  error: IError | null;
  success: boolean;
  loading: boolean;
}

export interface IWithdrawalDetailsData {
  txid: string;
  amount: string;
  time: string;
  address: string;
}

export interface IDepositDetailsData {
  txid: string;
  amount: string;
  time: string;
  address: string;
}

export interface IEnable2FAData {
  loading: boolean;
  secretCode: string;
  error: IError | null;
  success: boolean;
}

export interface IDisable2FAData {
  loading: boolean;
  error: IError | null;
}

export type IModalsState = {
  deposit: {
    isOpen: boolean;
    data: object;
  };
  withdraw: {
    isOpen: boolean;
    data: IWithdrawData | {};
  };
  withdrawalDetails: {
    isOpen: boolean;
    data: IWithdrawalDetailsData | {};
  };
  depositDetails: {
    isOpen: boolean;
    data: IDepositDetailsData | {};
  };
  enable2FA: {
    isOpen: boolean;
    data: IEnable2FAData | {};
  };
  disable2FA: {
    isOpen: boolean;
    data: IDisable2FAData | {};
  };
  signout: {
    isOpen: boolean;
    data: {};
  };
  longHedgeConfirm: {
    isOpen: boolean;
    data: IConfirmModalData | {};
  };
  position: {
    isOpen: boolean;
    data: IPositionModalData | {};
  };
  jurisdiction: {
    isOpen: boolean;
    data: {};
  };
  seedWords: {
    isOpen: boolean;
    data: {};
  };
  emailInUse: {
    isOpen: boolean;
    data: {};
  };
  sinInUse: {
    isOpen: boolean;
    data: {};
  };
  newVersion: {
    isOpen: boolean;
    data: {};
  };
};
export type OpenModalAction = { type: keyof IModalsState; data: {} };
export type CloseModalAction = { type: keyof IModalsState };
type UpdateModalAction = { type: keyof IModalsState; newData: {} };

export const DEFAULT_STATE: IModalsState = {
  deposit: {
    isOpen: false,
    data: {},
  },
  withdraw: {
    isOpen: false,
    data: {},
  },
  withdrawalDetails: {
    isOpen: false,
    data: {},
  },
  depositDetails: {
    isOpen: false,
    data: {},
  },
  enable2FA: {
    isOpen: false,
    data: {},
  },
  disable2FA: {
    isOpen: false,
    data: {},
  },
  signout: {
    isOpen: false,
    data: {},
  },
  longHedgeConfirm: {
    isOpen: false,
    data: {},
  },
  position: {
    isOpen: false,
    data: {},
  },
  jurisdiction: {
    isOpen: false,
    data: {},
  },
  seedWords: {
    isOpen: false,
    data: {},
  },
  emailInUse: {
    isOpen: false,
    data: {},
  },
  sinInUse: {
    isOpen: false,
    data: {},
  },
  newVersion: {
    isOpen: false,
    data: {},
  },
};

// Sync Actions
export const openModal = createAction<OpenModalAction>('openModal');
export const closeModal = createAction<CloseModalAction>('closeModal');
export const updateModalData = createAction<UpdateModalAction>(
  'updateModalData'
);

// Async Actions
export default createReducer<IModalsState>(
  {
    [openModal.toString()]: (
      state: IModalsState,
      { type, ...data }: OpenModalAction
    ) => ({
      ...state,
      [type]: {
        ...data,
        isOpen: true,
      },
    }),
    [closeModal.toString()]: (
      state: IModalsState,
      { type }: CloseModalAction
    ) => ({
      ...state,
      [type]: {
        ...DEFAULT_STATE[type],
      },
    }),

    [updateModalData.toString()]: (
      state: IModalsState,
      { type, newData }: UpdateModalAction
    ) => ({
      ...state,
      [type]: {
        ...state[type],
        data: {
          ...state[type].data,
          ...newData,
        },
      },
    }),
  },
  DEFAULT_STATE
);
