import { combineReducers, Reducer } from 'redux';
import { combineEpics } from 'redux-observable';
import app, {
  IAppState,
  connectEpic,
  connectedEpic,
  sendMessageEpic,
  disconnectEpic,
  heartbeatEpic,
} from './app/appDux';
import wallet, { IWalletState } from './wallet/walletDux';
import modals, { IModalsState } from './modals/modalsDux';
import notifications, {
  INotificationsState,
} from './notifications/notificationsDux';
import signinPage, { ISigninPageState } from '~pages/SigninPage/signinPageDux';
import activationPage, {
  IActivationPageState,
} from '~pages/ActivationPage/activationPageDux';
import signupPage, { ISignupPageState } from '~pages/SignupPage/signupPageDux';
import user, { IUserState } from './user/userDux';
import resetPasswordPage, {
  IResetPasswordPageState,
} from '~pages/ResetPasswordPage/ResetPasswordPageDux';
import dashboardPage, {
  IDashboardPageState,
} from '~pages/DashboardPage/dashboardPageDux';
import accountPage, {
  IAccountPageState,
} from '~pages/AccountPage/accountPageDux';
import positionsPage, {
  IPositionsPageState,
} from '~pages/PositionsPage/positionsPageDux';
import forgotPasswordPage, {
  IForgotPasswordPageState,
} from '~pages/ForgotPasswordPage/ForgotPasswordPageDux';

export interface IState {
  app: IAppState;
  user: IUserState;
  wallet: IWalletState;
  modals: IModalsState;
  notifications: INotificationsState;
  signinPage: ISigninPageState;
  activationPage: IActivationPageState;
  signupPage: ISignupPageState;
  resetPasswordPage: IResetPasswordPageState;
  dashboardPage: IDashboardPageState;
  accountPage: IAccountPageState;
  positionsPage: IPositionsPageState;
  forgotPasswordPage: IForgotPasswordPageState;
}

export const rootReducer = combineReducers<Reducer<IState>>({
  app,
  user,
  wallet,
  modals,
  notifications,
  signinPage,
  activationPage,
  signupPage,
  resetPasswordPage,
  dashboardPage,
  accountPage,
  positionsPage,
  forgotPasswordPage,
});

export const rootEpic = combineEpics(
  connectEpic,
  connectedEpic,
  sendMessageEpic,
  disconnectEpic,
  heartbeatEpic
);
