import request from '~providers/helpers';
import { IHistoryRes, ITransactionRes } from '~dux/wallet/types';

interface IHistoryProps {
  addresses: string[];
}

export const getTransactionsHistory = async (
  params: IHistoryProps
): Promise<IHistoryRes> => request.get('/wallet/history', { params });

export const getTransaction = async (txid: string): Promise<ITransactionRes> =>
  request.get(`/wallet/transaction/${txid}`);
