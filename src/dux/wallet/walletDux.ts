import { createAction, createReducer } from 'redux-act';
import { batch } from 'react-redux';
import { Dispatch } from 'react';
import { createSelector } from 'reselect';
import i18next from 'i18next';
import Big from 'big.js';
import { IState } from '~dux/rootDux';
import {
  ITransactionRes,
  IUTXOs,
  IListUnspentMessage,
  IHistoryRes,
  ITransactionPayload,
} from '~dux/wallet/types';
import { formatUTXOsResponse, formatHistoryMessage } from '~utils/formatters';
import { getAddressesSelector } from '~dux/user/userDux';
import { getTransaction } from './walletApi';
import { addSnackbarAction } from '~dux/notifications/notificationsDux';
import {
  settleContractsAction,
  contractAddressesSelector,
} from '~pages/PositionsPage/positionsPageDux';

type ISpentUTXOs = string[];

export interface IWalletState {
  transactions: IHistoryRes | null;
  UTXOs: IUTXOs | null;
  spentUTXOs: ISpentUTXOs;
}

const DEFAULT_STATE = {
  transactions: null,
  UTXOs: null,
  spentUTXOs: [],
};
let firstListUnspentLoading = true;
// Selectors
const walletSelector = (state: IState): IWalletState => state.wallet;

export const UTXOsSelector = createSelector(
  walletSelector,
  ({ UTXOs, spentUTXOs }): IUTXOs | null =>
    UTXOs
      ? Object.entries(UTXOs).reduce((acc, [key, utxo]) => {
          if (spentUTXOs.includes(key)) return acc;
          acc[key] = utxo;
          return acc;
        }, {} as IUTXOs)
      : null
);

export const transactionsSelector = createSelector(
  walletSelector,
  ({ transactions }): IHistoryRes | null => transactions
);

export const walletBalanceSelector = createSelector(
  walletSelector,
  ({ UTXOs, spentUTXOs }) =>
    Object.entries(UTXOs || {}).reduce(
      (acc, [, val]) =>
        spentUTXOs.includes(`${val.txid}:${val.outputIndex}`)
          ? acc
          : acc.add(val.amount),
      Big(0)
    )
);

export const walletDepositsSelector = createSelector(
  (state: IState) => ({
    ...getAddressesSelector(state),
    history: transactionsSelector(state) || {},
    contracts: contractAddressesSelector(state),
  }),
  (props) => formatHistoryMessage(props)?.received
);

export const walletWithdrawalsSelector = createSelector(
  (state: IState) => ({
    ...getAddressesSelector(state),
    history: transactionsSelector(state) || {},
    contracts: contractAddressesSelector(state),
  }),
  (props) => formatHistoryMessage(props)?.sent
);

export const spentUTXOsSelector = createSelector(
  walletSelector,
  ({ spentUTXOs }): ISpentUTXOs => spentUTXOs
);

// Sync Actions
const receiveTransactionMessage = createAction<ITransactionPayload>(
  'receiveTransactionMessage'
);
export const receiveUTXOsMessage = createAction<IListUnspentMessage>(
  'receiveUTXOsMessage'
);
export const receiveHistoryMessage = createAction<IHistoryRes>(
  'receiveHistoryMessage'
);

export const addSpentUTXOs = createAction<ISpentUTXOs>('addSpentUTXOs');

export const removeSpentUTXOs = createAction<ISpentUTXOs>('removeSpentUTXOs');

interface IUTXOsMessage {
  newUTXOs: IUTXOs;
  keepUTXOs: IUTXOs;
}
const UTXOsMessage = createAction<IUTXOsMessage>('UTXOsMessage');

export const removeUTXO = createAction<string>('removeUTXO');

export const eraseWalletDataAction = createAction('eraseWalletDataAction');

type Action = ReturnType<
  | typeof receiveUTXOsMessage
  | typeof receiveTransactionMessage
  | typeof removeUTXO
  | typeof addSnackbarAction
  | typeof UTXOsMessage
  | typeof settleContractsAction
  | typeof removeSpentUTXOs
>;

export const resetFirstListUnspentLoading = (): void => {
  firstListUnspentLoading = true;
};

export const getWalletUTXOs = (messages: IListUnspentMessage) => async (
  dispatch: Dispatch<Action>,
  getState: () => IState
): Promise<void> => {
  const state = getState();
  const currentUTXOs = UTXOsSelector(state) || {};
  const spentUTXOs = spentUTXOsSelector(state);
  const { contractAddresses } = state.positionsPage;
  const settledContracts = [] as string[];
  let newUTXOs = formatUTXOsResponse(messages);
  const { activeAddress, changeAddress } = getAddressesSelector(state);
  let transactions: ITransactionRes[] = [];
  const addresses: string[] = [];
  // if we got UTXOs only for one address from two we need to keep UTXOs for another
  const incomingAddresses = Object.keys(messages);
  let keepUTXOs = {} as IUTXOs;
  if (incomingAddresses.length === 1) {
    const keepAddress =
      incomingAddresses[0] === activeAddress ? changeAddress : activeAddress;
    keepUTXOs = Object.values(currentUTXOs).reduce((acc, utxo) => {
      if (keepAddress === utxo.address)
        acc[`${utxo.txid}:${utxo.outputIndex}`] = utxo;
      return acc;
    }, {} as IUTXOs);
  }

  // Fetch new transaction info by txid only if wallet page finished loading
  if (!firstListUnspentLoading) {
    const promises: Array<Promise<ITransactionRes>> = [];
    const txArray: { [key: string]: boolean | undefined } = {};
    Object.entries(newUTXOs).forEach(([key, { txid, address }]) => {
      if (!currentUTXOs[key] && !txArray[txid]) {
        promises.push(getTransaction(txid));
        addresses.push(address);
        txArray[txid] = true;
      }
    });
    transactions = await Promise.all(promises);
  }
  firstListUnspentLoading = false;
  batch(() => {
    transactions.forEach((transaction, index) => {
      const address = addresses[index];
      // check if we already have the same transaction
      const existingTx = state.wallet?.transactions?.[address].some(
        (t) => t.txid === transaction.txid
      );
      if (!existingTx) {
        // Calculate deposit amount
        const amount = transaction.vout.reduce(
          (acc2, { scriptPubKey, value }) =>
            scriptPubKey.addresses[0] === activeAddress
              ? Big(acc2).plus(value)
              : acc2,
          Big(0) as Big
        );
        // If amount is not a zero it's a deposit transaction
        if (Big(amount).gt(0)) {
          dispatch(
            addSnackbarAction({
              message: i18next.t('successful_deposit', {
                amount: amount.toString(),
              }),
              variant: 'success',
            })
          );
        }
        dispatch(receiveTransactionMessage({ transaction, address }));
        // we need to check if this is contract transaction
        if (address === activeAddress && contractAddresses.length) {
          if (contractAddresses.includes(transaction.vin[0].address)) {
            settledContracts.push(transaction.vin[0].address);
          }
        }
      }
    });
    // filter UTXOs to not include already spent UTXOs
    newUTXOs = Object.values(newUTXOs).reduce((acc, utxo) => {
      if (!spentUTXOs.includes(`${utxo.txid}:${utxo.outputIndex}`))
        acc[`${utxo.txid}:${utxo.outputIndex}`] = utxo;
      return acc;
    }, {} as IUTXOs);
    keepUTXOs = Object.values(keepUTXOs).reduce((acc, utxo) => {
      if (!spentUTXOs.includes(`${utxo.txid}:${utxo.outputIndex}`))
        acc[`${utxo.txid}:${utxo.outputIndex}`] = utxo;
      return acc;
    }, {} as IUTXOs);
    batch(() => {
      // Update store by new UTXO information
      dispatch(UTXOsMessage({ newUTXOs, keepUTXOs }));
      if (settledContracts.length) {
        const t = i18next.t('contract_was_settled');
        dispatch(addSnackbarAction({ message: t, variant: 'info' }));
        dispatch(settleContractsAction(settledContracts));
      }
    });
  });
};

// Async Actions
export default createReducer<IWalletState>(
  {
    [receiveHistoryMessage.toString()]: (
      state: IWalletState,
      transactions: IHistoryRes
    ) => ({
      ...state,
      transactions,
    }),

    [receiveTransactionMessage.toString()]: (
      state: IWalletState,
      payload: ITransactionPayload
    ) => ({
      ...state,
      transactions: {
        ...state.transactions,
        [payload.address]: [
          ...(state.transactions
            ? state.transactions[payload.address] || []
            : []),
          payload.transaction,
        ],
      } as IHistoryRes,
    }),

    [UTXOsMessage.toString()]: (
      state: IWalletState,
      { newUTXOs, keepUTXOs }
    ) => ({
      ...state,
      UTXOs: {
        ...newUTXOs,
        ...keepUTXOs,
      },
    }),

    [removeUTXO.toString()]: (state: IWalletState, id: string) => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { [id]: _, ...utxos } = state.UTXOs || {};
      return {
        ...state,
        UTXOs: { ...utxos },
      };
    },
    [eraseWalletDataAction.toString()]: () => ({
      ...DEFAULT_STATE,
    }),
    [addSpentUTXOs.toString()]: (state: IWalletState, UTXOs: ISpentUTXOs) => ({
      ...state,
      spentUTXOs: [...state.spentUTXOs, ...UTXOs],
    }),
    [removeSpentUTXOs.toString()]: (
      state: IWalletState,
      UTXOs: ISpentUTXOs
    ) => ({
      ...state,
      spentUTXOs: state.spentUTXOs.filter((utxo) => !UTXOs.includes(utxo)),
    }),
  },
  DEFAULT_STATE
);
