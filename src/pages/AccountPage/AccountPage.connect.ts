import { connect } from 'react-redux';
import { AccountPage } from './AccountPage';
import { IState } from '~dux/rootDux';
import { IMapState } from './types';
import { changePassword, resetError } from '~pages/AccountPage/accountPageDux';
import { openModal } from '~dux/modals/modalsDux';

const mapStateToProps = (state: IState): IMapState => ({
  isLoading: state.accountPage.isLoading,
  success: state.accountPage.success,
  error: state.accountPage.error,
  twoFAMode: state.user.securityFields.twoFaMode,
  userName: state.user.name,
  refCode: state.user.refCode,
});

const mapDispatchToProps = {
  changePassword,
  resetError,
  openModal,
};

export default connect(mapStateToProps, mapDispatchToProps)(AccountPage);
