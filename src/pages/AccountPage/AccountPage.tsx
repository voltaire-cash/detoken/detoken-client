import React, { FC, useCallback, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import styled from '~config/theme';
import { IAccountPageProps } from './types';
import { Button, Input, PageTitle } from '~components';
import { AccountPageChangePasswordForm } from './components/AccountPageChangePassword';

const AccountPage: FC<IAccountPageProps> = ({
  success,
  isLoading,
  error,
  changePassword,
  userName,
  refCode,
  twoFAMode,
  resetError,
  openModal,
}: IAccountPageProps): React.ReactElement => {
  const { t } = useTranslation();
  const refLink = `${process.env.LANDING_ORIGIN}?r=${refCode}`;

  // Clear errors on exit (on route change)
  useEffect(() => {
    return (): void => resetError();
  }, [resetError]);

  const handle2FAButtonClick = useCallback(() => {
    if (twoFAMode) {
      openModal({ type: 'disable2FA', data: {} });
    } else {
      openModal({ type: 'enable2FA', data: {} });
    }
  }, [openModal, twoFAMode]);

  const handleSeedWordsButtonClick = useCallback(() => {
    openModal({ type: 'seedWords', data: {} });
  }, [openModal]);

  return (
    <Container>
      <PageTitle title={`Detoken: ${t('account_page')}`} />
      <NameHeader>
        <NameAbbr>
          {userName
            .split(' ')
            .slice(0, 3)
            .map((word) => word && word[0].toUpperCase())
            .join('')}
        </NameAbbr>
        {userName}
      </NameHeader>
      <Content>
        <Title>{t('referral_link_title')}</Title>
        <Subtitle>{t('referral_link_subtitle')}</Subtitle>
        <Input type="text" defaultValue={refLink} readOnly withCopy />
        <Divider />
        <Title>{t('password_title')}</Title>
        <Subtitle>{t('password_subtitle')}</Subtitle>
        <AccountPageChangePasswordForm
          success={success}
          changePassword={changePassword}
          resetError={resetError}
          isLoading={isLoading}
          error={error}
        />
        <Divider />
        <Title>{t('two_fa_title')}</Title>
        <Subtitle>
          {t(twoFAMode ? 'two_fa_disable_subtitle' : 'two_fa_enable_subtitle')}
        </Subtitle>
        <Button fullWidth onClick={handle2FAButtonClick}>
          {t(twoFAMode ? 'disable_2fa_button' : 'enable_2fa_button')}
        </Button>
        <Divider />
        <Title>{t('seed_words_title')}</Title>
        <Subtitle>{t('view_your_wallet_seed_words')}</Subtitle>
        <Button fullWidth onClick={handleSeedWordsButtonClick}>
          {t('seed_words_button')}
        </Button>
      </Content>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  height: 100%;
  background-color: ${({ theme }): string => theme.background.primary};
`;

const NameHeader = styled.div`
  height: 110px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  border-bottom: 1px solid ${({ theme }): string => theme.border.default};
  box-sizing: border-box;
  background-color: ${({ theme }): string => theme.background.secondary};
  color: ${({ theme }): string => theme.text.primary};
  font-size: calc(4rem / 3);
`;

const NameAbbr = styled.div`
  width: 48px;
  height: 48px;
  background-color: ${({ theme }): string => theme.background.primary};
  border: 1px solid ${({ theme }): string => theme.border.default};
  border-radius: 4px;
  margin-right: 24px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Content = styled.div`
  width: 100%;
  max-width: 400px;
  padding: 50px 44px;
  box-sizing: border-box;
`;

const Title = styled.div`
  font-weight: bold;
  font-size: calc(4rem / 3);
  line-height: calc(4rem / 3);
  color: ${({ theme }): string => theme.text.header};
`;

const Subtitle = styled.p`
  color: ${({ theme }): string => theme.text.primary};
  font-size: 1rem;
  margin-top: 10px;
  margin-bottom: 30px;
  line-height: calc(4rem / 3);
`;

const Divider = styled.div`
  height: 1px;
  background-color: ${({ theme }): string => theme.border.accountSection};
  margin: 50px 0;
`;

export { AccountPage };
