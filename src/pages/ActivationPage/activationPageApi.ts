import request from '../../providers/helpers';
import { IError } from '../../types/Error';

interface IConfirmProps {
  code: string;
  email: string;
}

export const sentConfirmationEmail = async (
  params: IConfirmProps
): Promise<void | IError> => request.get('/auth/confirm', { params });
