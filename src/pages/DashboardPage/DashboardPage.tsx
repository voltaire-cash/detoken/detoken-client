import React, { ReactElement, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { IDashboardPageProps } from './types';
import { SendReceiveSection } from '~components/SendReceiveSection';
import { PageTitle } from '~components';
import { StartContract } from './components/StartContract';

export const DashboardPage = ({
  error,
  oraclePrice,
  balance,
  isLoading,
  contracts,
  setError,
  showContractInfo,
  getPremiumPriceData,
  premiums,
  setAmount,
  amount,
  prices,
  setCurrency,
  currency,
  isActivePosition,
  setMaxAmount,
  isMaxAmount,
  setIsMaxAmount,
  position,
  productId,
  setPosition,
  setProductId,
}: IDashboardPageProps): ReactElement => {
  const { t } = useTranslation();

  // Clear errors on exit (on route change)
  useEffect(() => {
    return (): void => {
      setError(null);
      if (isMaxAmount) {
        setIsMaxAmount(false);
      }
    };
  }, [setError, setIsMaxAmount, isMaxAmount]);

  return (
    <>
      <PageTitle title={`Detoken: ${t('dashboard_page')}`} />
      <SendReceiveSection />
      <StartContract
        error={error}
        balance={balance}
        isLoading={isLoading}
        contracts={contracts}
        oraclePrice={oraclePrice}
        setError={setError}
        onSubmit={showContractInfo}
        getPremiumPriceData={getPremiumPriceData}
        premiums={premiums}
        setAmount={setAmount}
        amount={amount}
        prices={prices}
        setCurrency={setCurrency}
        currency={currency}
        isActivePosition={isActivePosition}
        setMaxAmount={setMaxAmount}
        isMaxAmount={isMaxAmount}
        setIsMaxAmount={setIsMaxAmount}
        position={position}
        productId={productId}
        setProductId={setProductId}
        setPosition={setPosition}
      />
    </>
  );
};
