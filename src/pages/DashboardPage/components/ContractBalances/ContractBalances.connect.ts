import { connect } from 'react-redux';
import Big from 'big.js';
import { oraclePriceSelector } from '~dux/app/appDux';
import { IState } from '~dux/rootDux';
import { walletBalanceSelector } from '~dux/wallet/walletDux';
import { ContractBalances } from './ContractBalances';

export interface IMapState {
  positions: IState['positionsPage']['positions'];
  oraclePrice: Big;
  balance: Big;
}

const mapStateToProps = (state: IState): IMapState => ({
  positions: state.positionsPage.positions,
  oraclePrice: oraclePriceSelector(state),
  balance: walletBalanceSelector(state),
});

export default connect(mapStateToProps)(ContractBalances);
