import React, { FC, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import Big from 'big.js';
import styled from '~config/theme';
import { formatFiat } from '~utils/formatters';
import { getExistingContractParams } from '~utils/contract';
import { SATOSHIS_IN_BCH, POSITION } from '~config/constants';
import { IState } from '~dux/rootDux';

interface IProps {
  balance: Big;
  oraclePrice: Big;
  positions: IState['positionsPage']['positions'];
}

export const ContractBalances: FC<IProps> = ({
  balance,
  positions,
  oraclePrice,
}: IProps) => {
  const { t } = useTranslation();
  const [contractsValueBCH, setContractsValueBCH] = useState<Big>(Big(0));

  const { HEDGE } = POSITION;

  useEffect(() => {
    let contractsBCH = Big(0);
    let isSubscribed = true;

    const getContracts = async (): Promise<void | null> => {
      const promises = positions
        .filter((p) => !p.isSettled)
        .map((position) =>
          getExistingContractParams({
            data: position,
            oraclePrice: oraclePrice.toString(),
          })
        );

      const contracts = await Promise.all(promises);

      if (isSubscribed) {
        contracts.forEach((contract) => {
          const { simulation, takerSide } = contract;
          const { hedgePayoutSats, longPayoutSats } = simulation;

          const currentValueBCH = Big(
            takerSide === HEDGE ? hedgePayoutSats : longPayoutSats
          ).div(SATOSHIS_IN_BCH);
          contractsBCH = contractsBCH.plus(currentValueBCH);
        });
        setContractsValueBCH(contractsBCH);
      }
    };

    getContracts();
    return (): void => {
      isSubscribed = false;
    };
  }, [positions, oraclePrice, HEDGE]);

  const contractsValueUSD = formatFiat(
    contractsValueBCH.mul(oraclePrice || '0').toFixed(2),
    { zeroFraction: true }
  );

  const accountValueBCH = balance.plus(contractsValueBCH);
  const accountValueUSD = formatFiat(
    accountValueBCH.mul(oraclePrice || '0').toFixed(2),
    { zeroFraction: true }
  );

  return positions.length !== 0 ? (
    <>
      <Item>
        <Title>{t('contracts_value')}:&nbsp;</Title>
        <Balance>
          <CryptoBalance>
            {contractsValueBCH.toString()} BCH&nbsp;
          </CryptoBalance>
          <FiatBalance>~ {contractsValueUSD}</FiatBalance>
        </Balance>
      </Item>
      <Item>
        <Title>{t('account_value')}:&nbsp;</Title>
        <Balance>
          <CryptoBalance>{accountValueBCH.toString()} BCH&nbsp;</CryptoBalance>
          <FiatBalance>~ {accountValueUSD}</FiatBalance>
        </Balance>
      </Item>
    </>
  ) : null;
};

const Item = styled.div`
  display: flex;
  flex-wrap: wrap;
  font-size: 14px;
`;

const Title = styled.span`
  color: ${({ theme }): string => theme.text.secondary};
  white-space: nowrap;
`;

const CryptoBalance = styled.span`
  color: ${({ theme }): string => theme.text.primary};
  white-space: nowrap;
`;

const FiatBalance = styled.span`
  color: ${({ theme }): string => theme.text.primary};
  white-space: nowrap;
`;

const Balance = styled.div`
  white-space: nowrap;
`;
