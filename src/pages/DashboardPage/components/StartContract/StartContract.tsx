import React, {
  FC,
  ReactElement,
  useCallback,
  useEffect,
  useMemo,
} from 'react';
import { useTranslation } from 'react-i18next';
import Big from 'big.js';
import styled, { Theme } from '~config/theme';
import { Button, Dropdown, InputDropdown, UserBalance } from '~components';
import { StartContractDetails } from './StartContractDetails';
import { POSITION } from '~config/constants';
// import { ITranslations } from '~components/Dropdown/Dropdown';
import { separateThousands } from '~utils/formatters';
import { ERROR_KEYS } from '~types/Error';
import { IDashboardPageProps } from '../../types';
import { ContractBalances } from '../ContractBalances';
import { OraclePrice } from '~components/OraclePrice';

const currencies = ['BCH', 'USD'];
// TODO: uncomment when more than 2 products available
// const translations: ITranslations = CONTRACTS.reduce(
//   (acc: ITranslations, c: string) => {
//     acc[c] = c.replace(/-/g, '_');
//     return acc;
//   },
//   {}
// );

interface IProps {
  isLoading: boolean;
  contracts: IDashboardPageProps['contracts'];
  setError: IDashboardPageProps['setError'];
  error: IDashboardPageProps['error'];
  balance: IDashboardPageProps['balance'];
  onSubmit: IDashboardPageProps['showContractInfo'];
  oraclePrice: IDashboardPageProps['oraclePrice'];
  getPremiumPriceData: IDashboardPageProps['getPremiumPriceData'];
  premiums: IDashboardPageProps['premiums'];
  setAmount: IDashboardPageProps['setAmount'];
  amount: IDashboardPageProps['amount'];
  prices: IDashboardPageProps['prices'];
  setCurrency: IDashboardPageProps['setCurrency'];
  currency: IDashboardPageProps['currency'];
  isActivePosition: IDashboardPageProps['isActivePosition'];
  setMaxAmount: IDashboardPageProps['setMaxAmount'];
  isMaxAmount: IDashboardPageProps['isMaxAmount'];
  setIsMaxAmount: IDashboardPageProps['setIsMaxAmount'];
  position: IDashboardPageProps['position'];
  productId: IDashboardPageProps['productId'];
  setProductId: IDashboardPageProps['setProductId'];
  setPosition: IDashboardPageProps['setPosition'];
}

interface IHelper {
  error?: boolean;
  theme: Theme;
}

export const StartContract: FC<IProps> = ({
  error,
  isLoading,
  // contracts,
  setError,
  balance,
  onSubmit,
  oraclePrice,
  getPremiumPriceData,
  premiums,
  setAmount,
  amount,
  prices,
  setCurrency,
  currency,
  isActivePosition,
  setMaxAmount,
  isMaxAmount,
  setIsMaxAmount,
  position,
  productId,
  setPosition,
}: // setProductId,
IProps): ReactElement => {
  const { key, field, params } = error || {};
  const { t } = useTranslation();

  const handleChangeType = useCallback(
    (e: React.MouseEvent<HTMLDivElement>) => {
      const { id } = e.currentTarget.dataset;
      setPosition(id as POSITION);
      setError(null);
    },
    [setError, setPosition]
  );

  const amountInBCH = useMemo(
    () =>
      currency === 'USD'
        ? Big(amount || '0')
            .div(oraclePrice)
            .toString()
        : amount || '0',
    [currency, amount, oraclePrice]
  );

  useEffect(() => {
    getPremiumPriceData(productId);
  }, [getPremiumPriceData, productId]);

  useEffect(() => {
    if (isMaxAmount) {
      setMaxAmount({ position, productId });
    }
  }, [oraclePrice, isMaxAmount, setMaxAmount, position, productId]);

  const dropdown = useMemo(
    () => (
      <Dropdown
        onChange={setCurrency}
        value={currency}
        items={currencies}
        minWidth={50}
      />
    ),
    [currency, setCurrency]
  );

  const handleMaxClick = useCallback(() => {
    setError(null);
    if (isMaxAmount) {
      setIsMaxAmount(false);
      return;
    }
    setMaxAmount({ position, productId });
  }, [
    position,
    productId,
    setMaxAmount,
    isMaxAmount,
    setIsMaxAmount,
    setError,
  ]);

  const handleAmountChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      const { value } = e.currentTarget;
      setError(null);
      if (isMaxAmount) {
        setIsMaxAmount(false);
      }

      setAmount(
        value
          .replace(/[^0-9.]/g, '')
          .replace(/([0-9]+)\.([0-9]+)\./, '$1$2.')
          .replace(/^0+([^.])/, '$1')
          .replace(/^0+/, '0')
          .replace(/^\./, '0.')
          .replace(/\.+/, '.')
          .replace(/,/g, '')
      );
    },
    [setError, setAmount, setIsMaxAmount, isMaxAmount]
  );

  const handleFormSubmit = useCallback(
    (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault();
      let value = amount;
      if (!value) {
        setError({
          field: 'amount',
          key: ERROR_KEYS.EMPTY_VALUE,
        });
        return;
      }
      if (currency === 'USD') {
        value = Big(amount).div(oraclePrice).toString();
      }

      if (Big(value).gt(balance)) {
        setError({
          field: 'amount',
          key: ERROR_KEYS.INSUFFICIENT_FUNDS,
        });
        return;
      }

      onSubmit({ amount, position, productId });
    },
    [
      amount,
      balance,
      setError,
      productId,
      position,
      onSubmit,
      oraclePrice,
      currency,
    ]
  );

  return (
    <PageWrapper onSubmit={handleFormSubmit}>
      <ContractWrapper>
        <StyledOraclePrice />
        {isActivePosition && <ContractBalances />}
      </ContractWrapper>
      <Container>
        <Header>
          <Item
            active={position === POSITION.HEDGE}
            data-id={POSITION.HEDGE}
            onClick={handleChangeType}
          >
            {t('hedge')}
          </Item>
          <Item
            active={position === POSITION.LONG}
            data-id={POSITION.LONG}
            onClick={handleChangeType}
          >
            {t('long')}
          </Item>
        </Header>
        <DurationContainer>
          {/* Uncomment when there will be more than 2 durations */}
          {/* <span>{t('duration')}</span> */}
          {/* <Dropdown */}
          {/*  // @ts-expect-error */}
          {/*  onChange={setProductId} */}
          {/*  value={productId} */}
          {/*  items={contracts.map((item) => item.id)} */}
          {/*  translations={translations} */}
          {/*  minWidth={73} */}
          {/* /> */}
          <PositionExplanation>
            {t(`${position}_position_explanation`)}
          </PositionExplanation>
        </DurationContainer>
        <Balance>
          <UserBalance />
        </Balance>
        <InputDropdown
          dropdown={dropdown}
          placeholder="0"
          onChange={handleAmountChange}
          value={separateThousands(amount)}
          error={field === 'amount'}
          btnText={t('max_button')}
          btnClick={handleMaxClick}
          btnHighlighted={isMaxAmount}
          type="tel"
        />
        <Helper error={field === 'amount'}>
          {field === 'amount' && key ? t(key, params) : ' '}
        </Helper>

        <Button type="submit" fullWidth disabled={isLoading}>
          {t(`start_${position}`)}
        </Button>
      </Container>
      <StartContractDetails
        oraclePrice={oraclePrice}
        show={!!amount}
        position={position}
        premiums={premiums}
        amount={amountInBCH}
        prices={prices}
      />
    </PageWrapper>
  );
};

const PageWrapper = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-bottom: 20px;
`;

const Container = styled.div`
  width: auto;
  padding: 44px 44px;
  box-sizing: border-box;
  position: relative;
  border-radius: 4px;
  background-color: ${({ theme }): string => theme.background.secondary};
  z-index: 1;
  max-width: 400px;
  margin-top: 10px;
  order: 1;
  ${({ theme }): string => `
    ${theme.mediaTablet} {
    order: 2;
      background-color: ${theme.background.primary};
      border: 1px solid ${theme.border.default};
      width: 400px;
    }
  `}
`;

const Header = styled.div`
  width: 100%;
  padding-bottom: 27px;
  border-bottom: 1px solid ${({ theme }): string => theme.border.section};
  // TODO: change to 40px when dropdown comes back
  margin-bottom: 32px;
  display: flex;
  justify-content: center;
  position: relative;
  & > div:first-of-type {
    margin-right: 15px;
  }
  & > div:last-of-type {
    margin-left: 15px;
  }
  ${({ theme }): string => `
    ${theme.mediaTablet}{
      border-color: ${theme.border.default};
    }
  `}
`;

interface IItem {
  theme: Theme;
  active: boolean;
}

const Item = styled.div`
  font-size: 1rem;
  font-weight: bold;
  color: ${({ theme }): string => theme.text.primary};
  &:hover {
    cursor: pointer;
  }
  ${({ active, theme }: IItem): string =>
    active
      ? `
      border-bottom: 3px solid ${theme.menu.activeColor}
    `
      : ''}
`;

const DurationContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  color: ${({ theme }): string => theme.text.primary};
  & > span {
    margin-right: 30px;
  }
  // TODO: change to 24px when dropdown comes back
  margin-bottom: 32px;
  ${({ theme }): string => `
    ${theme.mediaTablet}{
      margin-bottom: 40px; 
    }
  `};
  // TODO: remove when dropdown comes back
  height: 3.5625rem;
`;

const Balance = styled.div`
  margin-bottom: 8px;
`;

const Helper = styled.div`
  line-height: 1rem;
  min-height: 1rem;
  margin: 5px 0 14px;
  font-size: 14px;
  &:empty {
    display: none;
  }
  color: ${({ error, theme }: IHelper): string =>
    error ? theme.input.errorBorder : theme.text.primary};
`;

const StyledOraclePrice = styled(OraclePrice)`
  ${({ theme }): string => `
    ${theme.mediaTablet}{
      display: none;
    }
  `}
`;

const ContractWrapper = styled.section`
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
  max-width: 400px;
  text-align: center;
  margin-top: 30px;
  & > div {
    justify-content: center;
    &:first-of-type,
    &:nth-of-type(2) {
      margin-bottom: 5px;
    }
  }
  ${({ theme }): string => `
  ${theme.mediaTablet} {
  margin-top: 0;
  
  max-width: none;
  & > div {
    &:first-of-type {
      margin-right: 20px;
      margin-bottom: 0;
    }
    &:last-of-type {
      margin-left: 20px;
    }
  }
    flex-direction: row;
    margin: 10px 0 150px;
  }
  ${theme.mediaDesktop}{
    margin: 10px 0 50px;
    visibility: hidden;
  }
`};
`;

const PositionExplanation = styled.span`
  margin: 0 !important;
  text-align: center;
`;
