import React, { FC, ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import Big from 'big.js';
import styled from '~config/theme';
import InfoSvg from '~icons/info.svg';
import { Tooltip, Premium } from '~components';
import { formatFiat } from '~utils/formatters';
import { IDashboardPageProps } from '../../types';
import {
  BCH_DP,
  DUST_SATOSHIS,
  POSITION,
  SATOSHIS_IN_BCH,
} from '~config/constants';
import { calculatePremium } from '~utils/contract';

interface IProps {
  show: boolean;
  oraclePrice: IDashboardPageProps['oraclePrice'];
  position: POSITION;
  premiums: IDashboardPageProps['premiums'];
  amount: string;
  prices: IDashboardPageProps['prices'];
}

export const StartContractDetails: FC<IProps> = ({
  oraclePrice,
  show,
  position,
  premiums,
  amount,
  prices,
}: IProps): ReactElement | null => {
  const { t } = useTranslation();
  const { hedgePremium, longPremium } = premiums; // maker premiums
  const { hedgePrice, longPrice } = prices; // maker prices
  const { HEDGE } = POSITION;
  const makerPremium = (position === HEDGE ? longPremium : hedgePremium) || 0;
  const takerPrice = (position === HEDGE ? longPrice : hedgePrice) || 0;

  const realAmount = Big(amount || 0)
    .times(takerPrice)
    .times(100)
    .round(0, 0)
    .div(takerPrice || 1)
    .div(100)
    .round(BCH_DP, 3);

  const hedgeInputBCH = position === HEDGE ? realAmount : realAmount.mul(3);
  const longInputBCH =
    position === HEDGE ? realAmount.div(3).round(8, 0) : realAmount;

  const earnSatoshis = calculatePremium({
    position,
    longInputSats: longInputBCH.times(SATOSHIS_IN_BCH).toString(),
    hedgeInputSats: hedgeInputBCH.times(SATOSHIS_IN_BCH).toString(),
    premium: makerPremium || 0,
  });
  const earnUSD = Big(earnSatoshis)
    .div(SATOSHIS_IN_BCH)
    .mul(oraclePrice)
    .toFixed(2);

  const premiumSign = {
    positive: earnSatoshis <= -DUST_SATOSHIS,
    negative: earnSatoshis >= DUST_SATOSHIS,
    none: earnSatoshis > -DUST_SATOSHIS && earnSatoshis < DUST_SATOSHIS,
  };

  return (
    <Container show={show}>
      <Item>
        <Label>
          <span>{t('start_price')}</span>
          <Tooltip
            message={t('bch_usd_price_at_contract_start')}
            position="bottom"
          >
            <InfoIcon />
          </Tooltip>
        </Label>
        <Value>
          {formatFiat(Big(takerPrice).div(100).toString(), {
            zeroFraction: true,
          })}
        </Value>
      </Item>
      <Item>
        <Label>
          <span>{t(premiumSign.positive ? 'start_earn' : 'start_pay')}</span>
          <Tooltip
            message={t(
              premiumSign.positive
                ? 'approximated_earnings'
                : 'approximated_cost'
            )}
          >
            <InfoIcon />
          </Tooltip>
        </Label>
        <Premium
          positive={premiumSign.positive && !Big(earnUSD).eq(0)}
          negative={premiumSign.negative && !Big(earnUSD).eq(0)}
        >
          {formatFiat(earnUSD, { zeroFraction: true })}
        </Premium>
      </Item>
    </Container>
  );
};

interface IContainer {
  show: boolean;
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  max-width: 400px;
  width: 100%;
  box-sizing: border-box;
  border-radius: 0 0 4px 4px;
  padding: 0 44px;
  transform: ${({ show }: IContainer): string =>
    show ? 'translateY(0%)' : 'translateY(-110%)'};
  transition: transform 200ms ease-in-out;
  font-size: 16px;
  & > div:not(:last-of-type) {
    margin-bottom: 14px;
  }
  order: 2;
  ${({ theme }): string => `
    ${theme.mediaTablet} {
      order: 3;
      max-width: 350px;
      font-size: 18px;
      padding: 20px;
      border-width: 0 1px 1px 1px;
      border-style: solid;
      border-color: ${theme.border.section};
    }
  `}
`;

const Item = styled.div`
  display: flex;
  justify-content: space-between;
`;

const Label = styled.div`
  & > span:first-of-type {
    color: ${({ theme }): string => theme.text.header};
    margin-right: 10px;
  }
`;

const Value = styled.div`
  color: ${({ theme }): string => theme.text.primary};
`;

const InfoIcon = styled(InfoSvg as 'img')`
  width: 16px;
  height: 16px;
  fill: ${({ theme }): string => theme.text.primary};
  vertical-align: middle;
  &:hover {
    cursor: pointer;
  }
`;
