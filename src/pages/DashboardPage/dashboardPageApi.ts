import {
  sign,
  // @ts-expect-error
} from 'bitauth/lib/bitauth-browserify';
import request from '~providers/helpers';
import { CONTRACTS, POSITION } from '~config/constants';
import { IPosition } from '~pages/PositionsPage/types';
import { ISignedTransaction } from '~components/Modals/WithdrawModal/withdrawModalApi';

interface IGetEntryDataProps {
  productId: string;
  activePublicKey: string;
  activePrivateKey: string;
}

export interface IEntryDataRes {
  feeAddress: string;
  hedgePremium: number;
  hedgePrice: number;
  longPremium: number;
  longPrice: number;
  makerPubKey: string;
  maxHedgeSatoshis: number;
  maxLongSatoshis: number;
  startBlockHeight: number;
}

export const getEntryData = async ({
  productId,
  activePublicKey,
  activePrivateKey,
}: IGetEntryDataProps): Promise<IEntryDataRes> => {
  const url = `/futures/${productId}/entry-data`;
  const timestamp = Date.now();
  const signature = sign(
    `${request.defaults.baseURL}${url}${timestamp}`,
    activePrivateKey
  ).toString('hex');

  return request.get(url, {
    headers: {
      'x-identity': activePublicKey,
      'x-signature': signature,
      'x-timestamp': timestamp,
    },
  });
};

export interface IContractData {
  takerSide: POSITION;
  takerPubKey: string;
  hedgeUnits: number;
  contractAddress: string;
  fundingTransaction: ISignedTransaction;
  id: typeof CONTRACTS[number];
}

export interface IPostContract {
  data: IContractData;
  activePublicKey: string;
  activePrivateKey: string;
}

export const postContract = async ({
  data,
  activePublicKey,
  activePrivateKey,
}: IPostContract): Promise<IPosition> => {
  const url = `/futures/${data.id}`;
  const timestamp = Date.now();
  const signature = sign(
    `${request.defaults.baseURL}${url}${timestamp}`,
    activePrivateKey
  ).toString('hex');

  return request.post(url, data, {
    headers: {
      'x-identity': activePublicKey,
      'x-signature': signature,
      'x-timestamp': timestamp,
    },
  });
};

export interface IGetFeeAddress {
  contractAddress: string;
  activePublicKey: string;
  activePrivateKey: string;
}

interface IFeeAddress {
  feeAddress: string;
}

export const getFeeAddress = async ({
  contractAddress,
  activePublicKey,
  activePrivateKey,
}: IGetFeeAddress): Promise<IFeeAddress> => {
  const url = '/futures/fee-address';
  const timestamp = Date.now();
  const signature = sign(
    `${request.defaults.baseURL}${url}${timestamp}`,
    activePrivateKey
  ).toString('hex');

  return request.post(
    url,
    { contractAddress },
    {
      headers: {
        'x-identity': activePublicKey,
        'x-signature': signature,
        'x-timestamp': timestamp,
      },
    }
  );
};
