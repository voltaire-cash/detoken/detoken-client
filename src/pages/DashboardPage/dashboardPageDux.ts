import { createAction, createReducer } from 'redux-act';
import { Dispatch } from 'react';
import Big from 'big.js';
import { createSelector } from 'reselect';
import { batch } from 'react-redux';
import i18next from 'i18next';
import { AnyHedgeManager } from '@generalprotocols/anyhedge';
import { ERROR_KEYS, IError } from '~types/Error';
import { IState } from '~dux/rootDux';
import { oraclePriceSelector, sendWSMessageAction } from '~dux/app/appDux';
import {
  receiveUTXOsMessage,
  receiveHistoryMessage,
  UTXOsSelector,
  walletBalanceSelector,
  removeUTXO,
  addSpentUTXOs,
  removeSpentUTXOs,
} from '~dux/wallet/walletDux';
import {
  addSnackbarAction,
  removeSnackbarAction,
} from '~dux/notifications/notificationsDux';
import {
  POSITION,
  CONTRACTS,
  SATOSHIS_IN_BCH,
  MAX_HEDGE_UNITS,
  MIN_HEDGE_UNITS,
  BCH_DP,
  DUST_SATOSHIS,
} from '~config/constants';
import {
  getEntryData,
  IEntryDataRes,
  IContractData,
  postContract,
  IPostContract,
  getFeeAddress,
  IGetFeeAddress,
} from './dashboardPageApi';
import {
  getPublicKeysSelector,
  getPrivateKeysSelector,
  getAddressesSelector,
} from '~dux/user/userDux';
import { openModal, closeModal } from '~dux/modals/modalsDux';
import { IFuture } from '~dux/app/appApi';
import { getMargin } from '~utils/contract';
import {
  createPartialFundingTransaction,
  signPartialFundingTransaction,
} from '~utils/funding';
import { addPositionAction } from '~pages/PositionsPage/positionsPageDux';

interface IPremiums {
  hedgePremium: number;
  longPremium: number;
}

interface IPrices {
  hedgePrice: number;
  longPrice: number;
}

interface IMaximums {
  maxHedgeSatoshis: number;
  maxLongSatoshis: number;
}

interface IPremiumsPrices {
  premiums: IPremiums;
  prices: IPrices;
  maximums: IMaximums;
}

export interface IDashboardPageState {
  isLoading: boolean;
  error: IError | null;
  success: boolean;
  premiums: Partial<IPremiums>;
  prices: Partial<IPrices>;
  maximums: Partial<IMaximums>;
  contractData: IContractData | {};
  amount: string;
  currency: string;
  isMaxAmount: boolean;
  position: POSITION;
  productId: typeof CONTRACTS[number];
}

export interface IContractInfoProps {
  position: POSITION;
  amount: string;
  productId: typeof CONTRACTS[number];
}

interface IIntermediateContractData {
  takerSide: POSITION;
  hedgeUnits: number;
  contractAddress: string;
  id: typeof CONTRACTS[number];
  transactionData: {
    premiumSatoshis: number;
    takerInputSatoshis: number;
    makerInputSatoshis: number;
    dustCost: number;
    minerCost: number;
    productFee: string;
    feeChargedSatoshis: number;
  };
}

const DEFAULT_STATE = {
  isLoading: false,
  error: null,
  success: false,
  premiums: {},
  prices: {},
  maximums: {},
  contractData: {},
  amount: '',
  currency: 'BCH',
  isMaxAmount: false,
  position: POSITION.HEDGE,
  productId: CONTRACTS[0],
};

// Selectors
const duxSelector = (state: IState): IDashboardPageState => state.dashboardPage;
const manager = new AnyHedgeManager();

export const isDashboardPageLoadingSelector = createSelector(
  (state: IState) => state,
  ({ app, dashboardPage }) =>
    app.isDataServerConnecting || dashboardPage.isLoading
);

export const dashboardPageErrorsSelector = createSelector(
  duxSelector,
  ({ error }) => error
);

// Sync Actions
const loading = createAction('loading');
const success = createAction('success');
const failed = createAction<IError | null>('failed');

const updatePremiumsPricesAction = createAction<IPremiumsPrices>(
  'updatePremiumsPricesAction'
);
const saveContractDataAction = createAction<IIntermediateContractData>(
  'saveContractDataAction'
);
const setAmountAction = createAction<string>('setAmountAction');

const setCurrencyAction = createAction<string>('setCurrencyAction');

const isMaxAmountAction = createAction<boolean>('isMaxAmountAction');

const setPositionAction = createAction<POSITION>('setPositionAction');

const setProductIdAction = createAction<typeof CONTRACTS[number]>(
  'setProductIdAction'
);

export const setError = createAction<IError | null>('setError');

type Action = ReturnType<
  | typeof receiveUTXOsMessage
  | typeof receiveHistoryMessage
  | typeof failed
  | typeof loading
  | typeof addSnackbarAction
  | typeof openModal
  | typeof updatePremiumsPricesAction
  | typeof saveContractDataAction
  | typeof removeSnackbarAction
  | typeof closeModal
  | typeof setAmountAction
  | typeof addPositionAction
  | typeof isMaxAmountAction
  | typeof setPositionAction
  | typeof setProductIdAction
  | typeof addSpentUTXOs
  | typeof removeSpentUTXOs
>;

export const getWalletData = (props: {
  activeAddress: string;
  changeAddress: string;
}) => async (dispatch: Dispatch<Action>): Promise<void> => {
  const { activeAddress, changeAddress } = props;
  const addresses = [activeAddress, changeAddress];
  dispatch(sendWSMessageAction({ type: 'list_unspent', addresses }));
};

// TODO: this function is used to provide fresh data for calculating user earning in block below the start contract one
// it should be removed once we'll move entry data to WS
export const getPremiumPriceData = (
  productId: typeof CONTRACTS[number]
) => async (
  dispatch: Dispatch<Action>,
  getState: () => IState
): Promise<void> => {
  const state = getState();
  const { activePrivateKey } = getPrivateKeysSelector(state);
  const { activePublicKey } = getPublicKeysSelector(state);
  try {
    const {
      hedgePremium,
      longPremium,
      hedgePrice,
      longPrice,
      maxHedgeSatoshis,
      maxLongSatoshis,
    } = await getEntryData({
      productId,
      activePublicKey,
      activePrivateKey,
    });
    dispatch(
      updatePremiumsPricesAction({
        premiums: { hedgePremium, longPremium },
        prices: { hedgePrice, longPrice },
        maximums: { maxHedgeSatoshis, maxLongSatoshis },
      })
    );
    // eslint-disable-next-line no-empty
  } catch (e) {}
};

export const updateEntryData = (
  dispatch: Dispatch<Action>,
  getState: () => IState
): void => {
  CONTRACTS.forEach((contract) =>
    getPremiumPriceData(contract)(dispatch, getState)
  );
};

export const setAmount = (amount: string) => (
  dispatch: Dispatch<Action>
): void => {
  dispatch(setAmountAction(amount));
};

export const setIsMaxAmount = (status: boolean) => (
  dispatch: Dispatch<Action>
): void => {
  dispatch(isMaxAmountAction(status));
};

export const setPosition = (position: POSITION) => (
  dispatch: Dispatch<Action>
): void => {
  dispatch(setPositionAction(position));
};

export const setProductId = (productId: typeof CONTRACTS[number]) => (
  dispatch: Dispatch<Action>
): void => {
  batch(() => {
    dispatch(setProductIdAction(productId));
    dispatch(isMaxAmountAction(false));
  });
};

export const showContractInfo = ({
  amount, // in BCH
  position,
  productId,
}: IContractInfoProps) => async (
  dispatch: Dispatch<Action>,
  getState: () => IState
): Promise<any> => {
  dispatch(loading());

  const state = getState();
  const isModalOpen = state.modals.longHedgeConfirm.isOpen;
  const { HEDGE } = POSITION;
  const { futuresList } = state.app; // config from server
  const { activePrivateKey } = getPrivateKeysSelector(state);
  const { activePublicKey } = getPublicKeysSelector(state);
  const balance = walletBalanceSelector(state);
  const { currency } = state.dashboardPage;
  let entryData = {} as IEntryDataRes;
  let contract;

  // Get product config by id
  const {
    oraclePubKey,
    maturityModifier,
    fee,
    lowLiquidationMulti,
    highLiquidationMulti,
    minHedgeSatoshis,
    minLongSatoshis,
  } = futuresList.find(({ id }) => id === productId) as IFuture;

  // Get entry data
  try {
    entryData = await getEntryData({
      productId,
      activePublicKey,
      activePrivateKey,
    });
  } catch (e) {
    batch(() => {
      dispatch(failed(e));
      dispatch(
        addSnackbarAction({ message: 'e.something_wrong', variant: 'error' })
      );
    });
    return;
  }

  const {
    hedgePremium,
    longPremium,
    startBlockHeight,
    hedgePrice,
    longPrice,
    maxLongSatoshis,
    maxHedgeSatoshis,
    makerPubKey,
  } = entryData;

  // Prepare contract data
  const startPrice = position === HEDGE ? hedgePrice : longPrice;
  const margin = getMargin(lowLiquidationMulti);
  // we need us cents here
  let hedgeUnits;
  if (currency === 'USD') {
    hedgeUnits =
      position === HEDGE
        ? Big(amount).times(100).toFixed()
        : Big(amount).times(100).times(margin).toFixed();
  } else {
    hedgeUnits =
      position === HEDGE
        ? Big(amount).times(hedgePrice).round(0, 0).toString()
        : Big(amount).times(longPrice).round(0, 0).times(margin).toString();
  }

  if (Big(hedgeUnits).lt(MIN_HEDGE_UNITS)) {
    hedgeUnits = MIN_HEDGE_UNITS;
  }

  // calculate max and min for the contract
  const maxAhCents =
    position === HEDGE
      ? Big(MAX_HEDGE_UNITS)
      : Big(MAX_HEDGE_UNITS).div(margin).round(0, 0);
  const maxAhSatoshis = maxAhCents
    .div(startPrice)
    .round(8, 0)
    .mul(SATOSHIS_IN_BCH);
  const minAhCents =
    position === HEDGE
      ? Big(MIN_HEDGE_UNITS)
      : Big(MIN_HEDGE_UNITS).div(margin).round(0, 3);
  const minAhSatoshis = minAhCents
    .div(startPrice)
    .round(8, 3)
    .mul(SATOSHIS_IN_BCH);

  const maxMMTakerSatoshis =
    position === HEDGE
      ? Big(maxLongSatoshis).times(margin).round(0, 3)
      : Big(maxHedgeSatoshis).div(margin).round(0, 3);
  const maxTakerSatoshis = Math.min(
    Number(maxAhSatoshis),
    Number(maxMMTakerSatoshis)
  );

  const minMMTakerSatoshis =
    position === HEDGE ? minHedgeSatoshis : minLongSatoshis;
  const minTakerSatoshis = Math.max(Number(minAhSatoshis), minMMTakerSatoshis);

  const hedgePublicKey = position === HEDGE ? activePublicKey : makerPubKey;
  const longPublicKey = position === HEDGE ? makerPubKey : activePublicKey;

  if (hedgeUnits > MAX_HEDGE_UNITS) {
    const valueBCH = Big(maxAhSatoshis).div(SATOSHIS_IN_BCH);
    const valueUSD = Big(maxAhCents).div(100);
    dispatch(
      failed({
        key: ERROR_KEYS.CONTRACT_AMOUNT_GREATER_MAXIMUM,
        field: 'amount',
        params: {
          value:
            currency === 'USD'
              ? `$${valueUSD.toString()}`
              : `${valueBCH.toString()} BCH`,
        },
      })
    );
    if (isModalOpen) {
      dispatch(closeModal({ type: 'longHedgeConfirm' }));
    }
    return;
  }

  // Create a contract
  try {
    contract = await manager.createContract(
      oraclePubKey,
      hedgePublicKey,
      longPublicKey,
      Number(hedgeUnits), // expected number in BCH
      startPrice, // expected number
      startBlockHeight,
      0,
      maturityModifier,
      Number(highLiquidationMulti), // expected number
      Number(lowLiquidationMulti) // expected number
    );
  } catch (e) {
    dispatch(
      failed({ field: 'amount', key: ERROR_KEYS.INVALID_CONTRACT_INPUTS })
    );
    return;
  }

  const { address: contractAddress, metadata, parameters } = contract;
  const { longInputSats, hedgeInputSats, minerCost, dustCost } = metadata;

  const takerInputSatoshis =
    position === HEDGE ? hedgeInputSats : longInputSats;
  const makerInputSatoshis =
    position === HEDGE ? longInputSats : hedgeInputSats;

  // Dispatch an error if taker satoshis bigger than maxTakerSatoshis
  if (takerInputSatoshis > maxTakerSatoshis) {
    const valueBCH = Big(maxTakerSatoshis).div(SATOSHIS_IN_BCH);
    const valueUSD = valueBCH.mul(startPrice).div(100).round(2, 0);
    dispatch(
      failed({
        key: ERROR_KEYS.CONTRACT_AMOUNT_GREATER_MAXIMUM,
        field: 'amount',
        params: {
          value:
            currency === 'USD'
              ? `$${valueUSD.toString()}`
              : `${valueBCH.toString()} BCH`,
        },
      })
    );
    if (isModalOpen) {
      dispatch(closeModal({ type: 'longHedgeConfirm' }));
    }
    return;
  }

  // Dispatch an error if taker satoshis smaller than minTakerSatoshis
  if (takerInputSatoshis < minTakerSatoshis) {
    const minUserBCH = Big(minTakerSatoshis)
      .div(SATOSHIS_IN_BCH)
      .mul(startPrice)
      .round(0, 3)
      .div(startPrice)
      .round(8, 3);
    const minUserUSD = minUserBCH.mul(startPrice).div(100).toFixed(2);
    dispatch(
      failed({
        key: ERROR_KEYS.CONTRACT_AMOUNT_BELOW_MINIMUM,
        field: 'amount',
        params: {
          value:
            currency === 'USD'
              ? `$${minUserUSD}`
              : `${minUserBCH.toString()} BCH`,
        },
      })
    );
    if (isModalOpen) {
      dispatch(closeModal({ type: 'longHedgeConfirm' }));
    }
    return;
  }

  const premium = position === HEDGE ? longPremium : hedgePremium;

  let premiumSatoshis = Number(
    Big(makerInputSatoshis).mul(premium).round(0, 3)
  );

  if (Math.abs(premiumSatoshis) < DUST_SATOSHIS) {
    premiumSatoshis = 0;
  }

  // Calculate the fee to charge the user for using Detoken
  const feeChargedSatoshis = Big(fee)
    .div(100)
    .times(takerInputSatoshis)
    .round(0, 3)
    .toString();

  let totalContractCost = Big(takerInputSatoshis)
    .plus(dustCost)
    .plus(minerCost)
    .plus(feeChargedSatoshis);
  if (premiumSatoshis > 0) {
    totalContractCost = totalContractCost.plus(premiumSatoshis);
  }

  if (Big(balance).mul(SATOSHIS_IN_BCH).lt(totalContractCost)) {
    dispatch(
      failed({
        key: ERROR_KEYS.INSUFFICIENT_FUNDS,
        field: 'amount',
      })
    );
    if (isModalOpen) {
      dispatch(closeModal({ type: 'longHedgeConfirm' }));
    }
    return;
  }

  dispatch(
    saveContractDataAction({
      takerSide: position,
      hedgeUnits: Number(hedgeUnits),
      contractAddress: contract.address,
      id: productId,
      transactionData: {
        premiumSatoshis,
        takerInputSatoshis,
        makerInputSatoshis,
        dustCost,
        minerCost,
        productFee: fee,
        feeChargedSatoshis: Number(feeChargedSatoshis),
      },
    })
  );

  dispatch(
    openModal({
      type: 'longHedgeConfirm',
      data: {
        startPrice: metadata.startPrice,
        lowLiquidationMulti,
        maturityModifier: metadata.maturityModifier,
        value: Big(takerInputSatoshis).div(SATOSHIS_IN_BCH).toString(),
        fee,
        position,
        premium,
        hedgeInputSats,
        longInputSats,
        dustCost,
        minerCost,
        premiumSatoshis,
        startBlockHeight: metadata.startBlockHeight,
        contractAddress,
        lowLiquidationPrice: parameters.lowLiquidationPrice,
        highLiquidationPrice: parameters.highLiquidationPrice,
        productId,
      },
    })
  );
  dispatch(success());

  return true;
};

export const sendContract = () => async (
  dispatch: Dispatch<Action>,
  getState: () => IState
): Promise<void> => {
  batch(() => {
    dispatch(removeSnackbarAction(''));
    dispatch(loading());
  });
  const state = getState();
  const {
    activePrivateKey,
    rawActivePrivateKey,
    rawChangePrivateKey,
  } = getPrivateKeysSelector(state);
  const { activeAddress, changeAddress } = getAddressesSelector(state);
  const { activePublicKey } = getPublicKeysSelector(state);
  const { contractData, amount } = state.dashboardPage;
  const {
    id,
    takerSide: position,
    contractAddress,
    transactionData,
    hedgeUnits,
  } = contractData as IIntermediateContractData;
  let feeAddress;
  try {
    const response = await getFeeAddress({
      contractAddress,
      activePrivateKey,
      activePublicKey,
    } as IGetFeeAddress);
    feeAddress = response.feeAddress;
  } catch (err) {
    const { field, key } = err;
    dispatch(
      addSnackbarAction({
        message: i18next.t(key, { field: i18next.t(field) }),
        variant: 'error',
      })
    );
    dispatch(success());
    return;
  }
  // If we are here we need to prepare transaction and sign it and then send

  const UTXOs = UTXOsSelector(state);
  let usedUTXOs = [] as Array<string>;
  const {
    premiumSatoshis,
    takerInputSatoshis,
    makerInputSatoshis,
    dustCost,
    minerCost,
    productFee,
    feeChargedSatoshis,
  } = transactionData;
  // Create a transaction
  let signedTransaction;
  try {
    const {
      transaction,
      usedUTXOs: UTXOsToRemove,
    } = createPartialFundingTransaction({
      premiumSatoshis,
      takerInputSatoshis,
      makerInputSatoshis,
      dustCost,
      minerCost,
      contractAddress,
      changeAddress,
      productFee,
      anyHedgeFeeAddress: feeAddress,
      UTXOs,
      feeChargedSatoshis: Number(feeChargedSatoshis),
    });

    usedUTXOs = UTXOsToRemove;

    signedTransaction = signPartialFundingTransaction({
      transaction,
      UTXOs: UTXOs || {},
      addressToKey: {
        [activeAddress]: rawActivePrivateKey,
        [changeAddress]: rawChangePrivateKey,
      },
    });
  } catch (e) {
    dispatch(failed({ key: e.message, field: 'amount' }));
    return;
  }
  dispatch(addSpentUTXOs(usedUTXOs));
  try {
    const response = await postContract({
      data: {
        takerSide: position,
        takerPubKey: activePublicKey,
        hedgeUnits,
        contractAddress,
        fundingTransaction: signedTransaction,
        id,
      },
      activePrivateKey,
      activePublicKey,
    } as IPostContract);
    batch(() => {
      const { takerSide } = response;
      dispatch(
        addSnackbarAction({
          message: i18next.t('positions_created', { side: takerSide }),
          variant: 'success',
        })
      );
      dispatch(closeModal({ type: 'longHedgeConfirm' }));
      dispatch(setAmountAction(''));
      dispatch(
        addPositionAction(Object.assign(response, { isSettled: false }))
      );
      usedUTXOs.forEach((utxo) => dispatch(removeUTXO(utxo)));

      dispatch(success());
    });
    await getPremiumPriceData(id)(dispatch, getState);
  } catch (err) {
    const { field, key } = err;
    batch(() => {
      dispatch(
        addSnackbarAction({
          message: i18next.t(key, { field: i18next.t(field) }),
          variant: 'error',
        })
      );
      dispatch(removeSpentUTXOs(usedUTXOs));
      if (
        key === 'e.validation_funding_tx' ||
        key === 'e.mismatched_contract' ||
        key === 'e.validation_funding_tx_fee_output'
      ) {
        showContractInfo({
          amount,
          position,
          productId: id,
        })(dispatch, getState);
      }
      dispatch(success());
    });
  }
};

export interface ISetMaxAmount {
  position: POSITION;
  productId: typeof CONTRACTS[number];
}

export const setMaxAmount = ({ position, productId }: ISetMaxAmount) => async (
  dispatch: Dispatch<Action>,
  getState: () => IState
): Promise<string> => {
  const { HEDGE } = POSITION;
  const state = getState();
  const { futuresList } = state.app;
  const balance = walletBalanceSelector(state); // in BCH here
  const { currency, prices, premiums, maximums } = state.dashboardPage;
  const { hedgePrice, longPrice } = prices;
  const { hedgePremium, longPremium } = premiums;
  const { maxHedgeSatoshis, maxLongSatoshis } = maximums;
  const oraclePrice = oraclePriceSelector(state);
  const {
    lowLiquidationMulti,
    highLiquidationMulti,
    maturityModifier,
    fee,
    minHedgeSatoshis,
    minLongSatoshis,
  } = futuresList.find(({ id }) => id === productId) as IFuture;
  const margin = getMargin(lowLiquidationMulti);

  if (
    balance
      .times(SATOSHIS_IN_BCH)
      .lt(position === HEDGE ? minHedgeSatoshis : minLongSatoshis)
  ) {
    dispatch(
      failed({
        key: ERROR_KEYS.INSUFFICIENT_FUNDS,
        field: 'amount',
      })
    );
  }

  const startPrice = position === HEDGE ? hedgePrice : longPrice;
  const hedgeUnits =
    position === HEDGE
      ? Big(balance)
          .times(hedgePrice || 0)
          .round(0, 0)
          .toString()
      : Big(balance)
          .times(longPrice || 0)
          .round(0, 0)
          .times(margin)
          .toString();

  let contract = null;
  try {
    contract = await manager.createContract(
      '',
      '',
      '',
      Number(hedgeUnits), // expected number in BCH
      startPrice || 0, // expected number
      0,
      0,
      maturityModifier,
      Number(highLiquidationMulti), // expected number
      Number(lowLiquidationMulti) // expected number
    );
  } catch (e) {
    dispatch(
      addSnackbarAction({ message: 'e.something_wrong', variant: 'error' })
    );
    return '';
  }
  const { metadata } = contract;
  const { longInputSats, hedgeInputSats, minerCost, dustCost } = metadata;

  const takerInputSatoshis =
    position === HEDGE ? hedgeInputSats : longInputSats;
  const makerInputSatoshis =
    position === HEDGE ? longInputSats : hedgeInputSats;

  const premium = position === HEDGE ? longPremium : hedgePremium;
  let premiumSatoshis = Number(
    Big(makerInputSatoshis)
      .mul(premium || 0)
      .round(0, 3)
  );

  if (Math.abs(premiumSatoshis) < DUST_SATOSHIS) {
    premiumSatoshis = 0;
  }

  // Calculate the fee to charge the user for using Detoken
  const feeChargedSatoshis = Big(fee)
    .div(100)
    .times(takerInputSatoshis)
    .round(0, 3)
    .toString();

  let maxAmount: any;

  maxAmount = Big(balance)
    .mul(SATOSHIS_IN_BCH)
    .minus(dustCost)
    .minus(minerCost)
    .minus(feeChargedSatoshis);

  if (premiumSatoshis > 0) {
    maxAmount = maxAmount.minus(premiumSatoshis);
  }

  const maxMMTakerSatoshis =
    position === HEDGE
      ? Big(maxLongSatoshis || 0)
          .times(margin)
          .round(0, 3)
      : Big(maxHedgeSatoshis || 0)
          .div(margin)
          .round(0, 3);

  if (maxAmount.gt(maxMMTakerSatoshis)) {
    maxAmount = maxMMTakerSatoshis;
  }

  if (currency === 'USD') {
    maxAmount = maxAmount
      .div(SATOSHIS_IN_BCH)
      .mul(oraclePrice)
      .round(2, 0)
      .toString();
  } else {
    maxAmount = maxAmount.div(SATOSHIS_IN_BCH).round(BCH_DP, 0).toString();
  }
  batch(() => {
    dispatch(setAmountAction(maxAmount));
    dispatch(isMaxAmountAction(true));
  });
  return maxAmount;
};

export const setCurrency = (currency: string) => (
  dispatch: Dispatch<Action>,
  getState: () => IState
): void => {
  const state = getState();
  const { isMaxAmount, productId, position } = state.dashboardPage;
  batch(() => {
    dispatch(setCurrencyAction(currency));
    if (isMaxAmount) {
      setMaxAmount({ position, productId })(dispatch, getState);
    }
  });
};

// Async Actions
export default createReducer<IDashboardPageState>(
  {
    [loading.toString()]: (state: IDashboardPageState) => ({
      ...state,
      isLoading: true,
    }),

    [success.toString()]: (state: IDashboardPageState) => ({
      ...state,
      isLoading: false,
    }),

    [failed.toString()]: (state: IDashboardPageState, error: IError) => ({
      ...state,
      error,
      isLoading: false,
    }),

    [setError.toString()]: (
      state: IDashboardPageState,
      error: IError | null
    ) => ({
      ...state,
      error,
      isLoading: false,
    }),
    [updatePremiumsPricesAction.toString()]: (
      state: IDashboardPageState,
      data: IPremiumsPrices
    ) => ({
      ...state,
      premiums: data.premiums,
      prices: data.prices,
      maximums: data.maximums,
    }),
    [saveContractDataAction.toString()]: (
      state: IDashboardPageState,
      contractData: IContractData
    ) => ({
      ...state,
      contractData,
    }),
    [setAmountAction.toString()]: (
      state: IDashboardPageState,
      amount: string
    ) => ({
      ...state,
      amount,
    }),
    [setCurrencyAction.toString()]: (
      state: IDashboardPageState,
      currency: string
    ) => ({
      ...state,
      currency,
    }),
    [isMaxAmountAction.toString()]: (
      state: IDashboardPageState,
      isMaxAmount: boolean
    ) => ({
      ...state,
      isMaxAmount,
    }),
    [setPositionAction.toString()]: (
      state: IDashboardPageState,
      position: POSITION
    ) => ({
      ...state,
      position,
    }),
    [setProductIdAction.toString()]: (
      state: IDashboardPageState,
      productId: typeof CONTRACTS[number]
    ) => ({
      ...state,
      productId,
    }),
  },
  DEFAULT_STATE
);
