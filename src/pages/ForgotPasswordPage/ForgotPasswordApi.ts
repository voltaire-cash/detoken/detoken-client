import request from '~providers/helpers';

const { get } = request;

export const resetPassword = (email: string): Promise<void> => {
  return get(`/password/reset?email=${email}`);
};
