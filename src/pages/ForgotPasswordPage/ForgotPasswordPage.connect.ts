import { connect } from 'react-redux';
import { ForgotPasswordPage } from './ForgotPasswordPage';
import { IMapState, IMapDispatch } from './types';
import { resetPassword, resetErrors } from './ForgotPasswordPageDux';
import { IState } from '~dux/rootDux';

const mapDispatchToProps: IMapDispatch = {
  sendResetPassword: resetPassword,
  resetErrors,
};

const mapStateToProps = (state: IState): IMapState => ({
  passwordResetStatus: state.forgotPasswordPage.passwordResetStatus,
  error: state.forgotPasswordPage.error,
  loading: state.forgotPasswordPage.loading,
});

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordPage);
