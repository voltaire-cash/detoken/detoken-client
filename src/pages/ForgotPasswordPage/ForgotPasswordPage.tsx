import React, { useState, FC, useCallback, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { FormLayout, Form, TextField, PageTitle } from '~components';
import { SuccessReset } from './components/SuccessReset';
import { IForgotPasswordPage } from './types';

export const ForgotPasswordPage: FC<IForgotPasswordPage> = ({
  passwordResetStatus,
  error,
  sendResetPassword,
  resetErrors,
  loading,
}: IForgotPasswordPage) => {
  const { t } = useTranslation();
  const [email, setEmail] = useState<string>('');
  const [showResetSuccess, setShowResetSuccess] = useState<boolean>(false);

  const { field, key } = error || {};

  useEffect(() => {
    if (passwordResetStatus) {
      setShowResetSuccess(passwordResetStatus);
    }
  }, [passwordResetStatus]);

  const handleFormSubmit = useCallback(
    (e: React.FormEvent) => {
      e.preventDefault();
      sendResetPassword(email);
    },
    [email, sendResetPassword]
  );
  const handleInputChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>): void => {
      const { value } = e.currentTarget;
      setEmail(value);
      resetErrors();
    },
    [resetErrors]
  );
  if (showResetSuccess) {
    return <SuccessReset />;
  }
  return (
    <>
      <PageTitle title={`Detoken: ${t('reset_password_page')}`} />
      <FormLayout
        logo
        title={t('reset_password_header')}
        subtitle={t('reset_password_link_will_be_sent')}
      >
        <Form
          onSubmit={handleFormSubmit}
          submitText={t('send_reset_link')}
          submitDisabled={loading}
        >
          <TextField
            name="email"
            type="email"
            value={email}
            onChange={handleInputChange}
            placeholder={t('email')}
            error={field === 'email'}
            helperText={
              field === 'email' && key ? t(key, { field: t(field) }) : ''
            }
          />
        </Form>
      </FormLayout>
    </>
  );
};
