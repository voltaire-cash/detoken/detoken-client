import { createAction, createReducer } from 'redux-act';
import { Dispatch } from 'redux';
import i18next from 'i18next';
import { addSnackbarAction } from '~dux/notifications/notificationsDux';
import { IError } from '~types/Error';
import { resetPassword as resetPasswordRequest } from './ForgotPasswordApi';

export interface IForgotPasswordPageState {
  loading: boolean;
  passwordResetStatus: boolean;
  error: IError | null;
}

const DEFAULT_STATE = {
  loading: false,
  passwordResetStatus: false,
  error: null,
};

const passwordResetStatusAction = createAction<boolean>(
  'passwordResetStatusAction'
);

const loadingAction = createAction<boolean>('loadingAction');

const setErrorAction = createAction<IError | null>('setErrorAction');

type Action = ReturnType<
  | typeof passwordResetStatusAction
  | typeof loadingAction
  | typeof addSnackbarAction
  | typeof setErrorAction
>;

export const resetPassword = (email: string) => async (
  dispatch: Dispatch<Action>
): Promise<void> => {
  dispatch(loadingAction(true));
  try {
    await resetPasswordRequest(email);
    dispatch(passwordResetStatusAction(true));
  } catch (err) {
    const { field, key } = err;
    if (field) {
      if (field === 'activation') {
        dispatch(setErrorAction({ field: 'email', key }));
      } else {
        dispatch(setErrorAction({ field, key }));
      }
    } else {
      dispatch(addSnackbarAction({ message: i18next.t(key) }));
    }
  }
};

export const resetErrors = () => (dispatch: Dispatch<Action>): void => {
  dispatch(setErrorAction(null));
};

export default createReducer<IForgotPasswordPageState>(
  {
    [passwordResetStatusAction.toString()]: (
      state: IForgotPasswordPageState,
      status: boolean
    ) => ({
      ...state,
      passwordResetStatus: status,
      loading: false,
    }),
    [loadingAction.toString()]: (
      state: IForgotPasswordPageState,
      loading: boolean
    ) => ({
      ...state,
      error: loading ? DEFAULT_STATE.error : state.error,
      loading,
    }),
    [setErrorAction.toString()]: (
      state: IForgotPasswordPageState,
      error: IError | null
    ) => ({
      ...state,
      error,
      loading: false,
    }),
  },
  DEFAULT_STATE
);
