import React, { FC, ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import { FormLayout } from '~components';
import styled from '~config/theme';
import SuccessSvg from '~icons/success.svg';

export const SuccessReset: FC = (): ReactElement => {
  const { t } = useTranslation();
  return (
    <FormLayout icon={<StyledSuccess />} logo>
      <Text>{t('reset_link_was_sent')}</Text>
    </FormLayout>
  );
};

const Text = styled.div`
  text-align: center;
  font-size: calc(4rem / 3);
  font-weight: bold;
`;

const StyledSuccess = styled(SuccessSvg as 'img')`
  fill: ${({ theme }): string => theme.icons.success};
`;
