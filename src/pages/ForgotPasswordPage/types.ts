import { IError } from '~types/Error';

export interface IMapState {
  passwordResetStatus: boolean;
  error: IError | null;
  loading: boolean;
}

export interface IMapDispatch {
  sendResetPassword: (email: string) => void;
  resetErrors: () => void;
}

export interface IForgotPasswordPage extends IMapState, IMapDispatch {}
