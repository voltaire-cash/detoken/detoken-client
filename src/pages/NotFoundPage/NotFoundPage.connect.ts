import { connect } from 'react-redux';
import { IState } from '~dux/rootDux';
import { NotFoundPage } from './NotFoundPage';
import { NotFoundNotification } from './NotFoundPageDux';

const mapDispatchToProps = {
  showNotification: NotFoundNotification,
};

const mapStateToProps = (state: IState) => ({
  isAuthorized: state.user.isAuthorized,
});

export default connect(mapStateToProps, mapDispatchToProps)(NotFoundPage);
