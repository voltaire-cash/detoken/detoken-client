import { connect } from 'react-redux';
import { PositionsPage } from './PositionsPage';
import { IMapState, IMapDispatch } from './types';
import { IState } from '~dux/rootDux';
import { openModal } from '~dux/modals/modalsDux';
import {
  positionsSelector,
  settledContractTransactionsSelector,
  toggleActiveOnly,
} from './positionsPageDux';

const mapDispatchToProps: IMapDispatch = {
  openModal,
  toggleActiveOnly,
};

const mapStateToProps = (state: IState): IMapState => ({
  blockHeight: state.app.oraclePrice.blockHeight,
  oraclePrice: state.app.oraclePrice.price,
  loading: state.positionsPage.loading,
  positions: positionsSelector(state),
  settledContractTransactions: settledContractTransactionsSelector(state),
  activeOnly: state.positionsPage.activeOnly,
});

export default connect(mapStateToProps, mapDispatchToProps)(PositionsPage);
