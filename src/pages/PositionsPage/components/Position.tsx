import React, {
  Fragment,
  FC,
  ReactElement,
  useEffect,
  useState,
  useCallback,
} from 'react';
import { useTranslation } from 'react-i18next';
import Big from 'big.js';
import { IFuturesPosition } from '../types';
import styled, { Theme } from '~config/theme';
import { getTranslationKey, getDurationTranslation } from '~utils/translations';
import { getExistingContractParams, getMargin } from '~utils/contract';
import { blocksToDuration, formatFiat } from '~utils/formatters';
import { Premium } from '~components';
import ZoomOutSvg from '~icons/zoomOut.svg';
import {
  POSITION,
  BLOCKS_IN_DAY,
  SATOSHIS_IN_BCH,
  BCH_DP,
} from '~config/constants';
import {
  IContractMetadata,
  IContractParameters,
  ISimulationOutput,
  IParsedSettlementData,
} from '~types/anyHedge';

interface IOnClick {
  metadata: IContractMetadata | {};
  parameters: IContractParameters | {};
  simulation: ISimulationOutput | {};
  contract: IFuturesPosition;
  settlement: IParsedSettlementData | {};
}

interface IProps {
  data: IFuturesPosition;
  onClick: (props: IOnClick) => void;
  blockHeight: number;
  oraclePrice: string;
  index: number;
  settlementTransaction: string;
}

export const Position: FC<IProps> = ({
  data,
  blockHeight,
  onClick,
  oraclePrice,
  index,
  settlementTransaction,
}: IProps): ReactElement => {
  const { t } = useTranslation();
  const [contractMetadata, setContractMetadata] = useState({});
  const [contractParameters, setContractParameters] = useState({});
  const [contractSimulation, setContractSimulation] = useState({});
  const [longInputSats, setLongInputSats] = useState<number>(0);
  const [hedgeInputSats, setHedgeInputSats] = useState<number>(0);
  const [noData, setNoData] = useState(true);
  const [settlementData, setSettlementData] = useState({});

  useEffect(() => {
    let fetching = true;
    getExistingContractParams({
      data,
      oraclePrice,
      settlementTransaction,
    }).then(({ metadata, parameters, simulation, settlement }) => {
      if (fetching) {
        setContractMetadata(metadata);
        setContractParameters(parameters);
        setContractSimulation(simulation);
        setNoData(false);
        setLongInputSats(metadata.longInputSats);
        setHedgeInputSats(metadata.hedgeInputSats);
        setSettlementData(settlement);
      }
    });
    return (): void => {
      fetching = false;
    };
  }, [data, oraclePrice, settlementTransaction]);

  const handleClick = useCallback(() => {
    if (noData) return;
    onClick({
      metadata: contractMetadata,
      parameters: contractParameters,
      simulation: contractSimulation,
      contract: data,
      settlement: settlementData,
    });
  }, [
    contractMetadata,
    contractParameters,
    contractSimulation,
    data,
    onClick,
    noData,
    settlementData,
  ]);

  const { HEDGE } = POSITION;

  const {
    isSettled,
    startBlockHeight,
    maturityModifier,
    address: addr,
    takerSide: position,
    hedgeUnits,
    startPrice,
    lowLiquidationMulti,
  } = data;

  const margin = getMargin(lowLiquidationMulti);

  const leftBlocks = startBlockHeight + maturityModifier - blockHeight;
  const expiration = blocksToDuration(leftBlocks);

  const calcStartValueBCH = Big(hedgeUnits)
    .div(startPrice)
    .div(position === HEDGE ? 1 : margin)
    .toFixed(BCH_DP);

  const ahStartValueBCH = Big(
    position === HEDGE ? hedgeInputSats : longInputSats
  )
    .div(SATOSHIS_IN_BCH)
    .toFixed(BCH_DP);

  const startValueBCH = Big(ahStartValueBCH).eq(0)
    ? calcStartValueBCH
    : ahStartValueBCH;

  const startValueUSD = Big(hedgeUnits)
    .div(100)
    .div(position === HEDGE ? 1 : margin)
    .toString();

  const dayDuration = Big(maturityModifier).div(BLOCKS_IN_DAY).toString();

  const [days, hours] = expiration;
  const expirationStr =
    days || hours
      ? `~${getDurationTranslation(expiration)}`
      : t('less_then_one_hour');

  const expiresIn = `${leftBlocks} ${t(
    getTranslationKey(leftBlocks, 'block')
  )} (${expirationStr})`;

  // @ts-expect-error
  const hedgePayoutSats = contractSimulation?.hedgePayoutSats || 0;
  // @ts-expect-error
  const longPayoutSats = contractSimulation?.longPayoutSats || 0;

  const currentValueBCH = Big(
    position === HEDGE ? hedgePayoutSats : longPayoutSats
  ).div(SATOSHIS_IN_BCH);

  const currentValueUSD = formatFiat(
    currentValueBCH.mul(oraclePrice).toFixed(2),
    { zeroFraction: true }
  );

  const {
    // @ts-expect-error
    hedgeSatoshis: settledHedgeSats,
    // @ts-expect-error
    longSatoshis: settledLongSats,
  } = settlementData;

  const settledValueBCH = Big(
    position === HEDGE ? settledHedgeSats || 0 : settledLongSats || 0
  ).div(SATOSHIS_IN_BCH);

  const profitBCH = isSettled
    ? settledValueBCH.minus(startValueBCH)
    : currentValueBCH.minus(startValueBCH);

  return (
    <Fragment key={addr}>
      <Item first key={`${addr}_type`}>
        {t(position)}
      </Item>
      <Item padding key={`${addr}_profit`}>
        {isSettled && settledValueBCH.eq(0) ? (
          <>{t('not_available')}</>
        ) : (
          <Premium positive={profitBCH.gt(0)} negative={profitBCH.lt(0)}>
            {profitBCH.toFixed(BCH_DP)} BCH
          </Premium>
        )}
      </Item>
      <Item padding key={`${addr}_cur_val`}>
        {!isSettled ? (
          <>
            {currentValueBCH.toFixed(BCH_DP)} BCH
            <Fiat>&nbsp;~ {currentValueUSD}</Fiat>
          </>
        ) : (
          <Hyphen>&#8211;</Hyphen>
        )}
      </Item>
      <Item padding key={`${addr}_start_val`}>
        {startValueBCH} BCH
        <Fiat>&nbsp;~ {formatFiat(startValueUSD, { zeroFraction: true })}</Fiat>
      </Item>
      <Item padding key={`${addr}_start_price`}>
        {formatFiat(Big(startPrice).div(100).toString(), {
          zeroFraction: true,
        })}
      </Item>
      <Item padding key={`${addr}_duration`} alignRight>
        {dayDuration} {t(getTranslationKey(dayDuration, 'day'))}
      </Item>
      <Item padding key={`${addr}_expiration`} alignRight>
        {!isSettled ? expiresIn : t('expired')}
      </Item>
      <Item padding last key="modal_opener">
        <ModalOpener onClick={handleClick} data-index={index} />
      </Item>
    </Fragment>
  );
};

interface IItem {
  padding?: boolean;
  first?: boolean;
  last?: boolean;
  alignRight?: boolean;
  theme: Theme;
}

const Item = styled.div`
  padding-left: ${({ padding }: IItem): string => (padding ? '25px' : '')};
  ${({ first }: IItem): string =>
    first ? 'padding-left: 20px; display: flex; align-items: center;' : ''};
  padding-right: ${({ last }: IItem): string => (last ? '20px' : '')};
  color: ${({ theme }): string => theme.text.primary};
  margin-bottom: 15px;
  line-height: 21px;
  text-align: ${({ alignRight }): string => (alignRight ? 'right' : 'left')};
`;

const ModalOpener = styled<any>(ZoomOutSvg)`
  width: 16px;
  height: 16px;
  margin-left: 6px;
  fill: ${({ theme }): string => theme.text.primary};
  vertical-align: middle;
  cursor: pointer;
`;

const Fiat = styled.span`
  color: ${({ theme }): string => theme.text.fiat};
`;

const Hyphen = styled.span`
  color: ${({ theme }): string => theme.text.fiat};
`;
