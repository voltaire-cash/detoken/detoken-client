import { createAction, createReducer } from 'redux-act';
import { createSelector } from 'reselect';
import { Dispatch } from 'redux';
import { batch } from 'react-redux';
import { IFuturesPosition } from './types';
import { getFuturesPositions } from './positionsPageApi';
import { IState } from '~dux/rootDux';
import { sendWSMessageAction } from '~dux/app/appDux';
import {
  getAddressesSelector,
  getPrivateKeysSelector,
  getPublicKeysSelector,
} from '~dux/user/userDux';
import { addSnackbarAction } from '~dux/notifications/notificationsDux';
import { getActivePositions } from '~utils/getActivePositions';
import { IHistoryRes, IListUnspentMessage } from '~dux/wallet/types';
import { transactionsSelector } from '~dux/wallet/walletDux';

export const savePositions = createAction<IFuturesPosition[]>('saveFutures');
export const addPositionAction = createAction<IFuturesPosition>(
  'addPositionAction'
);
export const settleContractsAction = createAction<string[]>(
  'settleContractsAction'
);
const requestFailedAction = createAction<boolean>('requestFailedAction');

const toggleActiveOnlyAction = createAction('toggleActiveOnlyAction');

type IContractAddresses = string[];

const saveContractAddresses = createAction<IContractAddresses>(
  'saveContractAddresses'
);

const loadingAction = createAction('loadingAction');

export interface IPositionsPageState {
  loading: boolean;
  positions: IFuturesPosition[];
  requestFailed: boolean;
  contractAddresses: IContractAddresses;
  activeOnly: boolean;
}

type Action = ReturnType<
  | typeof savePositions
  | typeof loadingAction
  | typeof addSnackbarAction
  | typeof sendWSMessageAction
  | typeof saveContractAddresses
  | typeof requestFailedAction
  | typeof toggleActiveOnlyAction
>;

const duxSelector = (state: IState): IPositionsPageState => state.positionsPage;

export const positionsSelector = createSelector(
  duxSelector,
  ({ positions }): IFuturesPosition[] =>
    positions.sort(
      (a: IFuturesPosition, b: IFuturesPosition) =>
        new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime()
    )
);

export const isActivePositionSelector = createSelector(
  duxSelector,
  ({ positions }): boolean =>
    positions.some((p: IFuturesPosition) => !p.isSettled)
);

export const contractAddressesSelector = createSelector(
  duxSelector,
  ({ contractAddresses }): IContractAddresses => contractAddresses
);

export interface ISettledContractTxs {
  [address: string]: string;
}

export const settledContractTransactionsSelector = createSelector(
  (state: IState) => ({
    transactions: transactionsSelector(state),
    addresses: getAddressesSelector(state),
    contractAddresses: contractAddressesSelector(state),
  }),
  ({ transactions, addresses, contractAddresses }): ISettledContractTxs => {
    if (!transactions) return {};
    const settledContracts = {} as ISettledContractTxs;
    transactions[addresses.activeAddress].forEach((tx) => {
      if (
        contractAddresses.includes(tx.vin[0].address) &&
        tx.vin.length === 1 &&
        tx.vout.length === 2
      ) {
        settledContracts[tx.vin[0].address] = tx.hex;
      }
    });
    return settledContracts;
  }
);

export const updatePositionsFromTransactions = (txs?: IHistoryRes) => (
  dispatch: Dispatch,
  getState: () => IState
): void => {
  const state = getState();
  const { activeAddress } = getAddressesSelector(state);
  const { positions } = state.positionsPage;
  if (!positions.length) {
    return;
  }
  if (!txs && !state.wallet.transactions) {
    return;
  }
  const transactions = (txs || state.wallet.transactions) as IHistoryRes;
  const activeAddressTxs = transactions[activeAddress];
  const newPositions = positions.map((position) => {
    if (position.isSettled) return position;
    const { address } = position;
    let isSettled = false;
    activeAddressTxs.forEach((tx) => {
      if (tx.vin[0].address === address) isSettled = true;
    });
    return Object.assign(position, { isSettled });
  });
  dispatch(savePositions(newPositions));
};

export const getPositions = (height?: number) => async (
  dispatch: Dispatch<Action>,
  getState: () => IState
): Promise<void> => {
  dispatch(loadingAction());
  const state = getState();
  const { activePrivateKey } = getPrivateKeysSelector(state);
  const { activePublicKey } = getPublicKeysSelector(state);
  const blockHeight = height || state.app.oraclePrice.blockHeight;
  if (!blockHeight) {
    return;
  }
  let positions: IFuturesPosition[];
  try {
    positions = await getFuturesPositions({
      activePrivateKey,
      activePublicKey,
    });
  } catch (e) {
    dispatch(requestFailedAction(true));
    return;
  }

  const contractAddresses = positions.map((position) => position.address);

  positions = getActivePositions({
    positions,
    blockHeight,
  });

  batch(() => {
    dispatch(savePositions(positions));
    dispatch(saveContractAddresses(contractAddresses));
  });
  if (state.wallet.transactions) {
    // @ts-expect-error
    updatePositionsFromTransactions()(dispatch, getState);
  }
};

export const updatePositions = (data: IListUnspentMessage) => (
  dispatch: Dispatch,
  getState: () => IState
): void => {
  const { positions } = getState().positionsPage;

  const newPositions = positions.map((position) => {
    let isSettled = true;
    if (data[position.address]?.length) {
      isSettled = false;
    }
    return { ...position, isSettled };
  });
  dispatch(savePositions(newPositions));
};

export const toggleActiveOnly = () => (dispatch: Dispatch<Action>): void => {
  dispatch(toggleActiveOnlyAction());
};

const DEFAULT_STATE: IPositionsPageState = {
  loading: false,
  positions: [],
  contractAddresses: [],
  requestFailed: false,
  activeOnly: true,
};

export default createReducer(
  {
    [savePositions.toString()]: (
      state: IPositionsPageState,
      positions: IFuturesPosition[]
    ) => ({
      ...state,
      positions,
      loading: false,
      requestFailed: false,
    }),
    [loadingAction.toString()]: (state: IPositionsPageState) => ({
      ...state,
      loading: true,
      requestFailed: false,
    }),
    [addPositionAction.toString()]: (
      state: IPositionsPageState,
      position: IFuturesPosition
    ) => ({
      ...state,
      positions: [position, ...state.positions],
      contractAddresses: [...state.contractAddresses, position.address],
    }),
    [saveContractAddresses.toString()]: (
      state: IPositionsPageState,
      contractAddresses: IContractAddresses
    ) => ({
      ...state,
      contractAddresses,
    }),
    [settleContractsAction.toString()]: (
      state: IPositionsPageState,
      settledContracts: string[]
    ) => ({
      ...state,
      positions: state.positions.map((p) =>
        settledContracts.includes(p.address)
          ? Object.assign(p, { isSettled: true })
          : p
      ),
    }),
    [requestFailedAction.toString()]: (
      state: IPositionsPageState,
      status: boolean
    ) => ({
      ...state,
      requestFailed: status,
      loading: false,
    }),
    [toggleActiveOnlyAction.toString()]: (state: IPositionsPageState) => ({
      ...state,
      activeOnly: !state.activeOnly,
    }),
  },
  DEFAULT_STATE
);
