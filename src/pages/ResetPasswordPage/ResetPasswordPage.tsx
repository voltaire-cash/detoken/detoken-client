import React, { useState, useCallback, FC, ReactElement } from 'react';
import { Redirect } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useQuery } from '~hooks/useQuery';
import { IResetPasswordPageProps } from './types';
import styled from '~config/theme';
import { FormLayout, Form, TextField, PageTitle } from '~components';
import FailSvg from '~icons/fail.svg';
import { SeedWords } from './SeedWords';
import { validatePassword } from '~utils/validation';

export const ResetPasswordPage: FC<IResetPasswordPageProps> = ({
  resetPassword,
  error,
  loading,
  isCompleted,
  setError,
}: IResetPasswordPageProps): ReactElement => {
  const { key, field } = error || {};
  const [password, setPassword] = useState('');
  const [showSeedWords, setShowSeedWords] = useState(false);
  const code = useQuery().get('code') || '';
  const email = useQuery().get('email') || '';
  const { t } = useTranslation();

  const errorText = key ? t(key) : '';

  const handlePasswordChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      const { value } = e.currentTarget;
      setPassword(value);
      setError(null);
    },
    [setPassword, setError]
  );

  const handleFormSubmit = useCallback(
    (e: React.FormEvent): void => {
      e.preventDefault();
      const err = validatePassword(password);

      if (!err) {
        setShowSeedWords(true);
      } else {
        setError({ ...err, field: 'passwordHash' });
      }
    },
    [setError, password]
  );

  const prevStep = useCallback(() => setShowSeedWords(false), []);

  const handleSendData = useCallback(
    (words: string): void => {
      resetPassword({ email, password, code, words });
    },
    [email, code, password, resetPassword]
  );

  if (isCompleted) return <Redirect to="/signin" />;

  if (!code || !email)
    return (
      <>
        <PageTitle title={`Detoken: ${t('reset_password_page')}`} />
        <FormLayout logo icon={<StyledFail />}>
          <Error>{t('code_email_is_missing_in_url')}</Error>
        </FormLayout>
      </>
    );

  if (showSeedWords)
    return (
      <>
        <PageTitle title={`Detoken: ${t('reset_password_page')}`} />
        <SeedWords
          error={error}
          onSubmit={handleSendData}
          loading={loading}
          onBackClick={prevStep}
          setError={setError}
        />
      </>
    );

  return (
    <>
      <PageTitle title={`Detoken: ${t('reset_password_page')}`} />
      <FormLayout
        logo
        title={t('reset_password_header')}
        subtitle={t('set_new_password')}
      >
        <Form onSubmit={handleFormSubmit} submitText={t('next_button')}>
          <TextField
            name="password"
            type="password"
            value={password}
            placeholder={t('new_password')}
            onChange={handlePasswordChange}
            error={field === 'passwordHash'}
            helperText={errorText}
          />
        </Form>
      </FormLayout>
    </>
  );
};

const Error = styled.div`
  font-size: calc(4rem / 3);
  font-weight: bold;
  text-align: center;
`;

const StyledFail = styled(FailSvg as 'img')`
  fill: ${({ theme }): string => theme.icons.fail};
`;
