import request from '~providers//helpers';

const { post } = request;

export interface IResetPassword {
  passwordHash: string;
  code: string;
  encryptedMnemonic: string;
  sin: string;
  email: string;
}

export const resetPassword = ({
  passwordHash,
  code,
  encryptedMnemonic,
  sin,
  email,
}: IResetPassword): Promise<void> => {
  const body = {
    newPasswordHash: passwordHash,
    code,
    newEncryptedMnemonic: encryptedMnemonic,
    sin,
    email,
  };
  return post('/password/reset', body);
};
