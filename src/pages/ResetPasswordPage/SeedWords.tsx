import React, { FC, ReactElement, useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { FormLayout, Form, TextField } from '~components';
import { IError } from '~types/Error';

interface IProps {
  error: IError | null;
  onSubmit: (words: string) => void;
  loading: boolean;
  onBackClick: () => void;
  setError: (err: IError | null) => void;
}
export const SeedWords: FC<IProps> = ({
  error,
  onSubmit,
  onBackClick,
  setError,
}: IProps): ReactElement => {
  const [seedWords, setSeedWords] = useState('');
  const { t } = useTranslation();
  const { key, field } = error || {};
  const errorText = key ? t(key) : '';

  const handleWordsChange = useCallback(
    (e: React.ChangeEvent<HTMLTextAreaElement>) => {
      const { value } = e.currentTarget;
      setSeedWords(value);
      setError(null);
    },
    [setError]
  );

  const handleFormSubmit = useCallback(
    (e: React.FormEvent) => {
      e.preventDefault();
      onSubmit(seedWords);
    },
    [onSubmit, seedWords]
  );

  return (
    <FormLayout
      logo
      title={t('reset_password_header')}
      subtitle={t('enter_12_words')}
      onBackClick={onBackClick}
    >
      <Form onSubmit={handleFormSubmit} submitText={t('reset_password_button')}>
        <TextField
          name="seed"
          multiline
          onChange={handleWordsChange}
          value={seedWords}
          error={field === 'words'}
          helperText={field === 'words' ? errorText : ''}
          placeholder={t('seed_words')}
        />
      </Form>
    </FormLayout>
  );
};
