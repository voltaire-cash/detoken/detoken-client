import {
  IResetPasswordPageState,
  IResetPassword,
} from './ResetPasswordPageDux';
import { IError } from '~types/Error';

export interface IMapDispatch {
  resetPassword: ({ words, password, code, email }: IResetPassword) => void;
  setError: (err: IError | null) => void;
}

export type IMapState = IResetPasswordPageState;

export interface IResetPasswordPageProps extends IMapState, IMapDispatch {}
