import React, { useState, useEffect, FC, useCallback } from 'react';
import { useHistory, useLocation, Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import storage from 'local-storage-fallback';
import { Set2FA } from './components/Set2FA';
import { ISigninPageState, ICheck2FAStatus, ILoginUser } from './signinPageDux';
import {
  TextField,
  Form,
  FormLayout,
  PageTitle,
  LinksDot,
  LinksContainer,
} from '~components';
import { STORAGE_KEYS } from '~config/constants';

interface IProps extends ISigninPageState {
  check2FAStatus: ({ email, password }: ICheck2FAStatus) => void;
  loginUser: ({ email, password, code }: ILoginUser) => void;
  isAuthorized: boolean;
  twoFAMode: boolean | null;
  resetErrors: () => void;
  userEmail: string;
  checkJurisdiction: () => void;
}

export const SigninPage: FC<IProps> = ({
  twoFAMode,
  error,
  loginUser,
  isAuthorized,
  resetErrors,
  loading,
  checkJurisdiction,
}: IProps): React.ReactElement => {
  const userEmail = storage.getItem(STORAGE_KEYS.USER_EMAIL) || '';
  const [email, setEmail] = useState<string>(userEmail);
  const [password, setPassword] = useState<string>('');
  const [twoFACode, setTwoFACode] = useState<string>('');
  const history = useHistory();
  const location = useLocation();
  const { t } = useTranslation();

  const { key, field } = error || {};
  const errorText = key ? t(key, { field }) : '';

  useEffect(() => {
    const { state } = location;
    if (isAuthorized) {
      const { from } = (state as any) || { from: { pathname: '/dashboard' } };
      history.push(from);
    }
  }, [isAuthorized, history, location]);

  useEffect(() => {
    checkJurisdiction();
  }, [checkJurisdiction]);

  const handleInputChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>): void => {
      const { name, value } = e.currentTarget;
      resetErrors();
      switch (name) {
        case 'email':
          setEmail(value);
          break;
        case 'password':
          setPassword(value);
          break;
        default:
          break;
      }
    },
    [resetErrors]
  );

  const handle2FAChange = useCallback(
    (value: string): void => {
      setTwoFACode(value);
      resetErrors();
    },
    [resetErrors]
  );
  const handleFormSubmit = useCallback(
    (e: React.FormEvent): void => {
      e.preventDefault();
      loginUser({ email, password, code: twoFACode });
    },
    [email, password, twoFACode, loginUser]
  );

  const sendData = useCallback((): void => {
    loginUser({ email, password, code: twoFACode });
  }, [email, password, twoFACode, loginUser]);

  if (twoFAMode) {
    return (
      <>
        <PageTitle title={`Detoken: ${t('login_2fa_page')}`} />
        <Set2FA
          value={twoFACode}
          onChange={handle2FAChange}
          error={error}
          onSubmit={sendData}
          loading={loading}
        />
      </>
    );
  }

  return (
    <>
      <PageTitle title={`Detoken: ${t('signin_page')}`} />
      <FormLayout
        logo
        title={t('sign_in')}
        subtitle={t('start_trading_instantly')}
      >
        <Form
          onSubmit={handleFormSubmit}
          submitText={t('sign_in_button')}
          submitDisabled={loading}
        >
          <TextField
            name="email"
            type="email"
            value={email}
            placeholder={t('email')}
            onChange={handleInputChange}
            error={field === 'email'}
            helperText={field === 'email' ? errorText : ''}
          />
          <TextField
            name="password"
            type="password"
            value={password}
            placeholder={t('password')}
            onChange={handleInputChange}
            error={field === 'password'}
            helperText={field === 'password' ? errorText : ''}
            inputProps={{ autoFocus: !!email }}
          />
        </Form>
        <LinksContainer>
          <Link to="/forgotpass">{t('forgot_password_question')}</Link>
          <LinksDot />
          <Link to="/signup">{t('dont_have_wallet_question')}</Link>
        </LinksContainer>
      </FormLayout>
    </>
  );
};
