import React, { useState, FC, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { IError } from '~types/Error';
import { FormLayout, Form, TextField } from '~components';

interface IProps {
  error: IError | null;
  onSubmit: (email: string) => void;
  onBackButton: () => void;
}

export const ResetPassword: FC<IProps> = (props: IProps) => {
  const { t } = useTranslation();
  const [email, setEmail] = useState<string>('');
  const { error, onSubmit, onBackButton } = props;

  const { field, key } = error || {};

  const handleFormSubmit = useCallback(
    (e: React.FormEvent) => {
      e.preventDefault();
      onSubmit(email);
    },
    [email, onSubmit]
  );
  const handleInputChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>): void => {
      const { value } = e.currentTarget;
      setEmail(value);
    },
    []
  );
  return (
    <FormLayout
      logo
      title={t('reset_password_header')}
      subtitle={t('reset_password_link_will_be_sent')}
      onBackClick={onBackButton}
    >
      <Form onSubmit={handleFormSubmit} submitText={t('send_reset_link')}>
        <TextField
          name="email"
          type="email"
          value={email}
          onChange={handleInputChange}
          placeholder={t('email')}
          error={field === 'email'}
          helperText={
            field === 'email' && key ? t(key, { field: t(field) }) : ''
          }
        />
      </Form>
    </FormLayout>
  );
};
