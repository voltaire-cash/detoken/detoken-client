import React, { FC, ReactElement, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import styled from '~config/theme';
import { IError } from '~types/Error';
import { FormLayout, InputWrapper, PinCode2FA, Form } from '~components';
import LockSvg from '~icons/lock.svg';

interface IProps {
  error: IError | null;
  value: string;
  onChange: (value: string) => void;
  onSubmit: () => void;
  loading: boolean;
}

export const Set2FA: FC<IProps> = (props: IProps): ReactElement => {
  const { t } = useTranslation();
  const { error, value, onChange, onSubmit, loading } = props;

  const { field, key } = error || {};

  const handleFormSubmit = useCallback(
    (e: React.FormEvent): void => {
      e.preventDefault();
      onSubmit();
    },
    [onSubmit]
  );

  return (
    <FormLayout
      logo
      title={t('two_fa')}
      subtitle={t('enter_your_2fa_code')}
      icon={<LockIcon />}
    >
      <Form
        onSubmit={handleFormSubmit}
        submitText={t('next_button')}
        submitDisabled={loading}
      >
        <InputWrapper
          label={t('two_fa_code')}
          error={field === 'code'}
          helperText={
            field === 'code' && key ? t(key, { field: t(field) }) : ''
          }
        >
          <PinCode2FA
            value={value}
            onChange={onChange}
            onEnterKeyDown={onSubmit}
            error={field === 'code'}
          />
        </InputWrapper>
      </Form>
    </FormLayout>
  );
};

const LockIcon = styled<any>(LockSvg)`
  fill: ${({ theme }): string => theme.logo.secondaryColor};
`;
