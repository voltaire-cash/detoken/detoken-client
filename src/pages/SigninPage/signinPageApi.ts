import request from '~providers/helpers';

const { post } = request;

interface UserData {
  name: string;
  email: string;
  encryptedMnemonic: string;
  sin: string;
  securityFields: { twoFaMode: boolean };
  refCode: string;
}

// ----------------- Get 2FA status------------------
export interface IGet2FAStatus {
  enabled: boolean;
}

export const get2FAStatus = async (
  email: string,
  passwordHash: string
): Promise<IGet2FAStatus> => {
  return post('/2fa/status', {
    email,
    passwordHash,
  });
};

// ----------------- User login------------------
interface LoginData {
  email: string;
  passwordHash: string;
  code?: string;
}

interface ILogin {
  user: UserData;
}

export const login = (data: LoginData): Promise<ILogin> => {
  return post('/auth/login', data);
};

// --------------- Check if prohibited jurisdiction -------------

export const checkJurisdiction = (): Promise<void> => {
  return post('/status');
};
