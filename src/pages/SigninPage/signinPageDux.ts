import { createAction, createReducer } from 'redux-act';
import { Dispatch } from 'redux';
import { batch } from 'react-redux';
import i18next from 'i18next';
import storage from 'local-storage-fallback';
import { mnemonicToSeedSync } from 'bip39';
// @ts-expect-error
import { fromSeed } from 'bitcore-lib-cash/lib/hdprivatekey';
import {
  get2FAStatus,
  login,
  checkJurisdiction as checkJurisdictionRequest,
} from './signinPageApi';
import { getHash } from '~utils/password';
import { saveUserDataAction, setUser2FAModeAction } from '~dux/user/userDux';
import {
  addSnackbarAction,
  removeSnackbarAction,
} from '~dux/notifications/notificationsDux';
import { IState } from '~dux/rootDux';
import { decrypt } from '~utils/wallet';
import { connectWSAction } from '~dux/app/appDux';
import { IError } from '~types/Error';
import { STORAGE_KEYS } from '~config/constants';
import { openModal } from '~dux/modals/modalsDux';

export interface ISigninPageState {
  loading: boolean;
  passwordResetStatus: string;
  error: IError | null;
}

const DEFAULT_STATE = {
  loading: false,
  passwordResetStatus: '',
  error: null,
};

const loginAction = createAction('loginAction');
const setErrorAction = createAction<IError | null>('setErrorAction');
const resetErrorsAction = createAction('resetErrorsAction');
const loadingAction = createAction<boolean>('loadingAction');

type Action = ReturnType<
  | typeof loginAction
  | typeof setErrorAction
  | typeof saveUserDataAction
  | typeof setUser2FAModeAction
  | typeof loadingAction
  | typeof connectWSAction
  | typeof addSnackbarAction
  | typeof removeSnackbarAction
  | typeof openModal
>;

export interface ICheck2FAStatus {
  email: string;
  password: string;
}
export const check2FAStatus = ({ email, password }: ICheck2FAStatus) => async (
  dispatch: Dispatch<Action>
): Promise<void> => {
  dispatch(loadingAction(true));
  try {
    const { enabled } = await get2FAStatus(email, getHash(password));
    dispatch(setUser2FAModeAction(enabled));
    dispatch(loadingAction(false));
  } catch (err) {
    const { field, key } = err;
    if (field === 'activation' && key) {
      dispatch(addSnackbarAction({ message: i18next.t(key) }));
    } else {
      dispatch(setErrorAction({ field, key }));
    }
  }
};

export interface ILoginUser {
  email: string;
  password: string;
  code?: string;
}
export const loginUser = ({ email, password, code }: ILoginUser) => async (
  dispatch: Dispatch<Action>,
  getState: () => IState
): Promise<void> => {
  const { twoFaMode } = getState().user.securityFields;
  if (twoFaMode === null) {
    await check2FAStatus({ email, password })(dispatch);
    const { twoFaMode: newTwoFaMode } = getState().user.securityFields;
    if (newTwoFaMode !== false) return;
  }
  const body = {
    email,
    passwordHash: getHash(password),
    code,
  };
  batch(() => {
    dispatch(removeSnackbarAction(''));
    dispatch(loadingAction(true));
  });
  try {
    const { user } = await login(body);
    const mnemonic = decrypt(user.encryptedMnemonic, password);
    const seedHex = mnemonicToSeedSync(mnemonic).toString('hex');
    const xpriv = fromSeed(seedHex).toString();
    dispatch(saveUserDataAction({ ...user, xpriv }));
  } catch (err) {
    const { field, key } = err;
    dispatch(setErrorAction({ field, key }));
  }

  dispatch(loadingAction(false));
  storage.setItem(STORAGE_KEYS.USER_EMAIL, email);
};

export const resetErrors = () => (dispatch: Dispatch<Action>): void => {
  dispatch(resetErrorsAction());
};

export const checkJurisdiction = () => async (): Promise<void> => {
  try {
    await checkJurisdictionRequest();
    // eslint-disable-next-line no-empty
  } catch (e) {}
};

export default createReducer<ISigninPageState>(
  {
    [setErrorAction.toString()]: (
      state: ISigninPageState,
      error: IError | null
    ) => ({
      ...state,
      error,
      loading: false,
    }),

    [resetErrorsAction.toString()]: (state: ISigninPageState) => ({
      ...state,
      error: DEFAULT_STATE.error,
      loading: false,
    }),

    [loadingAction.toString()]: (
      state: ISigninPageState,
      loading: boolean
    ) => ({
      ...state,
      error: loading ? DEFAULT_STATE.error : state.error,
      loading,
    }),
  },
  DEFAULT_STATE
);
