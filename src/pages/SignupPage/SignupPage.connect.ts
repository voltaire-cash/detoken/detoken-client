import { connect } from 'react-redux';
import { SignupPage } from './SignupPage';
import {
  signup,
  triggerErrorAction,
  signupPageLoadingSelector,
  signupPageErrorsSelector,
  signupPageSuccessSelector,
  ISignupPageState,
} from './signupPageDux';
import { IState } from '~dux/rootDux';
import { openModal } from '~dux/modals/modalsDux';

const mapDispatchToProps = {
  signup,
  triggerErrorAction,
  openModal,
};

const mapStateToProps = (state: IState): ISignupPageState => ({
  isLoading: signupPageLoadingSelector(state),
  success: signupPageSuccessSelector(state),
  error: signupPageErrorsSelector(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(SignupPage);
