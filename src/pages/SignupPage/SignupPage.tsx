import { generateMnemonic } from 'bip39';
import React, {
  FC,
  useState,
  useCallback,
  useEffect,
  ReactElement,
} from 'react';
import { useTranslation } from 'react-i18next';
import { SignupInitialFormStep } from './components/SignupInitialFormStep';
import { SignupImportOrCreateStep } from './components/SignupImportOrCreateStep';
import { STEPS, PREV_STEPS, IRequestData } from './types';
import { SignupCreateAccountStep } from './components/SignupCreateAccountStep';
import { SignupImportAccountStep } from './components/SignupImportAccountStep';
import { Signup2FAOrNotStep } from './components/Signup2FAOrNotStep';
import { Signup2FAStep } from './components/Signup2FAStep';
import { ISignupPageState } from './signupPageDux';
import { IError } from '~types/Error';
import { SignupSeedWordsNoteStep } from './components/SignupSeedWordsNoteStep';
import { SuccessStep } from './components/SuccessStep';
import { SignupTermsStep } from './components/SignupTermsStep';
import { PageTitle } from '~components';
import { OpenModalAction } from '~dux/modals/modalsDux';

interface IProps extends ISignupPageState {
  signup: (props: IRequestData) => void;
  triggerErrorAction: (props: IError | null) => void;
  openModal: (props: OpenModalAction) => void;
}

const DEFAULT_REQUEST_DATA: IRequestData = {
  name: '',
  email: '',
  password: '',
  mnemonic: '',
};

const SignupPage: FC<IProps> = ({
  success,
  isLoading,
  error,
  signup,
  triggerErrorAction,
  openModal,
}: IProps): ReactElement => {
  const [step, setStep] = useState<STEPS>(STEPS.IMPORT_OR_CREATE);
  const [createSeed] = useState(generateMnemonic());
  const [importSeed, setImportSeed] = useState('');
  const [importOrCreate, setImportOrCreate] = useState<'import' | 'create'>(
    'create'
  );
  const [requestData, setRequestData] = useState<IRequestData>(
    DEFAULT_REQUEST_DATA
  );
  const { t } = useTranslation();

  useEffect(() => {
    if (success) {
      setStep(STEPS.SUCCESS);
    }
  }, [success]);

  const handleFormSubmit = useCallback(
    (props: Partial<IRequestData>) => {
      setRequestData({ ...requestData, ...props });
      setStep(
        importOrCreate === 'import' ? STEPS.IMPORT : STEPS.SEED_WORDS_NOTE
      );
    },
    [requestData, importOrCreate]
  );

  const handleImportOrCreate = useCallback((value: 'import' | 'create') => {
    setImportOrCreate(value);
    setStep(STEPS.TERMS);
  }, []);

  const handle2FASubmit = useCallback(
    (props: { twoFaSecret: string; code: string }) => {
      signup({ ...requestData, ...props });
    },
    [requestData, signup]
  );

  const handleWriteMnemonic = useCallback(
    (seed?: string): void => {
      if (seed) {
        setImportSeed(seed);
        setRequestData({ ...requestData, mnemonic: seed });
      } else {
        setRequestData({ ...requestData, mnemonic: createSeed });
      }
      setStep(STEPS.USE_2FA_OR_NOT);
    },
    [requestData, createSeed]
  );

  const handle2FAOrNotSubmit = useCallback(
    (value: STEPS) => {
      if (value === STEPS.INIT_2FA) {
        setStep(STEPS.INIT_2FA);
      } else {
        signup(requestData);
      }
    },
    [signup, requestData]
  );

  const handleTermsSubmit = useCallback(() => {
    setStep(STEPS.SIGNUP_FORM);
  }, []);

  const handleBackButton = useCallback(() => {
    const prevStep = importOrCreate === 'import' ? STEPS.IMPORT : STEPS.CREATE;
    setStep(PREV_STEPS[step] || prevStep);
  }, [importOrCreate, step]);

  return (
    <>
      <PageTitle title={`Detoken: ${t('signup_page')}`} />
      {step === STEPS.IMPORT_OR_CREATE && (
        <SignupImportOrCreateStep onFormSubmit={handleImportOrCreate} />
      )}
      {step === STEPS.TERMS && (
        <SignupTermsStep
          error={error}
          triggerErrorAction={triggerErrorAction}
          onFormSubmit={handleTermsSubmit}
          onBackButton={handleBackButton}
        />
      )}
      {step === STEPS.SIGNUP_FORM && (
        <SignupInitialFormStep
          name={requestData.name}
          email={requestData.email}
          password={requestData.password}
          isLoading={isLoading}
          error={error}
          onFormSubmit={handleFormSubmit}
          onBackButton={handleBackButton}
          triggerErrorAction={triggerErrorAction}
          openModal={openModal}
        />
      )}
      {step === STEPS.SEED_WORDS_NOTE && (
        <SignupSeedWordsNoteStep
          onBackButton={handleBackButton}
          onFormSubmit={setStep}
        />
      )}
      {step === STEPS.CREATE && (
        <SignupCreateAccountStep
          seed={createSeed}
          onBackButton={handleBackButton}
          onFormSubmit={handleWriteMnemonic}
        />
      )}
      {step === STEPS.IMPORT && (
        <SignupImportAccountStep
          seed={importSeed}
          onBackButton={handleBackButton}
          onFormSubmit={handleWriteMnemonic}
          openModal={openModal}
        />
      )}
      {step === STEPS.USE_2FA_OR_NOT && (
        <Signup2FAOrNotStep
          error={error}
          isLoading={isLoading}
          onBackButton={handleBackButton}
          onFormSubmit={handle2FAOrNotSubmit}
        />
      )}
      {step === STEPS.INIT_2FA && requestData.email && (
        <Signup2FAStep
          error={error}
          isLoading={isLoading}
          userEmail={requestData.email}
          onBackButton={handleBackButton}
          onFormSubmit={handle2FASubmit}
          triggerErrorAction={triggerErrorAction}
        />
      )}
      {step === STEPS.SUCCESS && <SuccessStep />}
    </>
  );
};

export { SignupPage };
