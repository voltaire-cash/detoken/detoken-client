import { TOTP, Secret } from 'otpauth';
import React, { FC, useCallback, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { format } from 'date-fns';
import QRCode from 'qrcode.react';
import styled from '~config/theme';
import {
  PinCode2FA,
  FormLayout,
  Form,
  InputWrapper,
  TextField,
} from '~components';
import { IError } from '~types/Error';

interface IProps {
  error: IError | null;
  onFormSubmit: ({
    twoFaSecret,
    code,
  }: {
    twoFaSecret: string;
    code: string;
  }) => void;
  userEmail: string;
  isLoading: boolean;
  onBackButton: () => void;
  triggerErrorAction: (props: IError | null) => void;
}

const Signup2FAStep: FC<IProps> = ({
  error,
  isLoading,
  onBackButton,
  onFormSubmit,
  userEmail,
  triggerErrorAction,
}: IProps): React.ReactElement => {
  const [twoFaSecret] = useState(new Secret({ size: 32 }).b32);
  const [code, setCode] = useState('');
  const { t } = useTranslation();
  const { key, field } = error || {};

  const qrSecret = useMemo(() => {
    const timeLabel = format(new Date(), 'dd-MM-yyyy');
    return new TOTP({
      secret: Secret.fromB32(twoFaSecret),
      algorithm: 'SHA1',
      issuer: 'Detoken',
      label: `${userEmail}-${timeLabel}`,
    });
  }, [twoFaSecret, userEmail]);

  const handleChangeCode = useCallback(
    (value) => {
      setCode(value);
      triggerErrorAction(null);
    },
    [triggerErrorAction]
  );

  const handleFormSubmit = useCallback(
    (e: React.FormEvent) => {
      e.preventDefault();
      onFormSubmit({ twoFaSecret, code });
    },
    [onFormSubmit, twoFaSecret, code]
  );

  const handleEnterKeyDown = useCallback(
    () => onFormSubmit({ twoFaSecret, code }),
    [onFormSubmit, twoFaSecret, code]
  );

  return (
    <FormLayout
      logo
      title={t('enable_2fa')}
      subtitle={t('setup_two_fa_subtitle')}
      onBackClick={onBackButton}
    >
      <StyledQRCode value={qrSecret.toString()} level="H" includeMargin />
      <Form
        onSubmit={handleFormSubmit}
        submitText={t('signup')}
        submitDisabled={isLoading}
      >
        <TextField
          type="text"
          inputProps={{ defaultValue: twoFaSecret, readOnly: true }}
          error={field === 'twoFaSecret'}
          helperText={field === 'twoFaSecret' && key ? t(key) : ''}
          withCopy
          tooltip="secret_copied"
        />

        <InputWrapper
          label={t('two_fa_code')}
          error={field === 'code'}
          helperText={
            field === 'code' && key ? t(key, { field: t(field) }) : ''
          }
        >
          <PinCode2FA
            value={code}
            onChange={handleChangeCode}
            onEnterKeyDown={handleEnterKeyDown}
            error={field === 'code'}
          />
        </InputWrapper>
      </Form>
    </FormLayout>
  );
};

const StyledQRCode = styled(QRCode)`
  margin-top: -7px;
  margin-bottom: 23px;
`;

export { Signup2FAStep };
