import { validateMnemonic } from 'bip39';
import { useTranslation } from 'react-i18next';
import React, { FC, useState, useCallback } from 'react';
import { FormLayout, Form, TextField } from '~components';
import { ERROR_KEYS } from '~types/Error';
import { checkSin } from '~pages/SignupPage/signupPageApi';
import { OpenModalAction } from '~dux/modals/modalsDux';

interface IProps {
  seed?: string;
  onFormSubmit: (seed?: string) => void;
  onBackButton: () => void;
  openModal: (props: OpenModalAction) => void;
}

const SignupImportAccountStep: FC<IProps> = ({
  seed,
  onFormSubmit,
  onBackButton,
  openModal,
}: IProps): React.ReactElement => {
  const [words, setWords] = useState<string>(seed || '');
  const [error, setError] = useState('');
  const [disableSubmit, setDisableSubmit] = useState(false);
  const { t } = useTranslation();

  const handleWordsChange = useCallback(
    (e: React.ChangeEvent<HTMLTextAreaElement>) => {
      const { value } = e.currentTarget;
      setWords(value);
    },
    []
  );

  const handleFormSubmit = useCallback(
    (e: React.FormEvent) => {
      e.preventDefault();
      const phrase = words.replace(/\s+/g, ' ').trim();
      try {
        if (!validateMnemonic(phrase)) {
          throw t(ERROR_KEYS.WRONG_MNEMONIC_PHRASE);
        }
        setDisableSubmit(true);
        checkSin(phrase)
          .then(({ inUse }) => {
            setDisableSubmit(false);
            if (inUse) {
              openModal({ type: 'sinInUse', data: {} });
            } else {
              onFormSubmit(phrase);
            }
          })
          .catch(() => {
            setDisableSubmit(false);
          });
      } catch (err) {
        setError(err);
      }
    },
    [onFormSubmit, words, t, openModal]
  );

  return (
    <FormLayout
      logo
      onBackClick={onBackButton}
      title={t('seed_words_title')}
      subtitle={t('import_seed_subtitle')}
    >
      <Form
        onSubmit={handleFormSubmit}
        submitText={t('next_button')}
        submitDisabled={disableSubmit}
      >
        <TextField
          placeholder={t('seed_words_placeholder')}
          value={words}
          onChange={handleWordsChange}
          error={!!error}
          helperText={error}
          multiline
        />
      </Form>
    </FormLayout>
  );
};

export { SignupImportAccountStep };
