import React, { FC, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import styled from '~config/theme';
import { FormLayout, Button } from '~components';
import WalletSvg from '~icons/wallet.svg';

type StepType = 'import' | 'create';

interface IProps {
  onFormSubmit: (step: StepType) => void;
}

const SignupImportOrCreateStep: FC<IProps> = ({
  onFormSubmit,
}: IProps): React.ReactElement => {
  const { t } = useTranslation();

  const handleCreateNewWallet = useCallback(
    (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
      e.preventDefault();
      onFormSubmit('create');
    },
    [onFormSubmit]
  );

  const handleImportWallet = useCallback(
    (e: React.FormEvent) => {
      e.preventDefault();
      onFormSubmit('import');
    },
    [onFormSubmit]
  );

  return (
    <FormLayout
      logo
      title={t('create_wallet_title')}
      subtitle={t('create_wallet_subtitle')}
      icon={<WalletIcon />}
    >
      <CreateButton onClick={handleCreateNewWallet} fullWidth>
        {t('create_wallet_button')}
      </CreateButton>
      <ImportButton onClick={handleImportWallet} fullWidth variant="link">
        {t('import_wallet_button')}
      </ImportButton>
    </FormLayout>
  );
};

const CreateButton = styled(Button)`
  margin-bottom: 15px;
`;

const WalletIcon = styled<any>(WalletSvg)`
  fill: ${({ theme }): string => theme.logo.secondaryColor};
`;

const ImportButton = styled(Button)`
  height: min-content;
  margin-top: 15px;
`;

export { SignupImportOrCreateStep };
