import React, { FC, useState, useCallback } from 'react';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import styled from '~config/theme';
import { IError } from '~types/Error';
import { IRequestData } from '../types';
import {
  validateName,
  validateEmail,
  validatePassword,
} from '~utils/validation';
import { FormLayout, Form, TextField } from '~components';
import { checkEmail } from '~pages/SignupPage/signupPageApi';
import { OpenModalAction } from '~dux/modals/modalsDux';

interface IProps {
  isLoading: boolean;
  error: IError | null;
  name: string;
  email: string;
  password: string;
  onFormSubmit: (props: Partial<IRequestData>) => void;
  onBackButton: () => void;
  triggerErrorAction: (props: IError | null) => void;
  openModal: (props: OpenModalAction) => void;
}

const SignupInitialFormStep: FC<IProps> = ({
  isLoading,
  name,
  email,
  password,
  error,
  onFormSubmit,
  onBackButton,
  triggerErrorAction,
  openModal,
}: IProps): React.ReactElement => {
  const [formData, setFormData] = useState({
    name,
    email,
    password,
  });
  const [disableSubmit, setDisableSubmit] = useState(false);
  const { t } = useTranslation();
  const { key, field, params } = error || {};

  const handleInputChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>): void => {
      const { name: inputName, value } = e.currentTarget;
      setFormData({
        ...formData,
        [inputName]: value,
      });
      triggerErrorAction(null);
    },
    [formData, triggerErrorAction]
  );

  const handleFormSubmit = useCallback(
    (e: React.FormEvent) => {
      e.preventDefault();
      const nameError = validateName(formData.name);
      const emailError = validateEmail(formData.email);
      const passwordError = validatePassword(formData.password);
      if (nameError) {
        triggerErrorAction({ field: 'name', ...nameError });
        return;
      }
      if (emailError) {
        triggerErrorAction({ field: 'email', ...emailError });
        return;
      }
      if (passwordError) {
        triggerErrorAction({ field: 'passwordHash', ...passwordError });
        return;
      }
      setDisableSubmit(true);
      checkEmail(formData.email)
        .then(({ inUse }) => {
          setDisableSubmit(false);
          if (inUse) {
            openModal({ type: 'emailInUse', data: {} });
          } else {
            onFormSubmit(formData);
          }
        })
        .catch(() => {
          setDisableSubmit(false);
        });
    },
    [onFormSubmit, formData, triggerErrorAction, openModal]
  );

  return (
    <FormLayout
      logo
      onBackClick={onBackButton}
      title={t('create_wallet_title')}
      subtitle={t('create_account_subtitle')}
    >
      <Form
        onSubmit={handleFormSubmit}
        submitText={t('continue_button')}
        submitDisabled={disableSubmit}
      >
        <TextField
          id="name"
          name="name"
          type="text"
          placeholder={t('name')}
          value={formData.name}
          onChange={handleInputChange}
          disabled={isLoading}
          error={field === 'name'}
          helperText={
            key && field === 'name' ? t(key, { field, ...params }) : ''
          }
        />
        <TextField
          id="email"
          name="email"
          type="email"
          placeholder={t('email')}
          value={formData.email}
          onChange={handleInputChange}
          disabled={isLoading}
          error={field === 'email'}
          helperText={
            key && field === 'email' ? t(key, { field, ...params }) : ''
          }
        />
        <TextField
          id="password"
          name="password"
          type="password"
          placeholder={t('password')}
          value={formData.password}
          onChange={handleInputChange}
          disabled={isLoading}
          error={field === 'passwordHash'}
          helperText={
            key && field === 'passwordHash' ? t(key, { field, ...params }) : ''
          }
        />
      </Form>
      <StyledLink to="/signin">{t('already_have_wallet_button')}</StyledLink>
    </FormLayout>
  );
};

const StyledLink = styled(Link)`
  margin-top: 30px;
  font-size: 14px;
`;

export { SignupInitialFormStep };
