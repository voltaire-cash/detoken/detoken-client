import React, { FC, ReactElement, useCallback, useState } from 'react';
import { useTranslation, Trans } from 'react-i18next';
import { CheckBox, Form, FormLayout, InputWrapper } from '~components';
import styled from '~config/theme';
import { ERROR_KEYS, IError } from '~types/Error';
import { DETOKEN_LINK } from '~config/constants';

interface IProps {
  error: IError | null;
  triggerErrorAction: (props: IError | null) => void;
  onFormSubmit: () => void;
  onBackButton: () => void;
}

export const SignupTermsStep: FC<IProps> = ({
  error,
  triggerErrorAction,
  onFormSubmit,
  onBackButton,
}: IProps): ReactElement => {
  const [agreements, setAgreements] = useState({
    age: false,
    privacy: false,
    residence: false,
  });
  const { t } = useTranslation();
  const { field, key } = error || {};

  const handleAgeChange = useCallback(() => {
    setAgreements({ ...agreements, age: !agreements.age });
    triggerErrorAction(null);
  }, [agreements, triggerErrorAction]);
  const handlePrivacyChange = useCallback(() => {
    setAgreements({ ...agreements, privacy: !agreements.privacy });
    triggerErrorAction(null);
  }, [agreements, triggerErrorAction]);
  const handleResidenceChange = useCallback(() => {
    setAgreements({ ...agreements, residence: !agreements.residence });
    triggerErrorAction(null);
  }, [agreements, triggerErrorAction]);

  const handleFormSubmit = useCallback(
    (e: React.FormEvent) => {
      e.preventDefault();
      const [name] =
        Object.entries(agreements).find(([, value]) => !value) || [];
      if (name) {
        triggerErrorAction({
          field: `agreement_${name}`,
          key: ERROR_KEYS.ACCEPT_AGREEMENTS,
        });
      } else {
        onFormSubmit();
      }
    },
    [agreements, onFormSubmit, triggerErrorAction]
  );

  return (
    <FormLayout
      logo
      onBackClick={onBackButton}
      title={t('create_wallet_title')}
      subtitle={t('terms_and_conditions')}
    >
      <Form onSubmit={handleFormSubmit} submitText={t('continue')}>
        <InputWrapper
          error={field === 'agreement_age'}
          iconPos="top"
          helperText={field === 'agreement_age' && key ? t(key) : ''}
        >
          <AgreementContainer>
            <CheckBox
              isChecked={agreements.age}
              handleClick={handleAgeChange}
              error={field === 'agreement_age'}
            />
            <CheckboxLabel onClick={handleAgeChange}>
              {t('i_am_eighteen_years_old')}
            </CheckboxLabel>
          </AgreementContainer>
        </InputWrapper>
        <InputWrapper
          error={field === 'agreement_privacy'}
          iconPos="top"
          helperText={field === 'agreement_privacy' && key ? t(key) : ''}
        >
          <AgreementContainer>
            <CheckBox
              isChecked={agreements.privacy}
              handleClick={handlePrivacyChange}
              error={field === 'agreement_privacy'}
            />
            <CheckboxLabel onClick={handlePrivacyChange}>
              <Trans i18nKey="i_agree_with_the_user_agreement">
                I agree with the
                <a
                  href={`${DETOKEN_LINK}/legal`}
                  target="_blank"
                  rel="noopener noreferrer"
                  tabIndex={-1}
                >
                  user agreement
                </a>
                and
                <a
                  href={`${DETOKEN_LINK}/privacy`}
                  target="_blank"
                  rel="noopener noreferrer"
                  tabIndex={-1}
                >
                  privacy policy
                </a>
              </Trans>
            </CheckboxLabel>
          </AgreementContainer>
        </InputWrapper>
        <InputWrapper
          error={field === 'agreement_residence'}
          iconPos="top"
          helperText={field === 'agreement_residence' && key ? t(key) : ''}
        >
          <AgreementContainer>
            <CheckBox
              isChecked={agreements.residence}
              handleClick={handleResidenceChange}
              error={field === 'agreement_residence'}
            />
            <CheckboxLabel onClick={handleResidenceChange}>
              <Trans i18nKey="not_resident_of_prohibited_jurisdiction">
                I am not a resident of a
                <a
                  href={`${DETOKEN_LINK}/prohibited-jurisdictions`}
                  rel="noopener noreferrer"
                  target="_blank"
                  tabIndex={-1}
                >
                  prohibited jurisdiction
                </a>
              </Trans>
            </CheckboxLabel>
          </AgreementContainer>
        </InputWrapper>
      </Form>
    </FormLayout>
  );
};

const AgreementContainer = styled.div`
  display: flex;

  & > div:nth-of-type(2) {
    margin-left: 10px;
    color: inherit;
    & > a {
      color: inherit;
    }
  }
`;

const CheckboxLabel = styled.span`
  cursor: pointer;
  align-self: center;
`;
