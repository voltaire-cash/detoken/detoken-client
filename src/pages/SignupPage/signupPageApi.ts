import request from '~providers/helpers';
import { IError } from '~types/Error';
import { getSinFromMnemonic } from '~utils/wallet';

export interface ICreateUserProps {
  name: string;
  email: string;
  passwordHash: string;
  encryptedMnemonic: string;
  sin: string;
  twoFaSecret?: string;
  code?: string;
  refId?: string;
}

export const createUser = async (
  props: ICreateUserProps
): Promise<IError | void> => request.post('/users', props);

interface ICheckEmail {
  inUse: boolean;
}
export const checkEmail = async (email: string): Promise<ICheckEmail> =>
  request.get(`/users/checkemail?email=${email}`);

interface ICheckSin {
  inUse: boolean;
}

export const checkSin = async (seeds: string): Promise<ICheckSin> => {
  const sin = getSinFromMnemonic({ mnemonic: seeds });
  return request.get(`/users/checksin?sin=${sin}`);
};
