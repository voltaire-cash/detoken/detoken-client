import { createAction, createReducer } from 'redux-act';
import { createSelector } from 'reselect';
import { Dispatch } from 'redux';
import i18next from 'i18next';
import storage from 'local-storage-fallback';
import { IError, ERROR_KEYS } from '~types/Error';
import { IState } from '~dux/rootDux';
import { ICreateUserProps, createUser } from './signupPageApi';
import { getHash } from '~utils/password';
import { IRequestData } from './types';
import { encrypt, getSinFromMnemonic } from '~utils/wallet';
import { addSnackbarAction } from '~dux/notifications/notificationsDux';

export interface ISignupPageState {
  isLoading: boolean;
  success: boolean;
  error: IError | null;
}

const prepareRequestData = ({
  name,
  email,
  password,
  mnemonic,
  twoFaSecret,
  code,
}: IRequestData): ICreateUserProps => {
  const passwordHash = getHash(password);
  const encryptedMnemonic = encrypt(mnemonic, password);

  const sin = getSinFromMnemonic({
    mnemonic,
    derivation: "m/44'/145'/0'/0/0",
  });
  const refId = storage.getItem('refId') || '';
  return {
    name,
    email,
    passwordHash,
    encryptedMnemonic,
    sin,
    twoFaSecret,
    code,
    refId,
  };
};

const DEFAULT_STATE = {
  isLoading: false,
  error: null,
  success: false,
};

// Selectors
const duxSelector = (state: IState): ISignupPageState => state.signupPage;

export const signupPageLoadingSelector = createSelector(
  duxSelector,
  ({ isLoading }) => isLoading
);

export const signupPageSuccessSelector = createSelector(
  duxSelector,
  ({ success }) => success
);

export const signupPageErrorsSelector = createSelector(
  duxSelector,
  ({ error }) => error
);

// Sync Actions
const loading = createAction('loading');
const success = createAction('success');
const failed = createAction<IError>('failed');
export const triggerErrorAction = createAction<IError | null>(
  'triggerErrorAction'
);

// Async Actions
export const signup = (props: IRequestData) => async (
  dispatch: Dispatch
): Promise<void> => {
  dispatch(loading());
  try {
    const data = prepareRequestData(props);
    await createUser(data);
    dispatch(success());
  } catch (e) {
    const { key, field } = e || {};
    if (e && field === 'sin' && key === ERROR_KEYS.DUPLICATE) {
      dispatch(
        addSnackbarAction({
          message: i18next.t('sin_duplicate'),
          variant: 'error',
        })
      );
    } else {
      dispatch(failed(e));
    }
  }
};

export default createReducer<ISignupPageState>(
  {
    [loading.toString()]: (state: ISignupPageState) => ({
      ...state,
      error: DEFAULT_STATE.error,
      isLoading: true,
    }),

    [success.toString()]: (state: ISignupPageState) => ({
      ...state,
      success: true,
      isLoading: false,
    }),

    [failed.toString()]: (state: ISignupPageState, error: IError) => ({
      ...state,
      error,
      isLoading: false,
    }),

    [triggerErrorAction.toString()]: (
      state: ISignupPageState,
      error: IError | null
    ) => {
      return {
        ...state,
        error,
        isLoading: false,
      };
    },
  },
  DEFAULT_STATE
);
