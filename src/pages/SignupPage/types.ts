export interface IRequestData {
  name: string;
  email: string;
  password: string;
  mnemonic: string;
  twoFaSecret?: string;
  code?: string;
}

export enum STEPS {
  SIGNUP_FORM = 'signupForm',
  IMPORT_OR_CREATE = 'importOrCreateQuestion',
  IMPORT = 'import',
  SEED_WORDS_NOTE = 'seed_words_note',
  CREATE = 'create',
  USE_2FA_OR_NOT = 'use2FAQuestion',
  INIT_2FA = 'init2FA',
  NO_2FA = 'no2FA',
  SUCCESS = 'success',
  TERMS = 'terms',
}

export const PREV_STEPS: { [key: string]: STEPS } = {
  [STEPS.SIGNUP_FORM]: STEPS.TERMS,
  [STEPS.IMPORT]: STEPS.SIGNUP_FORM,
  [STEPS.SEED_WORDS_NOTE]: STEPS.SIGNUP_FORM,
  [STEPS.CREATE]: STEPS.SEED_WORDS_NOTE,
  [STEPS.INIT_2FA]: STEPS.USE_2FA_OR_NOT,
  [STEPS.NO_2FA]: STEPS.USE_2FA_OR_NOT,
  [STEPS.TERMS]: STEPS.IMPORT_OR_CREATE,
};
