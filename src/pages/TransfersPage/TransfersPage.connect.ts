import { connect } from 'react-redux';
import { TransfersPage } from './TransfersPage';
import { IState } from '~dux/rootDux';
import { openModal } from '~dux/modals/modalsDux';
import { IMapState, IMapDispatch } from './types';
import {
  walletDepositsSelector,
  walletWithdrawalsSelector,
} from '~dux/wallet/walletDux';
import { oraclePriceSelector, exchangeRatesSelector } from '~dux/app/appDux';

const mapDispatchToProps: IMapDispatch = {
  openModal,
};

const mapStateToProps = (state: IState): IMapState => ({
  exchangeRates: exchangeRatesSelector(state),
  oraclePrice: oraclePriceSelector(state),
  deposits: walletDepositsSelector(state),
  withdrawals: walletWithdrawalsSelector(state),
  currency: state.app.userCurrency,
});

export default connect(mapStateToProps, mapDispatchToProps)(TransfersPage);
