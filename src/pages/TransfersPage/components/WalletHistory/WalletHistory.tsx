import React, { FC, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import styled from '~config/theme';
import { ITransfersPageProps } from '../../types';
import { OpenModalAction } from '~dux/modals/modalsDux';
import { WalletHistoryColumn } from './components/WalletHistoryColumn';
import { IAppState } from '~dux/app/appDux';

interface IProps {
  currency: IAppState['userCurrency'];
  openModal: (props: OpenModalAction) => void;
  deposits: ITransfersPageProps['deposits'];
  withdrawals: ITransfersPageProps['withdrawals'];
  exchangeRates: ITransfersPageProps['exchangeRates'];
  oraclePrice: ITransfersPageProps['oraclePrice'];
}

export const WalletHistory: FC<IProps> = ({
  currency,
  openModal,
  deposits,
  withdrawals,
  exchangeRates,
  oraclePrice,
}: IProps): React.ReactElement => {
  const { t } = useTranslation();

  const handleDepositClick = useCallback(
    (e: React.MouseEvent<HTMLSpanElement, MouseEvent>) => {
      const { txid, amount, time, address } = e.currentTarget.dataset;
      openModal({
        type: 'depositDetails',
        data: { txid, amount, time, address },
      });
    },
    [openModal]
  );

  const handleWithdrawalClick = useCallback(
    (e: React.MouseEvent<HTMLSpanElement, MouseEvent>) => {
      const { txid, amount, time, address } = e.currentTarget.dataset;
      openModal({
        type: 'withdrawalDetails',
        data: { txid, amount, time, address },
      });
    },
    [openModal]
  );
  return (
    <Container>
      <Wrapper isEmpty={!deposits.length}>
        <Header>{t('received_title')}</Header>
        <WalletHistoryColumn
          currency={currency}
          data={deposits}
          oraclePrice={oraclePrice}
          exchangeRates={exchangeRates}
          onClick={handleDepositClick}
          stringSymbol="+"
        />
      </Wrapper>
      <Wrapper isEmpty={!withdrawals.length}>
        <Header>{t('sent_title')}</Header>
        <WalletHistoryColumn
          currency={currency}
          data={withdrawals}
          oraclePrice={oraclePrice}
          exchangeRates={exchangeRates}
          onClick={handleWithdrawalClick}
          stringSymbol="-"
        />
      </Wrapper>
    </Container>
  );
};

const Container = styled.section`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  margin: 20px 0 0;
  box-sizing: border-box;
  text-align: center;
  line-height: 1.6;
  height: calc(100vh - 105px);
  & > div {
    margin-bottom: 20px;
  }
  ${({ theme }): string => `
    ${theme.mediaTablet} {
    margin: 20px 10px 0;
    height: calc(100vh - 143px);
      padding: 0;
      margin: 20px 15px;
      flex-direction: row;
      & > div {
        margin-bottom: 0;
      }
    }
    ${theme.mediaDesktop}{
      margin: 20px 60px;
    }`};
`;

const Wrapper = styled.div<{ isEmpty?: boolean }>`
  border-bottom: 1px solid ${({ theme }): string => theme.border.section};
  box-sizing: border-box;
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  height: calc(50% - 20px);
  ${({ theme }): string => `${theme.mediaTablet} {
    background: ${theme.background.primary};
    border: 1px solid ${theme.border.default};
    border-radius: 4px;
    height: auto;      
    max-width: calc(50% - 9px);
    &:first-of-type {
        margin-right: 9px;
    }
    &:last-child {
        margin-left: 9px;
    }
  }`};
`;

const Header = styled.div`
  text-transform: uppercase;
  padding: 0 20px;
  font-size: 1rem;
  line-height: 21px;
  font-weight: bold;
  text-align: left;
  border-bottom: 1px solid ${({ theme }): string => theme.border.section};
  color: ${({ theme }): string => theme.text.header};
  display: flex;
  align-items: center;
  height: 54px;
  min-height: 54px;
  ${({ theme }): string => `
    ${theme.mediaTablet}{
      border-bottom: 1px solid ${theme.border.default};
    }
  `}
`;
