import Big from 'big.js';
import { ITransaction } from '~dux/wallet/types';
import { OpenModalAction } from '~dux/modals/modalsDux';
import { IAppState, IExchangeRates } from '~dux/app/appDux';

export interface IMapState {
  exchangeRates: IExchangeRates;
  oraclePrice: Big;
  deposits: ITransaction[];
  withdrawals: ITransaction[];
  currency: IAppState['userCurrency'];
}

export interface IMapDispatch {
  openModal: (props: OpenModalAction) => void;
}

export interface ITransfersPageProps extends IMapState, IMapDispatch {}
