import i18next from 'i18next';
import axios, { AxiosResponse, AxiosError } from 'axios';
import { IError, ERROR_KEYS } from '~types/Error';
import { connections } from '~config/constants';
import store from '../config/store';
import { addSnackbarAction } from '~dux/notifications/notificationsDux';
import { openModal } from '~dux/modals/modalsDux';

const request = axios.create({
  baseURL: connections.api,
  withCredentials: true,
});

const successHandler = ({ data }: AxiosResponse<any>): any => data;

const errorHandler = (err: AxiosError<{ error: IError }>): Promise<IError> => {
  // If we have response it's our error with IError interface
  if (
    err.config.url === '/status' &&
    err.config.method === 'post' &&
    !err.response
  ) {
    // assume this is response from AWS WAF
    store.dispatch<any>(openModal({ type: 'jurisdiction', data: {} }));
  }
  if (err.response) {
    const { data, config, status } = err.response;
    // probably no reason in this validation
    if (status === 403 && config.url === '/status') {
      store.dispatch<any>(openModal({ type: 'jurisdiction', data: {} }));
      return Promise.reject(err);
    }
    if (status === 503 && config.url === '/futures/v1-BCH-1d/entry-data') {
      return Promise.reject(err);
    }
    // If it's main error do common notification and return promise with IError
    if (!data.error.field) {
      const message = i18next.t(data.error.key, { ...data.error.params });
      store.dispatch<any>(addSnackbarAction({ message, variant: 'error' }));
    }
    return Promise.reject(data.error);
  }

  // If we have no response it's some common axios error.
  // Log it and reject promise with common error.
  console.error(err);
  // eslint-disable-next-line prefer-promise-reject-errors
  return Promise.reject({ key: ERROR_KEYS.COMMON });
};

request.interceptors.response.use(successHandler, errorHandler);

export default request;
