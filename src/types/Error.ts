export enum ERROR_KEYS {
  COMMON = 'e.something_wrong',
  RATE_LIMIT = 'e.rate_limit',
  FORBIDDEN = 'e.forbidden',
  NOT_FOUND = 'e.not_found',
  UNAUTHORIZED = 'e.unauthorized',
  BAD_REQUEST = 'e.bad_request',
  ALREADY_EXISTS = 'e.already_exists',
  INVALID_USER_NAME = 'e.validation_user_name',
  INVALID_EMAIL = 'e.validation_email',
  INVALID_PASSWORD = 'e.validation_password',
  INVALID_2FA_CODE = 'e.validation_2fa_code',
  INVALID_2FA_SECRET = 'e.validation_2fa_secret',
  INVALID_2FA_LOCAL_TIME = 'e.validation_2fa_local_time',
  INVALID_SIN = 'e.validation_sin',
  INVALID_VALUE = 'e.invalid_value',
  TOO_SHORT = 'e.too_short',
  TOO_LONG = 'e.too_long',
  LOWERCASE_MISSING = 'e.lowercase_missing',
  UPPERCASE_MISSING = 'e.uppercase_missing',
  NUMBER_MISSING = 'e.number_missing',
  MISSING_SPECIAL_CHAR = 'e.missing_special_char',
  DUPLICATE = 'e.duplicate',
  ACCEPT_AGREEMENTS = 'e.accept_agreements',
  REQUIRED = 'e.required',
  // ---------------- Local errors -------------
  WRONG_MNEMONIC_PHRASE = 'invalid_seed_phrase',
  PASSWORDS_MISMATCH = 'passwords_mismatch',
  WRONG_ADDRESS = 'wrong_address',
  INSUFFICIENT_FUNDS = 'insufficient_funds',
  AMOUNT_BELOW_MINIMUM = 'amount_below_minimum',
  AMOUNT_GREATER_MAXIMUM = 'amount_greater_maximum',
  CONTRACT_AMOUNT_BELOW_MINIMUM = 'contract_amount_below_minimum',
  CONTRACT_AMOUNT_GREATER_MAXIMUM = 'contract_amount_greater_maximum',
  EMPTY_VALUE = 'value_is_empty',
  PAGE_NOT_FOUND = 'page_not_found',
  INVALID_CONTRACT_INPUTS = 'invalid_contract_inputs',
}

interface IErrorParams {
  [paramKey: string]: null | boolean | string | number | object;
}
export interface IError {
  key: ERROR_KEYS;
  field?: string;
  params?: IErrorParams;
}

export interface IWSError {
  type: 'error';
  event: string;
  key: ERROR_KEYS;
  field?: string;
  params?: IErrorParams;
}
