declare module 'prevent-scroll' {
  // eslint-disable-next-line no-inner-declarations
  export function on(): void;

  // eslint-disable-next-line no-inner-declarations
  export function off(): void;
}
