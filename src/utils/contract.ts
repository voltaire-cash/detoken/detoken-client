import Big from 'big.js';
import { AnyHedgeManager } from '@generalprotocols/anyhedge';
import {
  POSITION,
  SATOSHIS_IN_BCH,
  BLOCKS_IN_DAY,
  BCH_DP,
  DUST_SATOSHIS,
} from '~config/constants';
import { formatFiat, blocksToDuration, IDuration } from '~utils/formatters';
import { IFuturesPosition } from '~pages/PositionsPage/types';
import {
  IContractMetadata,
  IContractParameters,
  ISimulationOutput,
  IParsedSettlementData,
} from '~types/anyHedge';

// export interface IContractData {
//   feeBCH: string;
//   totalCost: string;
//   startValue: string;
//   currentValue: string;
//   premiumBCH: string;
//   startHedgeVal: string;
//   startLongVal: string;
//   lowLiquidationPrice: string;
//   highLiquidationPrice: string;
// }

export interface IContractData {
  position: POSITION;
  startValueBCH: string;
  startValueUSD: string;
  currentValueBCH: string;
  currentValueUSD: string;
  premium: number;
  premiumBCH: string;
  premiumUSD: string;
  isPremiumPositive: boolean;
  dayDuration: string;
  expiration: IDuration;
  startPrice: string;
  lowLiquidationPrice: string;
  highLiquidationPrice: string;
  margin: string;
  startBlockHeight: number;
  maturityModifier: number;
  createdAt: Date;
  expiresAt: Date;
  fee: string;
  feeBCH: string;
  feeUSD: string;
  startHedgeValBCH: string;
  startHedgeValUSD: string;
  startLongValBCH: string;
  startLongValUSD: string;
  oraclePubKey: string;
  isSettled: boolean;
  totalCostBCH: string;
  totalCostUSD: string;
  contractAddress: string;
  blockDuration: number;
  profitBCH: string;
  settleValueBCH: string;
  settlePriceUSD: string;
  settleReason: string;
  settleValueUSD: string;
  leftBlocks: number;
}

const anyHedgeManager = new AnyHedgeManager();

const TEN_MINUTES = 10 * 60 * 1000;
const CENTS_IN_USD = 100;

// interface IProps {
//   fee: string;
//   position: POSITION;
//   premium: number;
//   parameters: {};
//   hedgeInputSats: number;
//   longInputSats: number;
//   totalInputSats: number;
//   minerCost: number;
//   dustCost: number;
//   startPrice: number;
//   oraclePrice: string;
//   lowLiquidationMulti: string;
//   highLiquidationMulti: string;
// }

interface IProps {
  metadata: IContractMetadata;
  parameters: IContractParameters;
  simulation: ISimulationOutput;
  contract: IFuturesPosition;
  oraclePrice: string;
  blockHeight: number;
  settlement: IParsedSettlementData | {};
}

interface ICalculatePremium {
  position: POSITION;
  longInputSats: number | string;
  hedgeInputSats: number | string;
  premium: number;
}

interface IExistingContractProps {
  data: IFuturesPosition;
  oraclePrice: string;
  settlementTransaction: string;
}

interface IExistingContractOutput {
  metadata: IContractMetadata;
  simulation: ISimulationOutput;
  parameters: IContractParameters;
  takerSide: POSITION;
  settlement: IParsedSettlementData | {};
}

export const getMargin = (lowLiquidationMulti: string): string => {
  const down = Big(1).minus(lowLiquidationMulti);
  return Big(1).div(down).minus(1).toString();
};

export const calculateContractValues = ({
  metadata,
  // parameters,
  simulation,
  contract,
  oraclePrice,
  blockHeight,
  settlement,
}: IProps): IContractData => {
  const {
    takerSide: position,
    startPrice,
    premium,
    maturityModifier,
    startBlockHeight,
    lowLiquidationMulti,
    highLiquidationMulti,
    fee,
    oraclePubKey,
    isSettled,
    address,
  } = contract;

  const { HEDGE } = POSITION;

  const startPriceUSD = Big(startPrice).div(CENTS_IN_USD);

  const startValueBCH = Big(
    position === HEDGE ? metadata.hedgeInputSats : metadata.longInputSats
  ).div(SATOSHIS_IN_BCH);

  const startValueUSD = formatFiat(
    Big(position === HEDGE ? metadata.hedgeUnits : metadata.longInputUnits)
      .div(CENTS_IN_USD)
      .toFixed(2),
    { zeroFraction: true }
  );

  const currentValueBCH = Big(
    position === HEDGE ? simulation.hedgePayoutSats : simulation.longPayoutSats
  ).div(SATOSHIS_IN_BCH);

  const currentValueUSD = formatFiat(
    currentValueBCH.mul(oraclePrice).toFixed(2),
    { zeroFraction: true }
  );

  let premiumBCH;
  const premiumSatoshis = Big(premium)
    .abs()
    .mul(position === HEDGE ? metadata.longInputSats : metadata.hedgeInputSats)
    .round(0, 3);

  if (premiumSatoshis.lt(DUST_SATOSHIS)) {
    premiumBCH = Big(0);
  } else {
    premiumBCH = premiumSatoshis.div(SATOSHIS_IN_BCH);
  }

  const premiumUSD = premiumBCH.mul(startPriceUSD).toFixed(2);

  const isPremiumPositive = Big(premium).lt(0);

  const dayDuration = Big(maturityModifier).div(BLOCKS_IN_DAY).toString();

  const leftBlocks = startBlockHeight + maturityModifier - blockHeight;
  const expiration = blocksToDuration(leftBlocks);

  const lowLiquidationPrice = formatFiat(
    Big(startPriceUSD).mul(lowLiquidationMulti).toFixed(2),
    { zeroFraction: true }
  );

  const highLiquidationPrice = formatFiat(
    Big(startPriceUSD).mul(highLiquidationMulti).toFixed(2),
    { zeroFraction: true }
  );

  const multiplicator = getMargin(lowLiquidationMulti);
  const margin = `${multiplicator}:1`;

  const expirationInMs =
    new Date(contract.createdAt).getTime() + maturityModifier * TEN_MINUTES;

  const feeBCH = Big(fee)
    .div(100)
    .mul(position === HEDGE ? metadata.hedgeInputSats : metadata.longInputSats)
    .div(SATOSHIS_IN_BCH);
  const feeUSD = formatFiat(feeBCH.mul(startPriceUSD).toFixed(2), {
    zeroFraction: true,
  });

  const startHedgeValBCH = Big(metadata.hedgeInputSats).div(SATOSHIS_IN_BCH);

  const startHedgeValUSD = formatFiat(
    startHedgeValBCH.mul(startPriceUSD).toFixed(2),
    { zeroFraction: true }
  );

  const startLongValBCH = Big(metadata.longInputSats).div(SATOSHIS_IN_BCH);
  const startLongValUSD = formatFiat(
    startLongValBCH.mul(startPriceUSD).toFixed(2),
    { zeroFraction: true }
  );

  const extraCostBCH = Big(metadata.dustCost)
    .plus(metadata.minerCost || 0)
    .div(SATOSHIS_IN_BCH)
    .plus(isPremiumPositive ? '0' : premiumBCH);

  const totalCostBCH = Big(
    position === HEDGE ? metadata.hedgeInputSats : metadata.longInputSats
  )
    .div(SATOSHIS_IN_BCH)
    .plus(feeBCH)
    .plus(extraCostBCH);

  const totalCostUSD = formatFiat(totalCostBCH.mul(startPriceUSD).toFixed(2), {
    zeroFraction: true,
  });

  const {
    // @ts-expect-error
    hedgeSatoshis: settledHedgeSats,
    // @ts-expect-error
    longSatoshis: settledLongSats,
  } = settlement;

  const settleValueBCH = Big(
    position === HEDGE ? settledHedgeSats || 0 : settledLongSats || 0
  ).div(SATOSHIS_IN_BCH);

  // @ts-expect-error
  const settlePriceUSD = Big(settlement.oraclePrice || '0')
    .div(CENTS_IN_USD)
    .toString();

  const settleValueUSD = settleValueBCH.mul(settlePriceUSD).toFixed(2);

  // @ts-expect-error
  const settleReason = settlement.settlementType || '';

  const profitBCH = isSettled
    ? settleValueBCH.minus(startValueBCH)
    : currentValueBCH.minus(startValueBCH);

  return {
    position,
    startValueBCH: startValueBCH.toFixed(BCH_DP),
    startValueUSD,
    currentValueBCH: currentValueBCH.toFixed(BCH_DP),
    currentValueUSD,
    premium,
    premiumBCH: premiumBCH.toFixed(BCH_DP),
    premiumUSD,
    isPremiumPositive,
    dayDuration,
    expiration,
    startPrice: Big(startPriceUSD).toFixed(2),
    lowLiquidationPrice,
    highLiquidationPrice,
    margin,
    startBlockHeight,
    maturityModifier,
    createdAt: new Date(contract.createdAt),
    expiresAt: new Date(expirationInMs),
    fee,
    feeBCH: feeBCH.toFixed(BCH_DP),
    feeUSD,
    startHedgeValBCH: startHedgeValBCH.toFixed(BCH_DP),
    startHedgeValUSD,
    startLongValBCH: startLongValBCH.toFixed(BCH_DP),
    startLongValUSD,
    oraclePubKey,
    isSettled,
    totalCostBCH: totalCostBCH.toFixed(BCH_DP),
    totalCostUSD,
    contractAddress: address,
    blockDuration: maturityModifier,
    profitBCH: profitBCH.toFixed(BCH_DP),
    settleValueBCH: settleValueBCH.toFixed(BCH_DP),
    settlePriceUSD,
    settleReason,
    settleValueUSD,
    leftBlocks,
  };
};

export const getExistingContractParams = async ({
  data,
  oraclePrice,
  settlementTransaction,
}: IExistingContractProps): Promise<IExistingContractOutput> => {
  const {
    oraclePubKey,
    hedgeUnits,
    startPrice,
    startBlockHeight,
    maturityModifier,
    highLiquidationMulti,
    lowLiquidationMulti,
  } = data;

  const { parameters, metadata } = await anyHedgeManager.createContract(
    oraclePubKey,
    '', // hedgePublicKey,
    '', // longPublicKey  ,
    hedgeUnits,
    startPrice,
    startBlockHeight,
    0, // earliestLiquidationModifier
    maturityModifier,
    Number(highLiquidationMulti),
    Number(lowLiquidationMulti)
  );

  const simulation = await anyHedgeManager.calculateSettlementOutcome(
    parameters,
    metadata.totalInputSats + metadata.minerCost! + metadata.dustCost,
    Math.floor(Number(oraclePrice) * 100)
  );

  let settlement = {};
  if (settlementTransaction) {
    try {
      settlement = (
        await anyHedgeManager.parseSettlementTransaction(settlementTransaction)
      ).settlement;
    } catch (e) {
      console.log(e);
    }
  }

  return {
    parameters,
    metadata,
    simulation,
    takerSide: data.takerSide,
    settlement,
  };
};

export const calculatePremium = ({
  position,
  longInputSats,
  hedgeInputSats,
  premium,
}: ICalculatePremium): number => {
  const makerInputSats =
    position === POSITION.HEDGE ? longInputSats : hedgeInputSats;
  // The premium is always paid on the amount of the maker.
  return Number(Big(premium).mul(makerInputSats).round(0, 3));
};

// All inputs for taker side
// export const calculateContractInfo = ({
//   fee,
//   position,
//   premium,
//   parameters,
//   hedgeInputSats,
//   longInputSats,
//   totalInputSats,
//   minerCost,
//   dustCost,
//   startPrice,
//   oraclePrice,
//   lowLiquidationMulti,
//   highLiquidationMulti,
// }: IProps): IContractData => {
//   const { HEDGE } = POSITION;
//   const inputSats = position === HEDGE ? hedgeInputSats : longInputSats;
//   const startPriceUSD = Big(startPrice).div(CENTS_IN_USD);
//   const startValue = Big(inputSats).div(SATOSHIS_IN_BCH).toString();
//   // positive indicates long pays hedge
//   const isPremiumPositive =
//     position === HEDGE ? Big(premium).gte(0) : Big(premium).lte(0);
//   // The premium is always paid on the amount of the maker.
//   const premiumSat = calculatePremium({
//     position,
//     longInputSats,
//     hedgeInputSats,
//     premium,
//   });
//   const premiumBCH = Big(premiumSat).div(SATOSHIS_IN_BCH).toString();
//
//   const totalSats = Big(totalInputSats)
//     .plus(minerCost)
//     .plus(dustCost)
//     .plus(isPremiumPositive ? '0' : premiumBCH)
//     .toString();
//
//   const { hedgePayoutSats, longPayoutSats } = anyHedgeManager.simulate(
//     parameters,
//     totalSats,
//     oraclePrice
//   );
//
//   const payoutSats = position === HEDGE ? hedgePayoutSats : longPayoutSats;
//   const currentValue = Big(payoutSats).div(SATOSHIS_IN_BCH).toString();
//   const startHedgeVal = Big(hedgeInputSats).div(SATOSHIS_IN_BCH).toString();
//   const startLongVal = Big(longInputSats).div(SATOSHIS_IN_BCH).toString();
//
//   const lowLiquidationPrice = formatFiat(
//     startPriceUSD.mul(lowLiquidationMulti).toFixed(2),
//     { zeroFraction: true }
//   );
//
//   const highLiquidationPrice = formatFiat(
//     startPriceUSD.mul(highLiquidationMulti).toFixed(2),
//     { zeroFraction: true }
//   );
//
//   const feeBCH = Big(fee)
//     .div(100)
//     .mul(inputSats)
//     .div(SATOSHIS_IN_BCH)
//     .toString();
//
//   const totalCost = Big(inputSats)
//     .div(SATOSHIS_IN_BCH)
//     .plus(feeBCH)
//     .plus(isPremiumPositive ? '0' : premiumBCH)
//     .toString();
//
//   return {
//     feeBCH,
//     totalCost,
//     startValue,
//     currentValue,
//     premiumBCH,
//     startHedgeVal,
//     startLongVal,
//     lowLiquidationPrice,
//     highLiquidationPrice,
//   };
// };
