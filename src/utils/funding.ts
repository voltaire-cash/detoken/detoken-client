// @ts-expect-error
import { Transaction, crypto } from 'bitcore-lib-cash';
import Big from 'big.js';
import { IUTXO, IUTXOs } from '~dux/wallet/types';
import { ERROR_KEYS } from '~types/Error';
import { AH_FEE_DIVISOR, MIN_WITHDRAWAL } from '~config/constants';
import { ISignedTransaction } from '~components/Modals/WithdrawModal/withdrawModalApi';

const {
  SIGHASH_ALL: ALL,
  SIGHASH_ANYONECANPAY: ANYONECANPAY,
  SIGHASH_FORKID: FORKID,
} = crypto.Signature;

// eslint-disable-next-line no-bitwise
const SIGHASH_FUNDING = ALL | ANYONECANPAY | FORKID;

export const createPartialFundingTransaction = ({
  takerInputSatoshis,
  makerInputSatoshis,
  premiumSatoshis,
  dustCost,
  minerCost,
  contractAddress,
  changeAddress,
  productFee,
  anyHedgeFeeAddress,
  UTXOs,
  feeChargedSatoshis,
}: any) => {
  //-----------
  if (!contractAddress) {
    throw new Error(`Contract address is empty: ${contractAddress}`);
  }
  if (!anyHedgeFeeAddress) {
    throw new Error(`Fee address is empty: ${anyHedgeFeeAddress}`);
  }
  if (!changeAddress) {
    throw new Error(`Change address is empty: ${changeAddress}`);
  }
  //-----------
  const transaction = new Transaction();

  const usedUTXOs = [];

  // Calculate the total cost of the contract for the user
  let totalTakerInputSatoshis =
    takerInputSatoshis + feeChargedSatoshis + dustCost + minerCost;

  // premium here is 0 in case it's in range (-546, 546)
  // premium < 0 means maker pays premium
  // premium > 0 means taker pays premium
  totalTakerInputSatoshis += premiumSatoshis;

  // Calculate the total to pay to the contract
  const contractOutputSatoshis =
    takerInputSatoshis + makerInputSatoshis + dustCost + minerCost;

  // Calculate the total to pay AnyHedge
  // (takerInputSats + makerInputSats) * FEE_PERCENTAGE_AS_DECIMAL / 4
  const anyHedgeOutputSatoshis = Number(
    Big(takerInputSatoshis + makerInputSatoshis)
      .times(productFee)
      .div(100)
      .div(AH_FEE_DIVISOR)
      .round(0, 3)
      .toString()
  );

  // Add the contract output
  transaction.to(contractAddress, contractOutputSatoshis);

  // Add the AnyHedge output
  transaction.to(anyHedgeFeeAddress, anyHedgeOutputSatoshis);

  // Recursively add UTXOs until input satoshis >= totalTakerInputSatoshis
  // Do not implement any network fee logic, this is done by the server
  const unspentOutputs: Array<IUTXO> = Object.values(UTXOs || {});
  while (transaction.inputAmount < totalTakerInputSatoshis) {
    if (!unspentOutputs.length) throw new Error(ERROR_KEYS.INSUFFICIENT_FUNDS);
    transaction.from(unspentOutputs[unspentOutputs.length - 1]);
    const utxo = unspentOutputs.pop();
    usedUTXOs.push(`${utxo!.txid}:${utxo!.outputIndex}`);
  }

  const extraSatoshis = transaction.inputAmount - totalTakerInputSatoshis;

  // if inputs bigger than 546 sats and than needed send them back to user
  if (extraSatoshis >= MIN_WITHDRAWAL) {
    transaction.to(changeAddress, extraSatoshis);
  }

  return { transaction, usedUTXOs };
};

interface ISignTransaction {
  transaction: any;
  UTXOs: IUTXOs;
  addressToKey: {
    [address: string]: any;
  };
}

export const signPartialFundingTransaction = ({
  transaction,
  UTXOs,
  addressToKey,
}: ISignTransaction): ISignedTransaction => {
  // Sign each input (SIGHASH_ALL || SIGHASH_ANYONECANPAY )
  // @ts-expect-error
  transaction.inputs.forEach((input, i) => {
    const { prevTxId: txId, outputIndex } = input.toJSON();
    const { address } =
      Object.values(UTXOs as IUTXOs).find(
        (utxo: IUTXO) => utxo.txid === txId && utxo.outputIndex === outputIndex
      ) || {};
    // this shouldn't happen
    if (!address) {
      console.log(`Missed address for ${txId}`);
      return;
    }
    const signature = input.getSignatures(
      transaction,
      addressToKey[address],
      i,
      SIGHASH_FUNDING
    );
    input.addSignature(transaction, signature[0]);
  });

  // You may have to disable some serialisation checks as the transaction will not be fully signed nor have enough input satoshis to cover the output + network fee
  return transaction.toJSON();
};
