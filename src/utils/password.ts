import SHA256 from 'crypto-js/sha256';

export const getHash = (password: string): string =>
  SHA256(password).toString();
