import i18n from 'i18next';

export const getTranslationKey = (
  num: number | string,
  str: string
): string => {
  const rule = new Intl.PluralRules(i18n.language).select(Number(num));
  return `${str}_${rule}`;
};

export const getDurationTranslation = (duration: [number, number]): string => {
  const [days, hours] = duration;
  let translation = '';
  if (days) {
    translation = `${days} ${i18n.t(getTranslationKey(days, 'day'))} `;
  }
  translation += `${hours} ${i18n.t(getTranslationKey(hours, 'hour'))}`;

  return translation;
};

const addZero = (value: number): string =>
  value <= 9 ? `0${value}` : `${value}`;

// output format is dd MMM, yyyy HH:mm
export const formatDateWithTranslation = (
  val: string | Date | number
): string => {
  if (!val) return '';
  const date = new Date(val);
  const dd = addZero(date.getDate());
  const mmm = new Intl.DateTimeFormat(i18n.language, { month: 'short' })
    .format(date)
    .replace('.', '');
  const MMM = mmm[0].toUpperCase() + mmm.slice(1);
  const yyyy = date.getFullYear();
  const mm = addZero(date.getMinutes());
  const hh = addZero(date.getHours());

  return `${dd} ${MMM}, ${yyyy} ${hh}:${mm}`;
};
