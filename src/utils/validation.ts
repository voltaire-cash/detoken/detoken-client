import isAlpha from 'validator/lib/isAlpha';
import isEmail from 'validator/lib/isEmail';
import { ERROR_KEYS, IError } from '~types/Error';

const MIN_NAME_LENGTH = 3;
const MAX_NAME_LENGTH = 255;
const MIN_EMAIL_LENGTH = 3;
const MAX_EMAIL_LENGTH = 255;
const LOWER_CASE = new Set('abcdefghijklmnopqrstuvwxyz'.split(''));
const UPPER_CASE = new Set('ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split(''));
const DIGITS = new Set('0123456789'.split(''));
const MIN_PASSWORD_LENGTH = 6;
const SPECIAL_CHARACTERS = new Set([
  ' ',
  '!',
  '"',
  '#',
  '$',
  '%',
  '&',
  "'",
  '(',
  ')',
  '*',
  '+',
  ',',
  '-',
  '.',
  '/',
  ':',
  ';',
  '<',
  '=',
  '>',
  '?',
  '@',
  '[',
  '\\',
  ']',
  '^',
  '_',
  '`',
  '{',
  '|',
  '}',
  '~',
]);

const bchAddrRegex = /^(?:bitcoincash:)?(q|p)[a-z0-9]{41}/;

const validateName = (name: any): IError | void => {
  if (typeof name !== 'string') {
    return {
      key: ERROR_KEYS.INVALID_VALUE,
      params: { allowed: 'string' },
    };
  }
  const lengthField = name.length;
  if (lengthField < MIN_NAME_LENGTH) {
    return {
      key: ERROR_KEYS.TOO_SHORT,
      params: { length: MIN_NAME_LENGTH },
    };
  }
  if (lengthField > MAX_NAME_LENGTH) {
    return {
      key: ERROR_KEYS.TOO_LONG,
      params: { length: MAX_NAME_LENGTH },
    };
  }
  const nameArray = name.trim().split(' ');
  // eslint-disable-next-line no-restricted-syntax
  for (const n of nameArray) {
    if (!isAlpha(n)) return { key: ERROR_KEYS.INVALID_USER_NAME };
  }
};

const validateEmail = (email: any): IError | void => {
  if (typeof email !== 'string') {
    return {
      key: ERROR_KEYS.INVALID_VALUE,
      params: { allowed: 'string' },
    };
  }
  const lengthField = email.length;
  if (lengthField < MIN_EMAIL_LENGTH) {
    return {
      key: ERROR_KEYS.TOO_SHORT,
      params: { length: MIN_EMAIL_LENGTH },
    };
  }

  if (lengthField >= MAX_EMAIL_LENGTH) {
    return {
      key: ERROR_KEYS.TOO_LONG,
      params: { length: MAX_EMAIL_LENGTH },
    };
  }

  if (!isEmail(email)) {
    return { key: ERROR_KEYS.INVALID_EMAIL };
  }
};

const validatePassword = (password: any): IError | void => {
  if (typeof password !== 'string') {
    return {
      key: ERROR_KEYS.INVALID_VALUE,
      params: { allowed: 'string' },
    };
  }
  let hasLowerCase = false;
  let hasUpperCase = false;
  let hasDigits = false;
  let hasSpecialCharacters = false;
  const len = password.length;

  // lower length rule
  if (len < MIN_PASSWORD_LENGTH) {
    return {
      key: ERROR_KEYS.TOO_SHORT,
      params: { length: MIN_PASSWORD_LENGTH },
    };
  }
  // lowerCase upperCase number and specialChar
  // eslint-disable-next-line no-restricted-syntax
  for (const c of password) {
    if (hasLowerCase && hasUpperCase && hasDigits && hasSpecialCharacters)
      break;
    if (LOWER_CASE.has(c)) {
      hasLowerCase = true;
    } else if (UPPER_CASE.has(c)) {
      hasUpperCase = true;
    } else if (DIGITS.has(c)) {
      hasDigits = true;
    } else if (SPECIAL_CHARACTERS.has(c)) {
      hasSpecialCharacters = true;
    }
  }

  if (!hasLowerCase) {
    return { key: ERROR_KEYS.LOWERCASE_MISSING };
  }
  if (!hasUpperCase) {
    return { key: ERROR_KEYS.UPPERCASE_MISSING };
  }
  if (!hasDigits) {
    return { key: ERROR_KEYS.NUMBER_MISSING };
  }
  if (!hasSpecialCharacters) {
    return { key: ERROR_KEYS.MISSING_SPECIAL_CHAR };
  }
};

const validateBchAddress = (address: string): IError | void => {
  if (!address.match(bchAddrRegex)) {
    return {
      key: ERROR_KEYS.WRONG_ADDRESS,
    };
  }
};

const validate2FACode = (code: string): IError | void => {
  if (!code) {
    return { key: ERROR_KEYS.EMPTY_VALUE, params: { field: 'code' } };
  }
  if (code.length < 6) {
    return {
      key: ERROR_KEYS.TOO_SHORT,
      params: { length: 6 },
    };
  }
};

export {
  validateName,
  validateEmail,
  validatePassword,
  validateBchAddress,
  validate2FACode,
};
