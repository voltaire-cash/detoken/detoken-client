/* eslint-disable @typescript-eslint/no-var-requires */
const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const VersionFile = require('webpack-version-file-plugin');

const root = path.resolve(__dirname, '../');
const src = `${root}/src`;
const babelLoader = {
  loader: 'babel-loader',
  options: {
    presets: ['@babel/preset-typescript', '@babel/preset-react'],
    // TODO fix plugin to have descriptive css class names!
    plugins: [
      'babel-plugin-emotion',
      '@babel/plugin-proposal-optional-chaining',
      'transform-class-properties',
    ],
  },
};

const versionFileTemplate = `{
  "version": "<%= package.version %>",
  "buildDate": "<%= Date.now(); %>"
}`;

module.exports = {
  entry: './src/index.tsx',
  context: `${root}`,
  output: {
    path: path.join(__dirname, '../dist'),
    filename: 'bundle.min.js',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    modules: [`${src}`, `${root}/node_modules/`],
    alias: {
      '~components': `${src}/components`,
      '~config': `${src}/config`,
      '~dux': `${src}/dux`,
      '~pages': `${src}/pages`,
      '~providers': `${src}/providers`,
      '~utils': `${src}/utils`,
      '~types': `${src}/types`,
      '~icons': `${src}/icons`,
      '~hooks': `${src}/hooks`,
    },
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: [babelLoader],
      },
      {
        test: /\.css$/,
        loaders: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(eot|ttf|woff|woff2|ico)$/,
        include: /fonts|assets/,
        loaders: ['file-loader'],
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
      {
        test: path.resolve(__dirname, '../node_modules/zeromq/lib/index.js'),
        use: 'null-loader',
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './public/index.html',
      favicon: './public/favicon.svg',
    }),
    new webpack.EnvironmentPlugin([
      'API_ORIGIN',
      'LANDING_ORIGIN',
      'WS_ORIGIN',
    ]),
    new CopyWebpackPlugin({
      patterns: [{ from: path.join(__dirname, '../src/assets'), to: 'assets' }],
    }),
    new webpack.IgnorePlugin(/^\.\/wordlists\/(?!english)/, /bip39\/src$/),
    new VersionFile({
      packageFile: `${root}/package.json`,
      templateString: versionFileTemplate,
      outputFile: `${root}/dist/version.json`,
    }),
  ],
  devServer: {
    host: '0.0.0.0',
    disableHostCheck: true,
    historyApiFallback: true,
    contentBase: path.join(__dirname, '../dist'),
    compress: true,
    port: 3000,
  },
  node: {
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
  },
};
