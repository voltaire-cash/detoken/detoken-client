/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const webpack = require('webpack');
const VersionFile = require('webpack-version-file-plugin');

const root = path.resolve(__dirname, '../');
const src = `${root}/src`;

const babelLoader = {
  loader: 'babel-loader',
  options: {
    cacheDirectory: true,
    presets: ['@babel/preset-typescript', '@babel/preset-react'],
    plugins: [
      'babel-plugin-emotion',
      '@babel/plugin-proposal-optional-chaining',
      'transform-class-properties',
    ],
  },
};

const bundleAnalyzerOptions = {
  analyzerMode: 'static', // `static`, `server`, `disabled`
  defaultSizes: 'parsed', // `stat`, `parsed`, `gzip`
  reportFilename: `${root}/build/stats/webpack-report.html`,
  generateStatsFile: false,
  statsFilename: `${root}/build/stats/webpack-stats.json`,
  openAnalyzer: false,
};

const versionFileTemplate = `{
  "version": "<%= package.version %>",
  "buildDate": "<%= Date.now(); %>"
}`;

module.exports = {
  mode: 'production',
  context: `${root}/`,
  entry: [`${src}/index.tsx`],
  output: {
    filename: '[name].[contenthash].js',
    chunkFilename: '[name].[contenthash].bundle.js',
    path: `${root}/build/app`,
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    modules: [`${root}/node_modules/`],
    alias: {
      '~components': `${src}/components`,
      '~config': `${src}/config`,
      '~dux': `${src}/dux`,
      '~pages': `${src}/pages`,
      '~providers': `${src}/providers`,
      '~utils': `${src}/utils`,
      '~types': `${src}/types`,
      '~icons': `${src}/icons`,
      '~hooks': `${src}/hooks`,
    },
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: [babelLoader],
      },
      {
        test: /\.css$/,
        loaders: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(eot|ttf|woff|woff2|ico)$/,
        include: /fonts|assets/,
        loaders: ['file-loader'],
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './public/index.html',
      favicon: './public/favicon.svg',
    }),
    new BundleAnalyzerPlugin(bundleAnalyzerOptions),
    new webpack.EnvironmentPlugin([
      'API_ORIGIN',
      'LANDING_ORIGIN',
      'WS_ORIGIN',
    ]),
    new CopyWebpackPlugin({
      patterns: [{ from: path.join(__dirname, '../src/assets'), to: 'assets' }],
    }),
    new webpack.IgnorePlugin(/^\.\/wordlists\/(?!english)/, /bip39\/src$/),
    new VersionFile({
      packageFile: `${root}/package.json`,
      templateString: versionFileTemplate,
      outputFile: `${root}/build/app/version.json`,
    }),
  ],
  optimization: {
    minimizer: [
      new TerserPlugin({
        terserOptions: { safari10: true },
      }),
    ],
  },
  node: {
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
  },
};
